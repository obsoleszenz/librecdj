# ToDo

## Names

- zendecks
- 4decks
- deckster
- deckstar
- dexter
- zdecks
- zdex
- zendex

## Next
- [ ] Main Menu
- [ ] Save cue pos to file
- [ ] quantized jump to position
- [ ] quantized play from position
- [ ] Faster playing/analyzing as soon as we read chunk 
- [x] Flac support
  - [x] WAV
- [ ] Music player mode


## GUI Transition
- [x] split off backend from tui



## Low priority
- [ ] tag: stars
- [ ] track color
- [ ] search library
- [ ] add playlist section
- [ ] clicking on loading file
- [ ] Escape for ejecting a track?
- [ ] use ratatui over tui
- [ ] async load metadata
- [ ] egui window on top of library view??


## Ideas
- [ ] Shift => Reverse/Clone
- [ ] Alt => Move Item?
- [ ] Window/META => Don't use
- [ ] CTRL => Do action quantized

- [ ] +/= button to zoom into waveform
- [ ] +/- to x2 or half the loop

- [ ] Use yt-dlp to get track files for tests
- [ ] Reverse Controls
- [ ] Slip
- [ ] Spin Back/Forward
- [ ] State recovery
  - Save periodically:
    - selected folder
    - loaded files
    - positions
    - etc
  - On startup recover
  - This could help to recover from panics/failures?


# Keybindings

## Play & Cue
q 		Cue
q + META	Quantized Cue
w		Play
w + META	Quantized Play


## Nudging
a		Nudge slower
s		Nudge little slower
d		Nudge little faster
f		Nudge faster


## Tempo/Pitch
z		Tempo slower
x		Tempo little slower
c		Tempo little faster
v		Tempo faster

## Beat Jump
n		Jump 1 beat forward
n + SHIFT	Jump 1 beat backward
m		Jump 2 beat forward
m + SHIFT	Jump 2 beat backward
,		Jump 4 beat forward
, + SHIFT	Jump 4 beat backward
.		Jump 8 beat foward
. + SHIFT	Jump 8 beat backward
/		Jump 16 beat forward
/ + SHIFT	Jump 16 beat backward

## Beat Jump
i		1 beat loop forward
i + SHIFT	1 beat loop backward
o		2 beat loop forward
o + SHIFT	2 beat loop backward
p		4 beat loop forward
p + SHIFT	4 beat loop backward
[		8 beat loop foward
[ + SHIFT	8 beat loop backward
]		16 beat loop forward
] + SHIFT	16 beat loop backward
\		Toggle loop


## Decks & Waveforms
F1		Select Deck A
F1 + CTRL	Clone selected decked to Deck A
F2		Select Deck B
F2 + CTRL	Clone selected decked to Deck B
F3		Select Deck C
F3 + CTRL	Clone selected decked to Deck C
F4		Select Deck D
F4 + CTRL	Clone selected decked to Deck D

Space		Switch Zoom of waveforms

## Library
UP		Move cursor one up
UP + ALT	Move item one up
DOWN 		Move cursor one down
DOWN + ALT	Move item one down
LEFT		Leave directory
RIGHT		Enter directory
PAGE UP		Move cursor half a page up
PAGE DOWN	Move cursor half a page down
PAGEUP + ALT	Move item & cursor half a page up
PAGEDOWN + ALT	Move item & cursor half a page down

B		Add item to playlist
G		Edit item metadata


## Other

F12		Open debug logs


## Missing

### Ideas
- Zoom waveform
  - Space + ALT  Super zoom
  - Space + META zoom over 64 beat
- Move loop
  - j  k  l  ; ' => move loop by 1,2,4,8,16 forward, with shift backwards
  - ; ' + SHIFT => double/half
  - ; ' + ALT  => nudge forward/backward
- Remove track
  - Delete
- Open search
  - Home
- Enable/Disable Slip
  - t
- Enable/Disable Reverse
  - r


### No Ideas
- Manipulate Loop
  - Change size to specific size from beginning or end




## Unused Keys
F5 to F11
E R T Y U

1 2 3 4 5 6 7 9 0 => Hotcues?
G H J K L ; ' '

`

Esc
Tab

## Done 
- [x] Ramping
- [x] Cueing
- [x] Nudging
- [x] Quantized beat jump
- [x] BPM Sync on load
- [x] beat & bar counter
- [x] Bug: Beat counter goes up to 5?
- [x] Bug: Main bpm takes paused tracks into account
- [x] Quit dialog
- [x] Looping 
- [x] Set first beat pos (maybe just use the cue_pos?)
- [x] Shift + loop set loop end to current position
- [x] Overview waveforms
- [x] render waveform
  - [x] Arc<> the buffer
- [x] 3band waveforms
  - [x] waveform zoom
  - [x] +/= button to zoom into waveform
- [x] deck cloning
- [x] change tempo of all decks
- [x] sometimes on load file audio cracks. Maybe because of freeing the Arc in the audio engine thread?
- [x] Loading .aiff causes panic:
      > panicked at 'unsupported format: IoError(Custom { kind: UnexpectedEof, error: "end of stream" })', libplayer/src/sample_loader.rs:145:10
- [x] Loading .m3u causes panic
- [x] remove track from playlist`
- [x] SHIFT + B edit metadata of current track
