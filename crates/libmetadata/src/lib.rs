use libkeynotation::Key;
use lofty::{config::WriteOptions, error::LoftyError, prelude::*, probe::Probe, tag::Tag};
use std::path::Path;

pub struct PositiveNonZeroF32(pub f32);

impl PositiveNonZeroF32 {
    pub fn new(number: f32) -> Self {
        assert!(number > 0.0, "Non negative, non zero float");
        Self(number)
    }
}

#[derive(Default, Debug, Clone)]
pub struct Metadata {
    pub artist: String,
    pub title: String,
    pub bpm: Option<f32>,
    pub key: String,
    pub comment: String,
    pub mood: String,
}

impl Metadata {
    pub fn serialize(&self) -> String {
        format!(
            "artist: {}\ntitle: {}\nbpm: {}\nkey: {}\nmood: {}\ncomment: {}",
            self.artist,
            self.title,
            self.bpm.unwrap_or(-1.0),
            self.key,
            self.mood,
            self.comment
        )
    }

    pub fn deserialize(deserialize: &str) -> Result<Self, String> {
        let mut artist: String = "".into();
        let mut title: String = "".into();
        let mut bpm: Option<f32> = None;
        let mut key: String = "".into();
        let mut comment: String = "".into();
        let mut mood: String = "".into();

        for (i, line) in deserialize.split('\n').enumerate() {
            let line = line.trim();
            if line.is_empty() {
                continue;
            }
            let (k, v) = match line.split_once(':') {
                None => return Err(format!("Line {}: Could not find seperator ':'", i + 1)),
                Some((k, v)) => (k, v),
            };
            let k = k.trim();
            let v = v.trim();
            match k {
                "artist" => artist = v.into(),
                "title" => title = v.into(),
                "bpm" => match v.parse::<f32>() {
                    Ok(v) => bpm = Some(v),
                    Err(_) => {
                        return Err(format!("Line {}: Could not parse {} to a f32", i + 1, v))
                    }
                },
                "key" => key = v.into(),
                "mood" => mood = v.into(),
                "comment" => comment = v.into(),
                _ => return Err(format!("Line: {}: Invalid key: {}", i + 1, key)),
            }
        }

        Ok(Self {
            artist,
            title,
            bpm,
            key,
            mood,
            comment,
        })
    }
}

pub fn read_metadata(path: impl AsRef<Path>) -> Result<Metadata, LoftyError> {
    let tagged_file = Probe::open(path)?.read()?;

    let tag = match tagged_file.primary_tag() {
        Some(primary_tag) => primary_tag,
        // If the "primary" tag doesn't exist, we just grab the
        // first tag we can find. Realistically, a tag reader would likely
        // iterate through the tags to find a suitable one.
        None => match tagged_file.first_tag() {
            Some(tag) => tag,
            None => return Ok(Metadata::default()),
        },
    };

    macro_rules! get_tag_string {
        (ItemKey::$key:ident) => {
            tag.get_string(&ItemKey::$key).unwrap_or("").to_string()
        };
    }

    let bpm = match tag.get_string(&ItemKey::Bpm) {
        Some(bpm) => bpm,
        None => tag.get_string(&ItemKey::IntegerBpm).unwrap_or("0.0"),
    };
    let bpm: Option<f32> = bpm
        .parse::<f32>()
        .ok()
        .and_then(|b| if b > 0.0 { Some(b) } else { None });

    let key = {
        let initial_key = get_tag_string!(ItemKey::InitialKey);

        match Key::from_string(&initial_key) {
            Ok(key) => key.lancelot(),
            Err(_) => initial_key,
        }
    };

    let metadata = Metadata {
        artist: get_tag_string!(ItemKey::TrackArtist),
        title: get_tag_string!(ItemKey::TrackTitle),
        bpm,
        key,
        comment: get_tag_string!(ItemKey::Comment),
        mood: get_tag_string!(ItemKey::Mood),
    };
    Ok(metadata)
}
pub fn write_metadata(
    path: impl AsRef<Path> + Copy,
    metadata: &Metadata,
) -> Result<(), LoftyError> {
    let mut tagged_file = Probe::open(path)?.read()?;

    let tag = match tagged_file.primary_tag_mut() {
        Some(primary_tag) => primary_tag,
        None => {
            if let Some(first_tag) = tagged_file.first_tag_mut() {
                first_tag
            } else {
                let tag_type = tagged_file.primary_tag_type();

                eprintln!("WARN: No tags found, creating a new tag of type `{tag_type:?}`");
                tagged_file.insert_tag(Tag::new(tag_type));

                tagged_file.primary_tag_mut().unwrap()
            }
        }
    };

    tag.insert_text(ItemKey::TrackArtist, metadata.artist.clone());
    tag.insert_text(ItemKey::TrackTitle, metadata.title.clone());
    tag.insert_text(ItemKey::Bpm, metadata.bpm.unwrap_or(-1.0).to_string());
    tag.insert_text(
        ItemKey::IntegerBpm,
        (metadata.bpm.unwrap_or(-1.0) as i64).to_string(),
    );
    tag.insert_text(ItemKey::InitialKey, metadata.key.clone());
    tag.insert_text(ItemKey::Comment, metadata.comment.clone());
    tag.insert_text(ItemKey::Mood, metadata.mood.clone());
    tag.save_to_path(path, WriteOptions::default())?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let metadata = read_metadata(
"/home/kerle/Musik/Techno/2023/05-Mai-Yuna/Kenji Hina - Tokyo Rythms Trilogy - 01 Laced Up.aiff"
        )
        .expect("Failed reading metadata");

        println!("{metadata:?}")
    }
}
