
use biquad::{Biquad, Coefficients, DirectForm2Transposed, ToHertz, Type, Q_BUTTERWORTH_F32};
use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct Waveform {
    pub sample_rate: usize,
    pub window_size: usize,
    pub bands: Arc<Vec<[f32; 3]>>,
}

pub fn calculate_waveform(
    stereo_audio_samples: Arc<Vec<Vec<f32>>>,
    sample_rate: usize,
    window_size: usize,
) -> Waveform {
    let len_samples = stereo_audio_samples[0].len();

    // Initialize 3band crossover
    //let mut crossover = Crossover::new(sample_rate);
    let mut crossover = BiquadCrossover::new(sample_rate as f32, 80.0, 14000.0);

    let mut sum_window_lo = 0.0;
    let mut sum_window_mi = 0.0;
    let mut sum_window_hi = 0.0;

    let mut c_sum_window = 0;

    let mut max_sum_window_lo: f32 = -70.0;
    let mut max_sum_window_mi: f32 = -70.0;
    let mut max_sum_window_hi: f32 = -70.0;

    let mut window_sums = Vec::with_capacity(len_samples / window_size);
    // Iterate over all stereo samples

    for i in 0..stereo_audio_samples[0].len() {
        // Turn stereo signal into mono signal
        let (sample_l, sample_r) = (stereo_audio_samples[0][i], stereo_audio_samples[1][i]);
        let mono_sample = (sample_l + sample_r) / 2.0;

        // Split mono signal into the low/mid/high bands with the crossover
        let [mono_lo, mono_mi, mono_hi] = crossover.run(mono_sample);

        // Calculate the square value of each band. Square = to the power of 2
        let (mono_lo_sqr, mono_mi_sqr, mono_hi_sqr) =
            (mono_lo.powf(2.0), mono_mi.powf(2.0), mono_hi.powf(2.0));

        // Add each band to the sum for this window
        sum_window_lo += mono_lo_sqr;
        sum_window_mi += mono_mi_sqr;
        sum_window_hi += mono_hi_sqr;

        c_sum_window += 1;
        // Check if we reached the maximum samples we want to average per window
        if c_sum_window == window_size {
            // Calculate the average of each band
            let sum_window_lo_average = sum_window_lo / window_size as f32;
            let sum_window_mi_average = sum_window_mi / window_size as f32;
            let sum_window_hi_average = sum_window_hi / window_size as f32;

            // Now take the square root for each band. Now we have a proper RMS value
            // for each band.
            let sum_window_lo_average_sqrt = sum_window_lo_average.sqrt();
            let sum_window_mi_average_sqrt = sum_window_mi_average.sqrt();
            let sum_window_hi_average_sqrt = sum_window_hi_average.sqrt();

            // Next we turn the RMS value for each band into dB
            let sum_window_lo_average_db = sum_window_lo_average_sqrt.log(10.0) * 20.0;
            let sum_window_mi_average_db = sum_window_mi_average_sqrt.log(10.0) * 20.0;
            let sum_window_hi_average_db = sum_window_hi_average_sqrt.log(10.0) * 20.0;

            // Check if we found a new max sum
            max_sum_window_lo = max_sum_window_lo.max(sum_window_lo_average_db);
            max_sum_window_mi = max_sum_window_mi.max(sum_window_mi_average_db);
            max_sum_window_hi = max_sum_window_hi.max(sum_window_hi_average_db);

            // Add the sums to our window_sums vector
            window_sums.push([
                sum_window_lo_average_db,
                sum_window_mi_average_db,
                sum_window_hi_average_db,
            ]);

            // Reset our state variables
            c_sum_window = 0;
            sum_window_lo = 0.0;
            sum_window_mi = 0.0;
            sum_window_hi = 0.0;
        }
    }

    // We want to gain stage all bands
    let gain_lo = if max_sum_window_lo > -70.0 {
        -6.0 - max_sum_window_lo
    } else {
        0.0
    };
    let gain_mi = if max_sum_window_mi > -140.0 {
        15.0 - max_sum_window_mi
    } else {
        0.0
    };
    let gain_hi = if max_sum_window_hi > -140.0 {
        30.0 - max_sum_window_hi
    } else {
        0.0
    };
    window_sums.iter_mut().for_each(|[lo, mi, hi]| {
        *lo += gain_lo;
        *mi += gain_mi;
        *hi += gain_hi;
    });

    Waveform {
        sample_rate,
        window_size,
        bands: Arc::new(window_sums),
    }
}
struct BiquadCrossover {
    biquad_f0_lp: DirectForm2Transposed<f32>,
    biquad_f0_hp: DirectForm2Transposed<f32>,
    biquad_f1_lp: DirectForm2Transposed<f32>,
    biquad_f1_hp: DirectForm2Transposed<f32>,
}

impl BiquadCrossover {
    fn new(sample_rate: f32, f0: f32, f1: f32) -> Self {
        let coeffs_f0_lp = Coefficients::<f32>::from_params(
            Type::LowPass,
            sample_rate.hz(),
            f0.hz(),
            Q_BUTTERWORTH_F32,
        )
        .unwrap();
        let coeffs_f0_hp = Coefficients::<f32>::from_params(
            Type::HighPass,
            sample_rate.hz(),
            f0.hz(),
            Q_BUTTERWORTH_F32,
        )
        .unwrap();
        let coeffs_f1_lp = Coefficients::<f32>::from_params(
            Type::LowPass,
            sample_rate.hz(),
            f1.hz(),
            Q_BUTTERWORTH_F32,
        )
        .unwrap();
        let coeffs_f1_hp = Coefficients::<f32>::from_params(
            Type::HighPass,
            sample_rate.hz(),
            f1.hz(),
            Q_BUTTERWORTH_F32,
        )
        .unwrap();

        let biquad_f0_lp = DirectForm2Transposed::<f32>::new(coeffs_f0_lp);
        let biquad_f0_hp = DirectForm2Transposed::<f32>::new(coeffs_f0_hp);
        let biquad_f1_lp = DirectForm2Transposed::<f32>::new(coeffs_f1_lp);
        let biquad_f1_hp = DirectForm2Transposed::<f32>::new(coeffs_f1_hp);

        Self {
            biquad_f0_lp,
            biquad_f0_hp,
            biquad_f1_lp,
            biquad_f1_hp,
        }
    }
    fn run(&mut self, sample: f32) -> [f32; 3] {
        let lo = self.biquad_f0_lp.run(sample);
        let mi_and_hi = self.biquad_f0_hp.run(sample);
        let mi = self.biquad_f1_lp.run(mi_and_hi);
        let hi = self.biquad_f1_hp.run(mi_and_hi);

        [lo, mi, hi]
    }
}
