pub struct LowPass {
    b: f64,
    y: f64,
}
impl LowPass {
    pub fn new(decay: f64) -> Self {
        Self {
            b: 1.0 - decay,
            y: 0.0,
        }
    }
    pub fn reset(&mut self) {
        self.y = 0.0;
    }

    pub fn insert(&mut self, x: f64) -> f64 {
        self.y += self.b * (x - self.y);
        self.y
    }

    pub fn value(&self) -> f64 {
        self.y
    }
}
