use jack::{jack_sys::jackctl_parameter_get_long_description, AudioOut, MidiIn, Port, RawMidi};
use rtrb::{Consumer, Producer};
use tracing::{debug, info};

use crate::scratch_engine::ScratchEngine;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
struct Midi {
    time: u32,
    bytes: (u8, u8, u8),
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum Event {
    Jog { frame: u32, time: u32, value: i32 },
    JogTouch { frame: u32, time: u32 },
    JogRelease { frame: u32, time: u32 },
}
impl From<RawMidi<'_>> for Midi {
    fn from(value: RawMidi) -> Self {
        let bytes = (value.bytes[0], value.bytes[1], value.bytes[2]);
        Self {
            time: value.time,
            bytes,
        }
    }
}

/// JackProcessHandler responds to JACK's requests to generate new buffers
/// of audio to pass to the audio interface (or another JACK client).
struct JackProcessHandler {
    out_a_l: Port<AudioOut>,
    out_a_r: Port<AudioOut>,
    midi_in: Port<MidiIn>,
    producer: Producer<Event>,
    scratch_engine: ScratchEngine,
}

impl JackProcessHandler {
    fn handle_midi(&mut self, _client: &jack::Client, ps: &jack::ProcessScope) {
        let frame = ps.last_frame_time();
        let midi_in_port = &self.midi_in;
        for midi in midi_in_port.iter(ps) {
            let midi: Midi = midi.into();
            debug!("midi={midi:?}");
            let r#type = midi.bytes.0 >> 4;
            match midi.bytes {
                (status, 0x51, value) if r#type == 0xB => {
                    let value = value as i32 - 64;
                    self.producer.push(Event::Jog {
                        frame,
                        time: midi.time,
                        value,
                    });
                }
                (status, 0x51, 64) if r#type == 0x9 => {
                    debug!("jog touch");
                    self.producer.push(Event::JogTouch {
                        frame,
                        time: midi.time,
                    });
                }
                (status, 0x51, 0) if r#type == 0x8 => {
                    debug!("jog release");
                    self.producer.push(Event::JogRelease {
                        frame,
                        time: midi.time,
                    });
                }
                _ => (),
            }
        }
    }
}

impl jack::ProcessHandler for JackProcessHandler {
    fn process(&mut self, client: &jack::Client, ps: &jack::ProcessScope) -> jack::Control {
        //assert_no_alloc::assert_no_alloc(|| {

        let sample_rate_out = client.sample_rate();
        let frame_count = ps.last_frame_time();

        self.handle_midi(client, ps);

        jack::Control::Continue
    }

    fn buffer_size(&mut self, _: &jack::Client, buffer_size_frames: jack::Frames) -> jack::Control {
        info!("buffer size changed to {buffer_size_frames} frames");
        jack::Control::Continue
    }
}

/// JackBackend initializes a new JACK client, creates and connects its ports,
/// and tells JACK to start processing.
pub struct JackBackend {
    _async_client: jack::AsyncClient<(), JackProcessHandler>,
    pub consumer: Consumer<Event>,
}

impl JackBackend {
    pub fn new() -> JackBackend {
        let (producer, consumer) = rtrb::RingBuffer::new(1024);
        let (jack_client, _status) =
            jack::Client::new("scratchengine", jack::ClientOptions::NO_START_SERVER).unwrap();

        macro_rules! register_port_audio_out {
            ($name:expr) => {
                jack_client
                    .register_port($name, jack::AudioOut)
                    .expect(&format!("Failed registering audio out port {}", $name))
            };
        }
        macro_rules! register_port_midi_in {
            ($name:expr) => {
                jack_client
                    .register_port($name, jack::MidiIn)
                    .expect(&format!("Failed registering midi in port {}", $name))
            };
        }
        macro_rules! register_port_midi_out {
            ($name:expr) => {
                jack_client
                    .register_port($name, jack::MidiOut)
                    .expect(&format!("Failed registering midi out port {}", $name))
            };
        }
        let out_a_l = register_port_audio_out!("deck_a_L");
        let out_a_r = register_port_audio_out!("deck_a_R");

        let midi_in = register_port_midi_in!("sc2000");

        let process_handler = JackProcessHandler {
            out_a_l,
            out_a_r,
            midi_in,
            producer,
            scratch_engine: ScratchEngine::new(0.8, jack_client.sample_rate()),
        };

        let async_client = jack_client.activate_async((), process_handler).unwrap();
        // activate_async consumes the client but it is still needed below.

        JackBackend {
            _async_client: async_client,
            consumer: consumer,
        }
    }
}
