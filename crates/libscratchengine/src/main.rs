mod filter;
mod jack;
mod scratch_engine;

use std::{f32::consts::TAU, thread::sleep, time::Duration};

use crate::jack::{Event, JackBackend};
use eframe::{
    egui::{self, vec2, Color32, Pos2, Sense, Stroke, Ui, Vec2},
    Frame,
};
use egui_plot::{Bar, BarChart, Line, Plot, PlotPoint, PlotPoints, Points};
use filter::LowPass;
use rtrb::Consumer;
use tracing::{debug, info};

fn main() {
    tracing_subscriber::fmt::init();

    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "My egui App",
        native_options,
        Box::new(|cc| Box::new(MyEguiApp::new(cc))),
    )
    .expect("Failed starting gui");
}

struct MyEguiApp {
    angle: f32,
    lp_angle: f32,
    jack_backend: JackBackend,
    accum: i32,
    raw_points: Vec<[f64; 2]>,
    lp_points: Vec<[f64; 2]>,
    on_off_points: Vec<[f64; 2]>,
    lp_filter: LowPass,
}

impl MyEguiApp {
    fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // Customize egui here with cc.egui_ctx.set_fonts and cc.egui_ctx.set_visuals.
        // Restore app state using cc.storage (requires the "persistence" feature).
        // Use the cc.gl (a glow::Context) to create graphics shaders and buffers that you can use
        // for e.g. egui::PaintCallback.
        let jack_backend = JackBackend::new();
        Self {
            angle: 0.0,
            lp_angle: 0.0,
            jack_backend,
            accum: 0,
            raw_points: vec![],
            lp_points: vec![],
            on_off_points: vec![],
            lp_filter: LowPass::new(0.8),
        }
    }
    fn reset(&mut self) {
        self.angle = 0.0;
        self.lp_angle = 0.0;
        self.accum = 0;
        self.raw_points.clear();
        self.lp_points.clear();
        self.on_off_points.clear();
    }
}

impl eframe::App for MyEguiApp {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        while !self.jack_backend.consumer.is_empty() {
            let event = self.jack_backend.consumer.pop().expect("Failed popping");
            match event {
                Event::Jog { frame, time, value } => {
                    let angle = 360.0 * (value as f32) / 2048.0;
                    self.angle -= angle;
                    self.accum += value;
                    let value = value as f64;
                    let x = (frame + time) as f64;
                    let lp_value = self.lp_filter.insert(value);
                    let lp_angle = 360.0 * (lp_value as f32) / 2048.0;
                    self.lp_angle -= lp_angle;
                    self.raw_points.push([x, value as f64]);
                    self.lp_points.push([x, lp_value]);
                    debug!("self.angle={}", self.angle);
                    debug!("self.accum={}", self.accum);
                }
                Event::JogTouch { frame, time } => {
                    let x = (frame + time) as f64;
                    self.on_off_points.push([x, 64.0]);
                }
                Event::JogRelease { frame, time } => {
                    let x = (frame + time) as f64;
                    self.on_off_points.push([x, 0.0]);
                }
            }
            info!("event={event:?}");
        }
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.horizontal(|ui| {
                ui.vertical(|ui| {
                    draw_circle(ui, self.angle, Color32::BLUE);
                    draw_circle(ui, self.lp_angle, Color32::RED);
                    if ui.button("reset").clicked() {
                        self.reset()
                    }
                });
                ui.vertical_centered_justified(|ui| {
                    let raw_line = Line::new(self.raw_points.clone()).color(Color32::BLUE);
                    let raw_points =
                        Points::new(self.raw_points.clone()).color(Color32::LIGHT_BLUE);
                    let lp_line = Line::new(self.lp_points.clone()).color(Color32::RED);
                    let lp_points = Points::new(self.lp_points.clone()).color(Color32::LIGHT_RED);
                    let on_off_bars = self
                        .on_off_points
                        .iter()
                        .zip(self.on_off_points.iter().skip(1))
                        .filter_map(|(a, b)| {
                            if a[1] == 64.0 && b[1] == 0.0 {
                                return Some(
                                    Bar::new(a[0], 64.0)
                                        .width(b[0] - a[0])
                                        .fill(Color32::LIGHT_GREEN),
                                );
                            }
                            None
                        })
                        .collect();
                    Plot::new("my_plot").show(ui, |plot_ui| {
                        plot_ui.bar_chart(BarChart::new(on_off_bars));
                        plot_ui.line(raw_line);
                        plot_ui.points(raw_points);
                        plot_ui.line(lp_line);
                        plot_ui.points(lp_points);
                    });
                });
            });
            ctx.request_repaint();
        });
    }
}

const CIRCLE_SIZE: f32 = 256.0;

pub fn draw_circle(ui: &mut Ui, angle: f32, color: impl Into<Color32>) {
    let size = Vec2::splat(CIRCLE_SIZE);
    let (response, painter) = ui.allocate_painter(size, Sense::hover());
    let rect = response.rect;
    let c = rect.center();
    let r = rect.width() / 2.0 - 1.0;

    // Middle line
    painter.line_segment(
        [c - vec2(0.0, r), c + vec2(0.0, r)],
        Stroke::new(1.0, Color32::DARK_GRAY),
    );

    let stroke = Stroke::new(1.0, color);
    painter.circle_stroke(c, r, stroke);

    let line_point_a = rotate_point(c - vec2(0.0, r), c, angle);
    let line_point_b = rotate_point(c + vec2(0.0, r), c, angle);
    painter.line_segment([line_point_a, line_point_b], stroke);
}

pub fn rotate_point(point: Pos2, center: Pos2, angle: f32) -> Pos2 {
    let angle = angle.to_radians();
    let s: f32 = angle.sin();
    let c: f32 = angle.cos();

    // Translate point back to origin
    let translated_point = point - center;

    // Rotate point
    let translated_point = Pos2::new(
        translated_point.x * c - translated_point.y * s,
        translated_point.x * s - translated_point.y * c,
    );

    // Translate point back
    let translated_point = translated_point + center.to_vec2();
    translated_point
}
