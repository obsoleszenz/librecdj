use std::collections::VecDeque;

use crate::filter::LowPass;

enum JogWheelInput {
    JogTouch,
    JogRelease,
    Rotate(f64),
}

enum JogWheelOutput {
    PositionChange(u32),
    TempoChange(f64),
    Play,
    Pause,
}

pub struct ScratchEngine {
    value_lp_filter: LowPass,
    time_diff_lp_filter: LowPass,
    sample_rate: usize,
    last_insert_times: u32,
    last_value: f64,
    events: VecDeque<JogWheelOutput>,
}
impl ScratchEngine {
    pub fn new(lp_filter_decay: f64, sample_rate: usize) -> Self {
        Self {
            value_lp_filter: LowPass::new(lp_filter_decay),
            time_diff_lp_filter: LowPass::new(0.9),
            sample_rate,
            last_insert_times: 0,
            last_value: 0.0,
            events: VecDeque::with_capacity(20),
        }
    }

    pub fn set_sample_rate(&mut self, sample_rate: u32) {
        self.sample_rate = sample_rate as usize;
        // TODO: Maybe we need to reset [`self.last_insert_times`] and [`self.time_diff_lp_filter`]?
    }

    /// Insert a new value with it's time.
    /// Returns a smoothed output value
    pub fn insert(&mut self, time: u32, value: f64) -> f64 {
        // Calculate time diff between last insert and this
        let time_diff = time - self.last_insert_times;

        // Smooth the time diff
        let time_diff = self.time_diff_lp_filter.insert(time_diff as f64);

        // Smooth the value
        let value = self.value_lp_filter.insert(value);

        self.last_insert_times = time;

        // # How much tempo change did we have according to RPM?
        // Position change in degrees? Or in samples?

        // 1. Imagine the track rotates at a certain velocity
        // `velocity = jog_rpm * tempo`
        //
        // 2.Now we need to modulate a tempo on top of that depending on
        // the change of the jog wheel
        // `velocity += jog_rpm / (jog_interval_change / jog_interval_per_rev)`

        // # How much position change in seconds did we have?
        // `advance_per_sec = tempo`
        // `jog_advance_sec` = `jog_rpm / (jog_interval_change / jog_interval_per_rev)`
        // Is it the same?????????????????????

        // For position change, how do we take the ramping into account? Do we need to?

        value
    }

    /// JogWheels often don't send a zero, so we somehow need to estimate that ourselves
    /// Returns an optional time at which we would estimate the zero reset
    pub fn get_zero_reset(&mut self, time: u32) -> Option<u32> {
        if self.last_value == 0.0 {
            return None;
        }
        let time_diff_to_last_insert = time - self.last_insert_times;
        let average_step_time = self.time_diff_lp_filter.value() as u32;
        if time_diff_to_last_insert < average_step_time {
            return None;
        }

        Some(self.last_insert_times + average_step_time)
    }
}
