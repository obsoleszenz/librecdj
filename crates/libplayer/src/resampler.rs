use rubato::{
    ResampleResult, SincFixedOut, SincInterpolationParameters, SincInterpolationType, VecResampler,
    WindowFunction,
};

pub const RESAMPLER_PARAMETERS: SincInterpolationParameters = SincInterpolationParameters {
    sinc_len: 256,
    f_cutoff: 0.95,
    oversampling_factor: 128,
    interpolation: SincInterpolationType::Cubic,
    window: WindowFunction::Hann2,
};

pub const MAX_CHUNK_SIZE: usize = 2048;
pub const MIN_TEMPO_RATIO: f32 = 0.1;
pub const MAX_TEMPO_RATIO: f32 = 2.0;

pub type ResamplerResult<T> = Result<T, rubato::ResampleError>;

/// A wrapper around rubato resampler to make the usage easier
/// and lazily allocates a new rubato resampler if needed.
pub struct Resampler {
    rubato: SincFixedOut<f32>,
    sample_rate_in: f64,
    sample_rate_out: f64,
    chunk_size: usize,
    tempo: f64,
}
impl Default for Resampler {
    fn default() -> Self {
        Self::new(48_000.0, 48_000.0, MAX_CHUNK_SIZE)
    }
}

impl Resampler {
    pub fn new(sample_rate_in: f64, sample_rate_out: f64, max_chunk_size: usize) -> Self {
        #[allow(clippy::expect_used)]
        let rubato = SincFixedOut::<f32>::new(
            Self::calculate_resample_ratio(sample_rate_in, sample_rate_out, 1.0),
            (1.0 / MIN_TEMPO_RATIO) as f64,
            RESAMPLER_PARAMETERS,
            max_chunk_size,
            2,
        )
        .expect("Failed creating SincFixedOut resampler");
        Self {
            rubato,
            sample_rate_in,
            sample_rate_out,
            tempo: 1.0,
            chunk_size: max_chunk_size,
        }
    }

    /// Calculates the resampling ratio needed for rubato.
    /// For convenience we split the calculation into the in/out sample rates and a tempo.
    /// The tempo parameter is playback speed. So 1.0 means original playback speed.
    /// 0.5 half the playback speed, 2.0 double.
    pub fn calculate_resample_ratio(sample_rate_in: f64, sample_rate_out: f64, tempo: f64) -> f64 {
        (sample_rate_out / sample_rate_in) * tempo.powi(-1)
    }

    /// Allows to change the in/out sample rates. This keeps the current tempo.
    pub fn set_sample_rate(
        &mut self,
        sample_rate_in: f64,
        sample_rate_out: f64,
    ) -> ResampleResult<()> {
        self.rubato.set_resample_ratio(
            Self::calculate_resample_ratio(sample_rate_in, sample_rate_out, self.tempo),
            false,
        )?;
        self.sample_rate_in = sample_rate_in;
        self.sample_rate_out = sample_rate_out;
        Ok(())
    }

    /// Returns the in/out sample rates
    pub fn sample_rate(&self) -> (f64, f64) {
        (self.sample_rate_in, self.sample_rate_out)
    }

    /// Playback tempo of audio can be changed by modifying the output sample
    /// rate to the one of the input sample rate. This ratio can be set with this
    /// method. tempo=1.0 means normal playback, 0.5 half as fast, 2.0 twice as fast.
    /// TODO: Is the ratio explanation correct?
    pub fn set_tempo(&mut self, tempo: f64, ramp: bool) -> ResamplerResult<()> {
        self.rubato.set_resample_ratio(
            Self::calculate_resample_ratio(self.sample_rate_in, self.sample_rate_out, tempo),
            ramp,
        )?;
        self.tempo = tempo;
        Ok(())
    }

    /// Returns the current tempo
    pub fn tempo(&self) -> f64 {
        self.tempo
    }

    /// Set the chunk_size you want to output next
    pub fn set_chunk_size(&mut self, chunk_size: usize) -> ResamplerResult<()> {
        rubato::Resampler::set_chunk_size(&mut self.rubato, chunk_size)?;
        self.chunk_size = chunk_size;
        Ok(())
    }

    /// Returns the current chunk size
    pub fn chunk_size(&self) -> usize {
        self.chunk_size
    }

    /// Return the amount of frames needed on input & output
    /// for the next resampling
    pub fn frames_next(&self) -> (usize, usize) {
        (
            self.rubato.input_frames_next(),
            self.rubato.output_frames_next(),
        )
    }

    /// Actually resamples [`wave_in`] with provided sample rates & chunk sizes into
    /// [`wave_out`]
    /// If Ok, first usize describes the samples read,
    /// second the amount of samples written
    pub fn process_into_buffer(
        &mut self,
        wave_in: &[&[f32]; 2],
        wave_out: &mut [&mut [f32]; 2],
    ) -> ResamplerResult<(usize, usize)> {
        log_error!(
            rubato::Resampler::process_into_buffer(&mut self.rubato, wave_in, wave_out, None,),
            "Resampling failed because of {:?}"
        )
    }
}
