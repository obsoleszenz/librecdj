/// PlayerEvents are one time signals
#[derive(Copy, Clone, Debug)]
pub enum PlayerEvent {
    /// File got loaded.
    /// Indicates that the samples got received from the [`crate::SampleLoader`] worker thread.
    FileLoaded,
    /// Resampler is ready
    /// Indicates that the samples got received from the [`crate::Resampler`] worker thread.
    ResamplerReady,
}
