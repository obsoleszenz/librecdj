use std::ops::{Deref, DerefMut};

#[derive(Clone, Debug, Copy)]
pub struct Bpm(pub f32);

impl Bpm {
    pub fn per_second(&self) -> f32 {
        self.0 / 60.0
    }

    pub fn calculate_beat_length_from_duration(&self, duration_secs: f32) -> f32 {
        (duration_secs * self.per_second()).round_ties_even()
    }
}

impl Default for Bpm {
    fn default() -> Self {
        Self(-1.0)
    }
}

impl Deref for Bpm {
    type Target = f32;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Bpm {
    fn deref_mut(&mut self) -> &mut f32 {
        &mut self.0
    }
}
