use std::time::Duration;

use serde::{Deserialize, Serialize};

use crate::{Bpm, HHMMSS};

#[derive(Clone, Debug, Default, Copy)]
pub struct FileInfo {
    /// Sample Rate of file
    pub sample_rate: usize,
    /// Total count of samples
    pub count_samples: usize,
}

#[derive(Clone, Debug, Default, Copy)]
pub struct LoopState {
    /// Start position of loop in samples. Relative to file sample rate.
    pub start: i64,
    /// End position of loop in samples. Relative to file sample rate.
    pub end: i64,
    /// Is this loop enabled?
    pub enabled: bool,
}

impl LoopState {
    pub fn duration_samples(&self) -> i64 {
        self.end - self.start
    }

    /// Calculate the length of this loop in seconds
    /// Attention: You need to pass the initial sample rate (aka file sample rate) to this method.
    /// Start & End position are always at file sample rate
    pub fn duration_secs(&self, initial_sample_rate: f32) -> f32 {
        self.duration_samples() as f32 / initial_sample_rate
    }

    pub fn is_playhead_in_loop(&self, playhead_samples: i64) -> bool {
        playhead_samples >= self.start && playhead_samples <= self.end
    }

    pub fn len_beats(&self, sample_rate: f32, bpm: f32) -> f32 {
        let bpm = Bpm(bpm);
        bpm.calculate_beat_length_from_duration(self.duration_secs(sample_rate))
    }
}

#[derive(Clone, Debug, Default, Copy)]
pub enum PitchBendRange {
    #[default]
    FivePercent,
    SevenPercent,
    TenPercent,
    ThreePercent,
}

#[derive(Clone, Debug, Default, Copy, Deserialize, Serialize, PartialEq)]
pub struct PlayerStateMetadata {
    pub bpm: Option<f32>,
}

#[derive(Clone, Debug, Default)]
pub struct PlayerState {
    pub file_info: Option<FileInfo>,
    /// Current playhead position (in samples)
    pub playhead_samples: i64,

    /// Tempo
    pub tempo: f32,

    /// JogWheel tempo
    pub jogwheel_tempo: f32,

    /// Direction
    pub reverse: bool,

    /// Are we playing?
    pub play: bool,

    /// The CUE pos, on cue release we will jump back to here
    pub cue_pos: i64,
    /// Are we cueing right now?
    pub cue: bool,

    /// loop
    pub looping: Option<LoopState>,

    /// A settable size used for beat jumping/looping
    /// TODO: Maybe there's a better name for this?
    pub beat_size: f32,

    /// PitchBend Range
    pub pitch_bend_range: PitchBendRange,

    /// Additional metadata. Right now only holds bpm
    pub metadata: PlayerStateMetadata,
}

impl PlayerState {
    /// Helper that calculates the [`std::time::Duration`] from a sample pos (and current sample rate)
    fn _sample_pos_into_duration(&self, sample_pos: i64) -> Position {
        if let Some(file_info) = &self.file_info {
            return Position::new(sample_pos, file_info.sample_rate as f64);
        }
        Position::new_failed()
    }

    pub fn playhead_samples(&self) -> i64 {
        self.playhead_samples
    }

    pub fn playhead_secs(&self) -> Option<f64> {
        Some(self.playhead_samples as f64 / self.initial_sample_rate()? as f64)
    }

    /// Returns the total [`Duration`] of this audio file
    pub fn position(&self) -> Position {
        self._sample_pos_into_duration(self.playhead_samples)
    }
    /// Returns the total [`Duration`] of this audio file
    pub fn total(&self) -> Position {
        if let Some(file_info) = &self.file_info {
            return self._sample_pos_into_duration(file_info.count_samples as i64);
        }
        Position::new_failed()
    }

    pub fn initial_bpm(&self) -> Option<f32> {
        self.metadata.bpm
    }

    pub fn bpm(&self) -> Option<f32> {
        Some(self.initial_bpm()? * self.tempo())
    }

    pub fn playing(&self) -> bool {
        self.play
    }

    pub fn loaded(&self) -> bool {
        self.file_info.is_some()
    }

    /// Returns true if the playhead is in between the start and end of an enabled loop
    /// Or said otherwise: We are playing and looping
    pub fn is_playhead_inside_enabled_loop(&self) -> bool {
        match self.looping {
            Some(loop_state) => {
                loop_state.enabled && loop_state.is_playhead_in_loop(self.playhead_samples())
            }
            None => false,
        }
    }

    pub fn loop_length_beat(&self) -> Option<f32> {
        let loop_state = self.looping?;

        let initial_sample_rate = self.initial_sample_rate()? as f32;
        let initial_bpm = self.initial_bpm()?;
        Some(loop_state.len_beats(initial_sample_rate, initial_bpm))
    }

    pub fn initial_sample_rate(&self) -> Option<usize> {
        self.file_info.map(|file_info| file_info.sample_rate)
    }

    pub fn tempo(&self) -> f32 {
        self.tempo
    }

    pub fn cue_position_secs(&self) -> Option<f64> {
        // TODO: Why can we use initial_sample_rate here? Doesn't it need to be the actual sample rate?
        self.initial_sample_rate()
            .map(|initial_sample_rate| self.cue_pos as f64 / initial_sample_rate as f64)
    }
}

//TODO: Move to units.rs
pub struct Position {
    negative: bool,
    duration: Duration,
    fail: bool,
}

impl Position {
    pub fn new(sample_pos: i64, rate: f64) -> Self {
        if rate == 0.0 {
            return Self {
                negative: false,
                duration: Duration::from_secs(0),
                fail: false,
            };
        }
        Self {
            negative: sample_pos < 0,
            duration: Duration::from_secs_f64(sample_pos.abs() as f64 / rate),
            fail: false,
        }
    }

    pub fn new_failed() -> Self {
        Self {
            negative: false,
            duration: Duration::from_secs_f64(0.0),
            fail: true,
        }
    }
}

impl ToString for Position {
    fn to_string(&self) -> String {
        if self.fail {
            return "--".to_string();
        }
        format!(
            "{}{}",
            if self.negative { "-" } else { "+" },
            self.duration.as_hhmmss()
        )
    }
}
