/// A simple queue data structure based on a const sized array.
#[derive(Debug)]
pub struct Queue<T, const S: usize>
where
    T: Copy,
{
    queue: [Option<T>; S],
}

impl<T, const S: usize> Queue<T, S>
where
    T: Copy,
{
    pub fn new() -> Self {
        Self { queue: [None; S] }
    }

    /// Push item to first free slot. Fails if queue is full.
    pub fn push(&mut self, item: T) -> Result<(), ()> {
        // Find first free slot
        for queue_pos in self.queue.iter_mut() {
            if queue_pos.is_none() {
                *queue_pos = Some(item);
                return Ok(());
            }
        }
        Err(())
    }

    /// Pop latest item
    pub fn pop(&mut self) -> Option<T> {
        for queue_pos in self.queue.iter_mut().rev() {
            if queue_pos.is_some() {
                return queue_pos.take();
            }
        }
        None
    }

    /// Clear the queue
    #[allow(unused)]
    pub fn clear(&mut self) {
        for queue_pos in self.queue.iter_mut() {
            if queue_pos.is_none() {
                break;
            }
            if queue_pos.is_some() {
                queue_pos.take();
            }
        }
    }

    /// Check if is full
    #[allow(unused)]
    pub fn is_full(&mut self) -> bool {
        self.queue.last().is_some()
    }
}

#[allow(clippy::expect_used)]
#[cfg(test)]
mod test {
    use crate::queue::Queue;
    use assert_no_alloc::*;
    #[test]
    fn check_queue() {
        let mut queue: Queue<f32, 16> = Queue::new();
        assert_no_alloc(|| {
            assert_eq!(queue.queue.len(), 16);
            queue.push(1.0).expect("push failed");
            queue.push(2.0).expect("push failed");
            assert_eq!(queue.pop(), Some(2.0));
            assert_eq!(queue.pop(), Some(1.0));
            assert_eq!(queue.pop(), None);
        })
    }
}
