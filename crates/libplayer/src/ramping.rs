#[derive(Clone, Copy, Debug)]
pub enum Ramping {
    Up,
    Down,
}

impl Ramping {
    pub fn apply(&self, buffer: &mut [f32]) {
        let buffer_len_minus_one_f32 = (buffer.len() - 1) as f32;

        let (mut i, a) = match self {
            Self::Up => (0.0, 1.0),
            Self::Down => (buffer_len_minus_one_f32, -1.0),
        };

        let step = 1.0 / buffer_len_minus_one_f32;
        for val in buffer.iter_mut() {
            *val *= i * step;
            i += a;
        }
    }
}

#[cfg(test)]
mod test {
    use super::Ramping;

    #[test]
    fn check_ramping_up() {
        let mut buffer = vec![1.0; 8];
        Ramping::Up.apply(&mut buffer);
        assert_eq!(
            buffer,
            [0.0, 0.14285715, 0.2857143, 0.42857146, 0.5714286, 0.71428573, 0.8571429, 1.0,]
        );
    }

    #[test]
    fn check_ramping_down() {
        let mut buffer = vec![1.0; 8];
        Ramping::Down.apply(&mut buffer);
        assert_eq!(
            buffer,
            [1.0, 0.8571429, 0.71428573, 0.5714286, 0.42857146, 0.2857143, 0.14285715, 0.0]
        );
    }
}
