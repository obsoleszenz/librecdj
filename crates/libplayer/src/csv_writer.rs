use std::{
    fs::File,
    io::Write,
    time::{Duration, SystemTime},
};

use rtrb::{Producer, RingBuffer};

pub const CSV_REQUEST_INTERVAL: Duration = Duration::from_millis(1000 / 30);

/// Realtime safe csv writer
/// Can be helpful to debug midi data for jogwheels and such
pub struct CSVWriter {
    producer: Producer<String>,
}

impl CSVWriter {
    pub fn new() -> Self {
        let (producer, mut consumer) = RingBuffer::new(1024);

        // Spawn thread creating resampler and allocating temporary buffers
        std::thread::spawn(move || {
            #[allow(clippy::expect_used)]
            let mut f = File::options()
                .create(true)
                .append(true)
                .open("data.log")
                .expect("Failed opening data.log file");

            let start_log_time = chrono::Local::now().format("%Y-%m-%d %H:%M:%S");
            writeln!(&mut f, "# start log at {}", start_log_time).expect("Failed writing log line");
            loop {
                let interval_start = SystemTime::now();
                while !consumer.is_empty() {
                    #[allow(clippy::expect_used)]
                    let log_line = consumer.pop().expect("Unreachable");
                    writeln!(&mut f, "{log_line}").expect("Failed writing log line");
                }
                std::thread::sleep(
                    CSV_REQUEST_INTERVAL.saturating_sub(
                        #[allow(clippy::expect_used)]
                        interval_start
                            .elapsed()
                            .expect("Failed getting elapsed time"),
                    ),
                );
            }
        });

        Self { producer }
    }
    pub fn write(&mut self, log_line: String) -> Result<(), rtrb::PushError<String>> {
        self.producer.push(log_line)
    }
}

impl Default for CSVWriter {
    fn default() -> Self {
        Self::new()
    }
}
