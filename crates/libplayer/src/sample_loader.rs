use std::{
    fs::File,
    path::Path,
    sync::Arc,
    time::{Duration, SystemTime},
};

use rtrb::{Consumer, Producer};
use symphonia::core::{
    audio::SampleBuffer,
    codecs::{DecoderOptions, CODEC_TYPE_NULL},
    io::MediaSourceStream,
    probe::Hint,
};

use thiserror::Error;

const LOADER_REQUEST_INTERVAL: Duration = Duration::from_millis(30);

#[derive(Clone)]
pub struct Samples {
    pub samples: Arc<Vec<Vec<f32>>>,
    pub rate: usize,
    pub count_samples: usize,
}

impl Samples {
    /// Convenient method to create Samples for tests
    pub fn from_mono(rate: usize, mono_samples: Vec<f32>) -> Self {
        let count_samples = mono_samples.len();
        let samples = Arc::new(vec![mono_samples.clone(), mono_samples]);
        Self {
            samples,
            rate,
            count_samples,
        }
    }
}

#[derive(Debug, Error)]
pub enum SampleLoaderError {
    #[error("IO error: {0:?}")]
    IO(#[from] std::io::Error),
    #[error("Symphonia error: {0:?}")]
    Symphonia(#[from] symphonia::core::errors::Error),
    #[error("No supported audio codec found in file")]
    UnsupportedAudioCodec,
    #[error("Task queue is full")]
    FullTaskQueue,
}

pub struct SampleLoader {
    pub samples: Option<Samples>,
    recv_samples: Consumer<Samples>,
    send_task: Producer<Task>,
}

impl Default for SampleLoader {
    fn default() -> Self {
        SampleLoader::new()
    }
}
impl SampleLoader {
    pub fn new() -> Self {
        let (send_task, mut recv_task) = rtrb::RingBuffer::<Task>::new(2);
        let (mut send_samples, recv_samples) = rtrb::RingBuffer::<Samples>::new(2);

        std::thread::spawn(move || loop {
            let interval_start = SystemTime::now();
            if let Ok(task) = recv_task.pop() {
                match task {
                    Task::LoadSamples { path } => {
                        let samples = log_error_ok!(
                            load_samples_from_audio_file(&path),
                            "Failed loading samples: {:?}"
                        );
                        if let Some(samples) = samples {
                            log_error_ok!(
                                send_samples.push(samples),
                                "Error sending samples back to loader: {:?}"
                            );
                        }
                    }
                    Task::DropSamples { samples } => {
                        // As samples is an Arc<> it might get freed here.
                        // But maybe also in the ui thread. If this causes
                        // problems, we should switch to basedrop
                        let _ = samples;
                    }
                }
            }
            std::thread::sleep(
                #[allow(clippy::expect_used)]
                LOADER_REQUEST_INTERVAL.saturating_sub(
                    interval_start
                        .elapsed()
                        .expect("Unreachable: Failed getting elapsed time"),
                ),
            );
        });

        Self {
            samples: None,
            recv_samples,
            send_task,
        }
    }

    pub fn from_samples(samples: Samples) -> Self {
        let (send_task, _recv_task) = rtrb::RingBuffer::<Task>::new(2);
        let (_send_samples, recv_samples) = rtrb::RingBuffer::<Samples>::new(2);
        Self {
            samples: Some(samples),
            recv_samples,
            send_task,
        }
    }

    /// Update the sample loader if needed (so there's new samples to be received)
    pub fn update(&mut self) -> bool {
        if let Ok(samples) = self.recv_samples.pop() {
            self.samples = Some(samples);
            return true;
        }
        false
    }

    pub fn load(&mut self, path: String) -> Result<(), SampleLoaderError> {
        if let Err(_err) = self.send_task.push(Task::LoadSamples { path }) {
            debug!("Failed sending Task::LoadSamples: {}", _err);
            return Err(SampleLoaderError::FullTaskQueue);
        }
        self.drop_self_samples();
        Ok(())
    }

    pub fn drop_self_samples(&mut self) {
        if let Some(samples) = self.samples.take() {
            if let Err(_err) = self.send_task.push(Task::DropSamples { samples }) {
                debug!("Failed sending Task::DropSamples: {}", _err);
            }
        }
    }

    pub fn load_samples(&mut self, samples: &Option<Samples>) -> Result<(), SampleLoaderError> {
        self.drop_self_samples();
        self.samples = samples.clone();

        Ok(())
    }
}

enum Task {
    LoadSamples { path: String },
    DropSamples { samples: Samples },
}

/// Load a whole audio file into memory
pub fn load_samples_from_audio_file(path: &str) -> Result<Samples, SampleLoaderError> {
    // Open the media source.
    let path = Path::new(&path);
    let src = File::open(path)?;

    // Create the media source stream.
    let mss = MediaSourceStream::new(Box::new(src), Default::default());

    // Create a probe hint using the file's extension. [Optional]
    let mut hint = Hint::new();
    if let Some(extension) = path.extension() {
        hint.with_extension(&extension.to_string_lossy());
    };

    // Probe the media source.
    let probed = symphonia::default::get_probe().format(
        &hint,
        mss,
        &Default::default(),
        &Default::default(),
    )?;

    // Get the instantiated format reader.
    let mut format = probed.format;

    // Find the first audio track with a known (decodeable) codec.
    let track = format
        .tracks()
        .iter()
        .find(|t| t.codec_params.codec != CODEC_TYPE_NULL)
        .ok_or(SampleLoaderError::UnsupportedAudioCodec)?;

    // Use the default options for the decoder.
    let dec_opts: DecoderOptions = Default::default();

    // Calculate the needed size for our sample vector
    // We do this now as we will borrow track in the decoder
    let samples_capacity: usize = if let Some(n_frames) = track.codec_params.n_frames {
        n_frames as usize
    } else {
        0
    };

    // Create a decoder for the track.
    let mut decoder = symphonia::default::get_codecs().make(&track.codec_params, &dec_opts)?;

    // Create sample buffer and retrieve sample rate
    let (mut read_buffer, rate) = {
        // Read first packet and determine sample buffer size
        let packet = format.next_packet()?;
        let decoded_packet = decoder.decode(&packet)?;
        let spec = *decoded_packet.spec();
        let rate = spec.rate as usize;

        // Get the capacity of the decoded buffer. Note: This is capacity, not length!
        let duration = decoded_packet.capacity() as u64;

        let mut read_buffer = SampleBuffer::<f32>::new(duration, spec);

        // As we loaded the first packet, let's also put it into the sample buffer
        read_buffer.copy_planar_ref(decoded_packet);
        (read_buffer, rate)
    };

    // Create vector that will hold all samples
    let mut samples: Vec<Vec<f32>> = (0..2)
        .map(|_| Vec::with_capacity(samples_capacity))
        .collect::<Vec<_>>();

    loop {
        // extract left & right channel
        let (left, right) = read_buffer.samples().split_at(read_buffer.len() / 2);
        samples[0].extend_from_slice(left);
        samples[1].extend_from_slice(right);

        // Decode next packet
        let packet = match format.next_packet() {
            Ok(packet) => packet,
            Err(_err) => {
                error!("Err loading next packet: {}", _err);
                break;
            }
        };
        let decoded_packet = match decoder.decode(&packet) {
            Ok(decoded_packet) => decoded_packet,
            Err(_err) => {
                error!("Err decoding next packet: {}", _err);
                break;
            }
        };

        // Copy decoded packet into read buffer
        read_buffer.copy_planar_ref(decoded_packet);
    }

    debug!("Precalculated sample count: {}", samples_capacity);
    debug!("Sample count: {}", samples[0].len());
    let count_samples = samples[0].len();
    Ok(Samples {
        samples: Arc::new(samples),
        rate,
        count_samples,
    })
}

#[allow(clippy::expect_used)]
#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_sample_loader() {
        debug!("test");
        let samples = load_samples_from_audio_file(
            "/home/kerle/Musik/Techno/2023/05-Mai-Yuna/Brutalismus 3000 - Good Girl.aiff",
        )
        .expect("Failed loading file");

        println!("samples.len() = {}", samples.samples[0].len());
    }
}
