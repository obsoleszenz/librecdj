#![allow(clippy::expect_used)]
#![allow(clippy::panic)]
use std::{cell::RefCell, ops::Range};

#[derive(Debug, PartialEq)]
pub enum TimeQueueError {
    InsertError(String),
    Full,
    Empty,
}

pub type Result<T> = std::result::Result<T, TimeQueueError>;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum TimeQueueElement<T>
where
    T: Clone + Sized,
{
    Zone(Zone),
    Item(Option<T>),
}

impl<T> TimeQueueElement<T>
where
    T: Copy + Sized,
{
    pub fn from_item(item: T) -> Self {
        Self::Item(Some(item))
    }
    pub fn is_zone(&self) -> bool {
        matches!(self, Self::Zone(_))
    }
}
impl<T> From<Zone> for TimeQueueElement<T>
where
    T: Copy + Sized,
{
    fn from(zone: Zone) -> Self {
        TimeQueueElement::Zone(zone)
    }
}

/// A queue which holds timed items.
/// Time is represended as u32, representing time in samples
/// A queue is divided into different time zones, each zone can
/// hold multiple items with the same time.
struct TimeQueue<T>
where
    T: Copy + Sized,
{
    queue: RefCell<Vec<TimeQueueElement<T>>>,
}

impl<T> TimeQueue<T>
where
    T: Copy + Sized,
{
    pub fn new(size: usize) -> Self {
        let mut time_queue = Self {
            queue: RefCell::new(Vec::with_capacity(size)),
        };
        // Call reset so we insert the Zone 0 element
        time_queue.reset();
        time_queue
    }
    /// Insert a new [`Zone`]
    fn insert_new_zone(&mut self, insert_index: usize, time: u32, item: T) -> Result<usize> {
        let mut queue = self.queue.borrow_mut();
        if queue.len() > queue.capacity() - 2 {
            return Err(TimeQueueError::Full);
        }
        queue.insert(
            insert_index,
            TimeQueueElement::Zone(Zone { time, len_items: 1 }),
        );
        let insert_index_item = insert_index + 1;
        queue.insert(insert_index_item, TimeQueueElement::Item(Some(item)));
        Ok(insert_index_item)
    }
    /// Append an item to existing [`Zone`].
    /// Panics in case [`zone_index`] doesn't point to a [`TimeQueueItem::Zone`]
    fn append_to_zone(&mut self, zone_index: usize, item: T) -> Result<usize> {
        let mut queue = self.queue.borrow_mut();
        if queue.len() > queue.capacity() - 1 {
            return Err(TimeQueueError::Full);
        }
        let insert_index;
        match queue.get_mut(zone_index) {
            Some(TimeQueueElement::Zone(Zone {
                time: _,
                ref mut len_items,
            })) => {
                insert_index = zone_index + *len_items + 1;
                *len_items += 1;
            }
            _ => panic!("Not a zone"),
        }

        queue.insert(insert_index, TimeQueueElement::Item(Some(item)));
        Ok(insert_index)
    }

    fn find_closest_zone(&self, time: u32) -> Option<(usize, Zone)> {
        // Find closest zone
        let mut closest_zone_index: Option<usize> = None;
        for (i, zone) in self
            .queue
            .borrow()
            .iter()
            .enumerate()
            .rev()
            .filter_map(|(index, e)| match e {
                TimeQueueElement::Zone(zone) => Some((index, zone)),
                TimeQueueElement::Item(_) => None,
            })
        {
            closest_zone_index = Some(i);
            if zone.time == time {
                break;
            } else if zone.time < time {
                break;
            }
        }

        let closest_zone_index = match closest_zone_index {
            Some(closest_zone_index) => closest_zone_index,
            None => return None,
        };

        let closest_zone = match self.queue.borrow().get(closest_zone_index) {
            Some(TimeQueueElement::Zone(zone)) => zone.clone(),
            _ => unreachable!("This is a bug. This should always be a Zone element."),
        };

        Some((closest_zone_index, closest_zone))
    }

    /// Insert a new item with a time.
    pub fn insert(&mut self, time: u32, item: T) -> Result<usize> {
        let is_empty = self.queue.borrow().is_empty();
        if is_empty {
            return self.insert_new_zone(0, time, item);
        }

        // Find closest zone
        let (closest_zone_index, closest_zone) = self.find_closest_zone(time).expect("This should not happen. If the queue is not empty there should always be a closest zone");

        // Check if we can append to an existing zone
        if closest_zone.time == time {
            return self.append_to_zone(closest_zone_index, item);
        }

        // We need to insert a new zone
        self.insert_new_zone(closest_zone_index + closest_zone.len_items + 1, time, item)
    }

    /// Get a [`Zone`] struct from the index.
    /// Returns None if no element at this index
    /// Returns None if element at this index is not a [`Zone`]
    pub fn get_zone(&self, zone_index: usize) -> Option<Zone> {
        match self.queue.borrow().get(zone_index) {
            Some(TimeQueueElement::Zone(zone)) => Some(*zone),
            _ => None,
        }
    }

    /// Take an Item at index
    /// Returns None if no element at this index
    /// Returns None if element at this index is not an Item.
    pub fn take_item(&self, index: usize) -> Option<T> {
        match self.queue.borrow().get(index) {
            Some(TimeQueueElement::Item(mut item)) => item.take(),
            _ => None,
        }
    }

    /// Clears queue. Zone with time 0 is still available.
    pub fn reset(&mut self) {
        let mut queue = self.queue.borrow_mut();
        queue.clear();
        queue.insert(0, Zone::new(0, 0).into());
    }

    pub fn inner(&self) -> &RefCell<Vec<TimeQueueElement<T>>> {
        &self.queue
    }

    /// Iter over all [`Slice`] in this Queue
    /// Note: [`Slice`] is a handy abstraction to make working with
    /// this data structure more easy.
    pub fn iter_slices(&self, max_time: u32) -> TimeQueueIterator<T> {
        TimeQueueIterator {
            queue: self,
            zone_iter_pos: 0,
            max_time,
        }
    }
}

pub struct Slice<'a, T>
where
    T: Copy + Sized,
{
    time_start: u32,
    time_end: u32,
    pos_item_start: usize,
    pos_item_end: usize,
    pos_iter: usize,
    queue: &'a TimeQueue<T>,
}

impl<'a, T> Slice<'a, T>
where
    T: Copy + Sized,
{
    /// The start time of this [`Slice`]
    pub fn time_start(&self) -> u32 {
        self.time_start
    }

    /// The end time of this [`Slice`]
    pub fn time_end(&self) -> u32 {
        self.time_end
    }

    /// Returns the time span from start to end
    pub fn time_span(&self) -> (u32, u32) {
        (self.time_start, self.time_end)
    }

    /// Range of the positions of the items in this [`Slice`]
    pub fn item_range(&self) -> Range<usize> {
        self.pos_item_start..self.pos_item_end
    }
}

impl<'a, T> std::iter::Iterator for Slice<'a, T>
where
    T: Copy + Sized,
{
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        match self.queue.take_item(self.pos_item_start + self.pos_iter) {
            Some(item) => {
                self.pos_iter += 1;
                Some(item)
            }
            None => None,
        }
    }
}

pub struct TimeQueueIterator<'a, T>
where
    T: Copy + Sized,
{
    queue: &'a TimeQueue<T>,
    zone_iter_pos: usize,
    max_time: u32,
}

impl<'a, T> std::iter::Iterator for TimeQueueIterator<'a, T>
where
    T: Copy + Sized,
{
    type Item = Slice<'a, T>;

    fn next(&mut self) -> Option<Self::Item> {
        let zone_pos_start = self.zone_iter_pos;
        match self.queue.get_zone(zone_pos_start) {
            Some(zone_start) => {
                let time_start = zone_start.time;
                let pos_item_start = zone_pos_start + 1;
                let pos_item_end = zone_pos_start + zone_start.len_items;
                let next_zone_pos = pos_item_end + 1;
                let next_zone = self.queue.get_zone(next_zone_pos);
                let time_end = next_zone.map(|z| z.time).unwrap_or(self.max_time);
                self.zone_iter_pos = next_zone_pos;

                Some(Slice {
                    time_start,
                    time_end,
                    pos_item_start,
                    pos_item_end,
                    pos_iter: 0,
                    queue: &self.queue,
                })
            }
            None => None,
        }
    }
}

pub struct ZoneIterItem<'a, T>
where
    T: Copy,
{
    queue: &'a TimeQueue<T>,
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub struct Zone {
    pub time: u32,
    pub len_items: usize,
}

impl Zone {
    pub fn new(time: u32, len_items: usize) -> Self {
        Self { time, len_items }
    }
}

#[cfg(test)]
mod tests {
    use std::{borrow::Borrow, ops::Deref};

    use assert_no_alloc::*;
    use chrono::TimeZone;

    use crate::time_queue::{TimeQueue, TimeQueueElement, TimeQueueError, Zone};

    #[cfg(debug_assertions)] // required when disable_release is set (default)
    #[global_allocator]
    static A: AllocDisabler = AllocDisabler;

    #[test]
    fn check_time_queue() {
        let mut queue = TimeQueue::new(7);

        #[derive(Debug, PartialEq, Eq, Copy, Clone)]
        enum Event {
            Foo,
            Bar,
            FooBar,
        }

        assert_no_alloc(|| {
            queue.insert(0, Event::Foo).expect("Failed inserting event");
        });
        assert_eq!(
            queue
                .inner()
                .borrow()
                .iter()
                .map(|e| e.to_owned())
                .collect::<Vec<TimeQueueElement<Event>>>(),
            vec![
                Zone::new(0, 1).into(),
                TimeQueueElement::from_item(Event::Foo)
            ]
        );
        assert_no_alloc(|| {
            queue
                .insert(128, Event::Bar)
                .expect("Failed inserting event");
        });

        assert_eq!(
            queue
                .inner()
                .borrow()
                .iter()
                .map(|e| e.to_owned())
                .collect::<Vec<TimeQueueElement<Event>>>(),
            vec![
                Zone::new(0, 1).into(),
                TimeQueueElement::from_item(Event::Foo),
                Zone::new(128, 1).into(),
                TimeQueueElement::from_item(Event::Bar)
            ]
        );

        assert_no_alloc(|| {
            queue
                .insert(1, Event::FooBar)
                .expect("Failed inserting event");
        });

        assert_eq!(
            queue
                .inner()
                .borrow()
                .iter()
                .map(|e| e.to_owned())
                .collect::<Vec<TimeQueueElement<Event>>>(),
            vec![
                Zone::new(0, 1).into(),
                TimeQueueElement::from_item(Event::Foo),
                Zone::new(1, 1).into(),
                TimeQueueElement::from_item(Event::FooBar),
                Zone::new(128, 1).into(),
                TimeQueueElement::from_item(Event::Bar)
            ]
        );

        // Only 1 slot is free, so inserting an Item in a not yet existing
        // Zone should fail
        assert_no_alloc(|| {
            matches!(queue.insert(130, Event::FooBar), Err(TimeQueueError::Full));
        });

        assert_no_alloc(|| {
            queue
                .insert(0, Event::FooBar)
                .expect("Failed inserting event");
        });

        assert_eq!(
            queue
                .inner()
                .borrow()
                .iter()
                .map(|e| e.to_owned())
                .collect::<Vec<TimeQueueElement<Event>>>(),
            vec![
                Zone::new(0, 2).into(),
                TimeQueueElement::from_item(Event::Foo),
                TimeQueueElement::from_item(Event::FooBar),
                Zone::new(1, 1).into(),
                TimeQueueElement::from_item(Event::FooBar),
                Zone::new(128, 1).into(),
                TimeQueueElement::from_item(Event::Bar)
            ]
        );

        // Queue is full, so this should fail
        assert_no_alloc(|| {
            matches!(queue.insert(0, Event::FooBar), Err(TimeQueueError::Full));
        });

        assert_no_alloc(|| {
            let mut iter = queue.iter_slices(128);

            let mut slice = iter.next().expect("Should exist");
            assert_eq!(slice.time_span(), (0, 1));
            assert_eq!(slice.next(), Some(Event::Foo));
            assert_eq!(slice.next(), Some(Event::FooBar));
            assert_eq!(slice.next(), None);

            let mut slice = iter.next().expect("Should exist");
            assert_eq!(slice.time_span(), (1, 128));
            assert_eq!(slice.next(), Some(Event::FooBar));
            assert_eq!(slice.next(), None);

            let mut slice = iter.next().expect("Should exist");
            assert_eq!(slice.time_span(), (128, 128));
            assert_eq!(slice.next(), Some(Event::Bar));
            assert_eq!(slice.next(), None);
        });

        assert_no_alloc(|| {
            queue.reset();
        });

        assert_eq!(
            queue
                .inner()
                .borrow()
                .iter()
                .map(|e| e.to_owned())
                .collect::<Vec<TimeQueueElement<Event>>>(),
            vec![Zone::new(0, 0).into()]
        );

        assert_no_alloc(|| {
            queue.insert(3, Event::Foo).expect("Should not fail");
            queue.insert(5, Event::Bar).expect("Should not fail");
        });

        assert_eq!(
            queue
                .inner()
                .borrow()
                .iter()
                .map(|e| e.to_owned())
                .collect::<Vec<TimeQueueElement<Event>>>(),
            vec![
                Zone::new(0, 0).into(),
                Zone::new(3, 1).into(),
                TimeQueueElement::from_item(Event::Foo),
                Zone::new(5, 1).into(),
                TimeQueueElement::from_item(Event::Bar),
            ]
        );

        assert_no_alloc(|| {
            let mut iter = queue.iter_slices(128);

            let mut slice = iter.next().expect("Should exist");
            assert_eq!(slice.time_span(), (0, 3));
            assert_eq!(slice.next(), None);

            let mut slice = iter.next().expect("Should exist");
            assert_eq!(slice.time_span(), (3, 5));
            assert_eq!(slice.next(), Some(Event::Foo));
            assert_eq!(slice.next(), None);

            let mut slice = iter.next().expect("Should exist");
            assert_eq!(slice.time_span(), (5, 128));
            assert_eq!(slice.next(), Some(Event::Bar));
            assert_eq!(slice.next(), None);
        });
    }
}
