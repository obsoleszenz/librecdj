use std::collections::VecDeque;

use thiserror::Error;
use tracing::{span, Level};

use crate::{
    command::{Direction, Position, TempoKind},
    queue::Queue,
    ramping::Ramping,
    resampler::{Resampler, MAX_TEMPO_RATIO, MIN_TEMPO_RATIO},
    sample_loader::{SampleLoader, SampleLoaderError, Samples},
    state::{FileInfo, LoopState, PlayerState},
    utils::{crossfaded_fill_buffer, fill_buffer},
    BeatSizeKind, Conditional, CueKind, LoopKind, LoopSize, NudgeKind, PlayerCommand, PlayerEvent,
    ResizeKind, Unit,
};

#[derive(Clone, Copy, Debug)]
struct Nudging {
    backup_tempo: f32,
    backup_play: bool,
    backup_reverse: bool,
}

#[derive(Clone, Copy, Debug)]
struct JogwheelState {
    pub is_scratch: bool,
    pub backup_tempo: f32,
    pub backup_play: bool,
    pub backup_reverse: bool,
}

impl JogwheelState {
    pub fn apply(self, player: &mut Player) {
        player.add_after_effect(AfterEffect::SetPlay(self.backup_play));
        player.add_after_effect(AfterEffect::SetTempo(self.backup_tempo));
        player.add_after_effect(AfterEffect::SetReverse(self.backup_reverse));
    }
}

// AfterEffects are getting executed in the end of [`Player::process()`]
// This allows to modify [`Player`] for the next time process() is getting called.
// This way we can stop/pause without clicks for example.
#[derive(Clone, Copy, Debug)]
enum AfterEffect {
    SetPlay(bool),
    SetPlayposition(i64),
    SetRamping(Ramping),
    SetCuePosition(i64),
    SetCue(bool),
    SetLoopState(Option<LoopState>),
    SetIsNudging(Option<Nudging>),
    SetTempo(f32),
    SetReverse(bool),
    UnsetJogwheelIsPlay(),
}

#[derive(Error, Debug)]
pub enum PlayerError {
    #[error("Error with sample loading: {0:?}")]
    SampleLoader(#[from] SampleLoaderError),
    #[error("Task queue of SampleLoader is full")]
    SampleLoaderTaskQueueFull,
    #[error("Error with resampler: {0:?}")]
    Resampler(#[from] rubato::ResampleError),
}

pub struct Player {
    pub state: PlayerState,
    pub sample_loader: SampleLoader,
    pub resampler: Resampler,

    // Some temporary buffers that we need for resampling & looping
    temp_buffer_a_left: Vec<f32>,
    temp_buffer_a_right: Vec<f32>,

    // Size of frame requested by audio host
    frame_size: usize,
    // Size of currently processed audio chunk
    chunk_size: usize,

    // If set this allows to linearly ramp up or down the
    // volume of the next buffer
    ramping: Option<Ramping>,

    is_nudging: Option<Nudging>,
    is_jogwheel_play: Option<JogwheelState>,

    // Queue of [`AfterEffect`]. See [`AfterEffect`] for more details.
    //
    // Currently this is limited to 16 effects and in case of an
    // overrun the player will panic. We might want to increase this
    // at some point and probably switch to an rtrb.
    after_effects: Queue<AfterEffect, 16>,
    events: Queue<PlayerEvent, 16>,

    command_queue: VecDeque<(usize, PlayerCommand)>,
}

impl Default for Player {
    fn default() -> Self {
        Self::new()
    }
}

const TEMP_BUFFER_SIZE: usize = 8192;

impl Player {
    pub fn new() -> Self {
        // Some temporary buffers that we need
        // The length of 4048 is just a generous assumption
        let temp_buffer_a_left = vec![0.0; TEMP_BUFFER_SIZE];
        let temp_buffer_a_right = vec![0.0; TEMP_BUFFER_SIZE];

        Self {
            state: PlayerState {
                file_info: None,
                tempo: 1.0,
                beat_size: 4.0,
                ..Default::default()
            },
            sample_loader: SampleLoader::new(),
            resampler: Resampler::default(),

            frame_size: 0,
            chunk_size: 0,
            temp_buffer_a_left,
            temp_buffer_a_right,

            ramping: None,
            is_nudging: None,

            is_jogwheel_play: None,

            after_effects: Queue::new(),
            events: Queue::new(),

            command_queue: VecDeque::with_capacity(32),
        }
    }

    /// Helper method to initialize [`Player`] with mono samples.
    /// Used for testing/debugging mainly.
    pub fn from_mono_wave(
        sample_rate_in: usize,
        sample_rate_out: usize,
        chunk_size: usize,
        wave: Vec<f32>,
    ) -> Self {
        let mut player = Player::new();
        let samples = Samples::from_mono(sample_rate_in, wave.clone());

        let sample_loader = SampleLoader::from_samples(samples);
        player.sample_loader = sample_loader;

        let resampler = Resampler::new(sample_rate_in as f64, sample_rate_out as f64, chunk_size);
        player.resampler = resampler;
        player.state.file_info = Some(FileInfo {
            sample_rate: sample_rate_in,
            count_samples: wave.len(),
        });

        player
    }

    /// Proceed the player & fill the buffer
    /// This method is realtime safe, and if not it's a bug.
    pub fn proceed_and_fill_buffer(
        &mut self,
        buffer_left: &mut [f32],
        buffer_right: &mut [f32],
        sample_rate_out: usize,
        frame_size: usize,
    ) -> Result<(), PlayerError> {
        let _span = span!(Level::TRACE, "libplayer");
        // debug!("proceed_and_fill_buffer()");

        self.frame_size = frame_size;
        //trace!("frame_size: {}", frame_size);

        let mut wrote_already = false;
        let mut start_pos = 0;
        let mut end_pos = frame_size;
        macro_rules! write_audio_samples {
            () => {{
                self.process_slice(
                    sample_rate_out,
                    &mut buffer_left[start_pos..end_pos],
                    &mut buffer_right[start_pos..end_pos],
                );
                wrote_already = true;
            }};
        };

        // We iterate over the command queue and slice the frame.
        // Imagine we have a queue with [(128, Command::A),(128, Command::B), (251, Command:C)]
        // This would slice the frame into following parts:
        // - 0..128
        // - 128..251 apply Command::A & B
        // - 251..frame_size Command::C
        loop {
            let (time, command) = match self.command_queue.pop_front() {
                Some(r) => r,
                None => {
                    // No more commands, so write last chunk
                    end_pos = frame_size;
                    write_audio_samples!();
                    break;
                }
            };

            let time = if time > frame_size {
                warn!("Received a time bigger then frame size, aligning it to current chunk");
                start_pos
            } else {
                time
            };

            if time != 0 && !wrote_already {
                // First command is not at time 0 so we first need to write a good chunk
                start_pos = 0;
                end_pos = time;
                write_audio_samples!();
            } else if time > start_pos {
                // Popped command is of next slice. So write this slice to buffer.
                end_pos = time;
                write_audio_samples!();
            }
            start_pos = time;

            self.apply_command(command)
        }
        Ok(())
    }

    /// This is not real time safe, but as we call it in situations where can anyways not do
    /// anything we accept possible click sounds
    fn resize_temp_buffers(&mut self, new_size: usize) {
        warn!("Resizing temp buffers, this might cause xruns");
        self.temp_buffer_a_left.resize(new_size, 0.0);
        self.temp_buffer_a_right.resize(new_size, 0.0);
    }

    /// Write the resampled samples for this slice
    fn process_slice(
        &mut self,
        // Sample rate requested by audio host
        sample_rate_out: usize,
        buffer_slice_left: &mut [f32],
        buffer_slice_right: &mut [f32],
    ) {
        // Figure out the chunk_size
        // TODO: Rename this to slice_size ?
        let chunk_size = buffer_slice_left.len();
        self.chunk_size = chunk_size;

        // Guard against writing zero samples
        if chunk_size == 0 {
            return;
        }

        // Update the sample loader if needed
        if self.sample_loader.update() {
            debug!("Updating sample loader");
            // Sample loader got updated, apply file info to our state
            self.state.file_info = Some(FileInfo {
                count_samples: self.sample_loader.samples.as_ref().unwrap().count_samples,
                sample_rate: self.sample_loader.samples.as_ref().unwrap().rate,
            });
            // Also enable ramping to make sure we are not getting any clicks
            self.ramping = Some(Ramping::Up);

            // Also add event for consumers of libplayer
            self.events.push(PlayerEvent::FileLoaded).unwrap();
        }

        let sample_rate_in = match &self.sample_loader.samples {
            Some(samples) => samples.rate,
            // Guard against file not loaded
            None => {
                // Fill buffer with zeros
                buffer_slice_left.fill(0.0);
                buffer_slice_right.fill(0.0);

                self.apply_all_after_effects();
                // warn!("File not loaded yet, filled buffer with zeros. Returning.");
                return;
            }
        };

        // We are not playing, so fill buffer with zeros and return
        if !self.state.play {
            buffer_slice_left.fill(0.0);
            buffer_slice_right.fill(0.0);

            self.apply_all_after_effects();
            // warn!("state.play is false, filled buffer with zeros. Returning.");
            return;
        }

        #[allow(clippy::expect_used)]
        self.resampler
            .set_sample_rate(sample_rate_in as f64, sample_rate_out as f64)
            .expect("Failed updating resampling ratio");

        // Update resampler to currently used chunk_size
        #[allow(clippy::expect_used)]
        self.resampler
            .set_chunk_size(chunk_size)
            .expect("Failed resizing chunk_size of resampler");

        // Check if resampler is ready and retrieve the needed sizes for our next frames
        let (input_frames_next, _output_frames_next) = self.resampler.frames_next();

        // Make sure the requested frame size is smaller then our temp buffers
        if input_frames_next > self.temp_buffer_a_left.len() {
            error!("Requested bigger frame then our temp buffer can hold!!");
            self.resize_temp_buffers(input_frames_next);
        }

        #[allow(clippy::expect_used)]
        let samples = self.sample_loader.samples.as_ref().expect(
            "Unreachable: sample_loader.samples was None which should not happen at this point",
        );

        let mut force_set_playhead: Option<i64> = None;

        let position = self.state.playhead_samples;
        match self.state.reverse {
            // Play track in normal play direction
            false => {
                // Handle loops
                let mut buffer_already_filled = false;
                if let Some(LoopState {
                    start,
                    end,
                    enabled,
                }) = self.state.looping
                {
                    if enabled && position >= start && position <= end {
                        // Looping is enabled & and playhead position is inside the loop.
                        // This means we might need to wrap the loop
                        debug!(
                            "Check loop wrapping: position={} frame_size={} end={}",
                            position, input_frames_next, end
                        );
                        let distance_to_end: i64 = end - position;
                        if distance_to_end >= 0 && distance_to_end <= input_frames_next as i64 {
                            // Yes we have to
                            debug!("For looping we have to wrap {} samples", distance_to_end);
                            let start_a = position;
                            let start_b = start - distance_to_end;

                            crossfaded_fill_buffer(
                                start_a,
                                start_b,
                                input_frames_next,
                                &samples.samples[0],
                                &mut self.temp_buffer_a_left,
                            );
                            crossfaded_fill_buffer(
                                start_a,
                                start_b,
                                input_frames_next,
                                &samples.samples[1],
                                &mut self.temp_buffer_a_right,
                            );

                            // Don't fill buffer again
                            buffer_already_filled = true;

                            // Set playheader to position + outputted samples
                            force_set_playhead = Some(start_b);
                        }
                    }
                }

                if !buffer_already_filled {
                    // Fill the temp buffer with the next frame
                    log_error_ok!(
                        fill_buffer(
                            position,
                            input_frames_next,
                            &samples.samples[0],
                            &mut self.temp_buffer_a_left,
                        ),
                        "Failed filing left buffer when buffer is not already filled. Error: {:?}"
                    );
                    log_error_ok!(
                        fill_buffer(
                            position,
                            input_frames_next,
                            &samples.samples[1],
                            &mut self.temp_buffer_a_right,
                        ),
                        "Failed filing right buffer when buffer is not already filled. Error: {:?}"
                    );
                }

                if let Ok((count_input_samples, _count_output_samples)) =
                    self.resampler.process_into_buffer(
                        &[
                            &self.temp_buffer_a_left[..input_frames_next],
                            &self.temp_buffer_a_right[..input_frames_next],
                        ],
                        &mut [buffer_slice_left, buffer_slice_right],
                    )
                {
                    // Increment position
                    self.state.playhead_samples =
                        if let Some(force_set_playhead) = force_set_playhead {
                            force_set_playhead + count_input_samples as i64
                        } else {
                            self.state.playhead_samples + count_input_samples as i64
                        };
                    // debug!(
                    //     "Incremented playhead_samples to: {}",
                    //     self.state.playhead_samples
                    // );
                }
            }
            // Play track in reverse direction
            true => {
                log_error_ok!(
                    fill_buffer(
                        position - input_frames_next as i64,
                        input_frames_next,
                        &samples.samples[0],
                        &mut self.temp_buffer_a_left,
                    ),
                    "Failed filling left buffer when playing in reverse direction. Err: {:?}"
                );
                log_error_ok!(
                    fill_buffer(
                        position - input_frames_next as i64,
                        input_frames_next,
                        &samples.samples[1],
                        &mut self.temp_buffer_a_right,
                    ),
                    "Failed filling right buffer when playing in reverse direction. Err: {:?}"
                );

                // We want to play backwards so reverse the temp buffers
                self.temp_buffer_a_left.reverse();
                self.temp_buffer_a_right.reverse();

                // Because of reversing the temp buffer, our samples
                // are now at the end of the buffer so we calculate the end
                // range and create properly sliced left & right buffers.
                let start = self.temp_buffer_a_left.len() - input_frames_next;
                let end = start + input_frames_next;
                let left_buffer = &self.temp_buffer_a_left[start..end];
                let right_buffer = &self.temp_buffer_a_right[start..end];

                // Resample the left & right buffer into our output buffers
                // TODO: give output buffers better names
                if let Ok((count_input_samples, _count_output_samples)) =
                    self.resampler.process_into_buffer(
                        &[left_buffer, right_buffer],
                        &mut [buffer_slice_left, buffer_slice_right],
                    )
                {
                    // Decrement position
                    self.state.playhead_samples -= count_input_samples as i64;
                }
            }
        }
        // Apply any ramping
        if let Some(ramping) = self.ramping.take() {
            debug!("Applying ramping to buffer: {:?}", ramping);
            ramping.apply(buffer_slice_left);
            ramping.apply(buffer_slice_right);
        }
        self.apply_all_after_effects();
    }

    // Add an [`AfterEffect`] to the after effects queue
    fn add_after_effect(&mut self, effect: AfterEffect) {
        if let Err(_err) = self.after_effects.push(effect) {
            error!(
                "After effect queue is already full! {:?}",
                self.after_effects
            );
        }
    }

    // This method should always be called after an audio slice got written
    fn apply_all_after_effects(&mut self) {
        // Execute all after effects
        // debug!("Executing after effects: {:?}", self.after_effects);
        while let Some(after_effect) = self.after_effects.pop() {
            self.apply_after_effect(&after_effect);
        }
        // trace!("{:?}", self.after_effects);
    }

    /// Applies an [`AfterEffect`]
    fn apply_after_effect(&mut self, effect: &AfterEffect) {
        match effect {
            // Stop happens always with one full buffer delay
            // So that we can properly ramp out
            AfterEffect::SetPlay(play) => self.state.play = *play,
            AfterEffect::SetPlayposition(play_position) => {
                debug!("AfterEffect::SetPlayposition({})", play_position);
                self.state.playhead_samples = *play_position
            }
            AfterEffect::SetRamping(ramping) => self.ramping = Some(*ramping),
            AfterEffect::SetCuePosition(cue_position) => self.state.cue_pos = *cue_position,
            AfterEffect::SetCue(cue) => self.state.cue = *cue,
            AfterEffect::SetLoopState(loop_state) => self.state.looping = *loop_state,
            AfterEffect::SetIsNudging(is_nudging) => self.is_nudging = *is_nudging,
            AfterEffect::SetTempo(tempo) => self.set_tempo(*tempo, false),
            AfterEffect::SetReverse(reverse) => self.state.reverse = *reverse,
            AfterEffect::UnsetJogwheelIsPlay() => self.is_jogwheel_play = None,
        }
    }

    /// Update the tempo in state and resampler
    pub fn set_tempo(&mut self, tempo: f32, ramp: bool) {
        if tempo > MAX_TEMPO_RATIO {
            warn!("Setting tempo greater then MAX_TEMPO_RATIO");
        }
        if tempo < MIN_TEMPO_RATIO {
            warn!("Setting tempo smaller then MIN_TEMPO_RATIO");
        }
        let tempo = tempo.max(MIN_TEMPO_RATIO).min(MAX_TEMPO_RATIO);
        self.state.tempo = tempo;

        #[allow(clippy::expect_used)]
        self.resampler
            .set_tempo(tempo as f64, ramp)
            .expect("Failed applying tempo");
    }

    pub fn event(&mut self) -> Option<PlayerEvent> {
        self.events.pop()
    }

    /// Pushed an command with it's timestamp to the internal [`TimeQueue`]
    pub fn command(&mut self, time: u32, command: PlayerCommand) {
        self.command_queue.push_back((time as usize, command))
    }

    /// Apply command. This changes the internal state to reflect whatever
    /// this command needs.
    fn apply_command(&mut self, command: PlayerCommand) {
        debug!("Processing {:?}", command);
        match command {
            PlayerCommand::Seek(position) => {
                let sample_rate = match self.state.file_info {
                    Some(file_info) => file_info.sample_rate,
                    None => {
                        error!("No file loaded yet!");
                        return;
                    }
                };

                match position {
                    Position::Absolute(position) => {
                        self.state.playhead_samples =
                            position.to_sample(sample_rate, self.state.initial_bpm());
                    }
                    Position::Relative(position) => {
                        self.state.playhead_samples +=
                            position.to_sample(sample_rate, self.state.initial_bpm());
                    }
                }
            }
            PlayerCommand::Play(play, position) => {
                let sample_rate = if let Some(file_info) = self.state.file_info {
                    file_info.sample_rate
                } else {
                    error!("Failed playing from position as current deck is not loaded");
                    return;
                };

                // Handle playing from a specific position
                match (play, position) {
                    (Some(true), Some(position)) => {
                        self.state.playhead_samples =
                            position.to_sample(sample_rate, self.state.initial_bpm());
                    }
                    (Some(false), None) => {
                        warn!("Unnecessairily passed position parameter on pause, ignoring");
                    }
                    _ => (),
                }

                // Always reset is_jogwheel_play
                if let Some(jogwheel_state) = self.is_jogwheel_play {
                    debug!("We are scratching so reset player state to before scratching");
                    self.add_after_effect(AfterEffect::SetTempo(jogwheel_state.backup_tempo));
                    self.add_after_effect(AfterEffect::SetReverse(jogwheel_state.backup_reverse));
                    self.add_after_effect(AfterEffect::UnsetJogwheelIsPlay())
                }

                let play = match play {
                    Some(play) => play,
                    None => self.state.cue || !self.state.play,
                };

                // Make sure we properly ramp next time we process_and_fill
                match play {
                    true => self.ramping = Some(Ramping::Up),
                    false => self.ramping = Some(Ramping::Down),
                }

                if play && self.state.cue {
                    // Disable cue if we are playing
                    self.state.cue = false;

                    // Also don't ramp as we are already playing
                    self.ramping = None;
                }

                match play {
                    true => {
                        self.state.play = true;
                    }
                    false => self.add_after_effect(AfterEffect::SetPlay(false)),
                };
            }
            PlayerCommand::Reverse(reverse) => {
                self.state.reverse = reverse;
            }
            PlayerCommand::Tempo(tempo_kind) => {
                match tempo_kind {
                    TempoKind::Absolute(tempo_absolute) => {
                        self.state.tempo = tempo_absolute;
                    }
                    TempoKind::Relative(tempo_relative) => {
                        self.state.tempo += tempo_relative;
                    }
                    TempoKind::Percent(tempo_percent) => {
                        self.state.tempo *= 1.0 + tempo_percent;
                    }
                }
                self.set_tempo(self.state.tempo + self.state.jogwheel_tempo, true);
            }
            PlayerCommand::Load { path, play } => match self.sample_loader.load(path) {
                Ok(()) => {
                    self.ramping = Some(Ramping::Down);
                    self.add_after_effect(AfterEffect::SetPlayposition(0));

                    self.add_after_effect(AfterEffect::SetPlay(play.unwrap_or(false)));
                    self.add_after_effect(AfterEffect::SetCue(false));
                    self.add_after_effect(AfterEffect::SetCuePosition(0));
                    self.add_after_effect(AfterEffect::SetLoopState(None));
                    self.add_after_effect(AfterEffect::SetIsNudging(None))
                }
                #[allow(unused)]
                Err(err) => {
                    error!("Failed loading file to sample loader: {:?}", err)
                }
            },
            PlayerCommand::Cue(cue_kind, position) => match cue_kind {
                CueKind::Press => {
                    if self.state.cue {
                        return;
                    }

                    let is_scratching = if let Some(jogwheel_state) = self.is_jogwheel_play {
                        jogwheel_state.apply(self);
                        jogwheel_state.is_scratch
                    } else {
                        false
                    };
                    if let Some(position) = position {
                        if let Some(file_info) = self.state.file_info {
                            self.state.cue_pos =
                                position.to_sample(file_info.sample_rate, self.state.initial_bpm());
                        } else {
                            error!("Failed setting cue pos as deck is currently not loaded, setting cue to current playhead");
                            self.state.cue_pos = self.state.playhead_samples;
                        }
                    } else if !is_scratching && self.state.play {
                        self.state.playhead_samples = self.state.cue_pos;
                    } else {
                        self.state.cue_pos = self.state.playhead_samples;
                    }
                    self.ramping = Some(Ramping::Up);
                    self.state.cue = true;
                    self.state.play = true;
                }
                CueKind::Release => {
                    match self.state.cue {
                        true => {
                            // Reset play position to cue position
                            self.state.cue = false;

                            self.ramping = Some(Ramping::Down);
                            self.add_after_effect(AfterEffect::SetPlay(false));
                            self.add_after_effect(AfterEffect::SetPlayposition(self.state.cue_pos));
                        }
                        false => {
                            // Nothing to do
                        }
                    }
                }
                CueKind::Reset => match self.state.play {
                    true => {
                        self.state.cue = false;
                        self.ramping = Some(Ramping::Down);

                        self.add_after_effect(AfterEffect::SetPlayposition(self.state.cue_pos));
                        self.add_after_effect(AfterEffect::SetRamping(Ramping::Up))
                    }
                    false => {
                        self.state.playhead_samples = self.state.cue_pos;
                        self.state.cue = false;
                    }
                },
            },

            PlayerCommand::Nudge(nudge_kind) => match nudge_kind {
                NudgeKind::Enable {
                    direction,
                    tempo_playing,
                    tempo_paused,
                } => {
                    // Make sure that we are not nudging already
                    if self.is_nudging.is_some() {
                        error!("We are already nudging!");
                        return;
                    }

                    // Backup current parameters that we might affect
                    let backup_tempo = self.state.tempo;
                    let backup_play = self.state.play;
                    let backup_reverse = self.state.reverse;
                    self.is_nudging = Some(Nudging {
                        backup_tempo,
                        backup_play,
                        backup_reverse,
                    });

                    let tempo = if self.state.play {
                        // Calculate tempo
                        let relative_tempo = match direction {
                            Direction::Forward => 1.0 + tempo_playing,
                            Direction::Backward => 1.0 - tempo_playing,
                        };

                        self.state.tempo * relative_tempo
                    } else {
                        self.state.reverse = direction.is_reverse();

                        self.state.play = true;

                        // Enable ramping to prevent clicks
                        self.ramping = Some(Ramping::Up);

                        // Calculate tempo
                        let relative_tempo = 1.0 - tempo_paused;
                        self.state.tempo * relative_tempo
                    };

                    self.set_tempo(tempo, true);
                }
                NudgeKind::Disable => {
                    if let Some(Nudging {
                        backup_tempo,
                        backup_play,
                        backup_reverse,
                    }) = self.is_nudging.take()
                    {
                        if !backup_play {
                            self.ramping = Some(Ramping::Down);
                            self.add_after_effect(AfterEffect::SetPlay(false));
                        }
                        self.state.reverse = backup_reverse;

                        self.set_tempo(backup_tempo, true);
                    }
                }
            },
            PlayerCommand::Loop(loop_kind) => match loop_kind {
                LoopKind::Set(conditional, loop_position) => {
                    let sample_rate = match self.state.file_info {
                        Some(file_info) => file_info.sample_rate,
                        None => {
                            error!("Cannot set loop size as no file is loaded yet!");
                            return;
                        }
                    };

                    let enabled = match conditional {
                        Conditional::Enable => true,
                        Conditional::Disable => false,
                        Conditional::Toggle => {
                            if let Some(loop_state) = self.state.looping {
                                !loop_state.enabled
                            } else {
                                true
                            }
                        }
                    };
                    match loop_position {
                        LoopSize::Absolute { position, length } => {
                            let loop_length_sample =
                                length.to_sample(sample_rate, self.state.initial_bpm());
                            let (start, end) = if loop_length_sample > 0 {
                                let start =
                                    position.to_sample(sample_rate, self.state.initial_bpm());
                                let end =
                                    start + length.to_sample(sample_rate, self.state.initial_bpm());
                                (start, end)
                            } else {
                                let end = position.to_sample(sample_rate, self.state.initial_bpm());
                                let start = end - loop_length_sample.abs();
                                (start, end)
                            };
                            self.state.looping = Some(LoopState {
                                start,
                                end,
                                enabled,
                            })
                        }
                        LoopSize::Relative(length) => {
                            let loop_length_sample =
                                length.to_sample(sample_rate, self.state.initial_bpm());
                            let (start, end) = if loop_length_sample > 0 {
                                let start = self.state.playhead_samples;
                                let end = start.saturating_add(loop_length_sample);
                                (start, end)
                            } else {
                                let end = self.state.playhead_samples;
                                let start = end.saturating_sub(loop_length_sample.abs());
                                (start, end)
                            };
                            self.state.looping = Some(LoopState {
                                start,
                                end,
                                enabled,
                            })
                        } // SetKind::Remove => {
                          //     self.state.looping.take();
                          // }
                    }
                }
                LoopKind::Enabled(condition) => {
                    if let Some(loop_state) = self.state.looping.as_mut() {
                        loop_state.enabled = match condition {
                            Conditional::Enable => true,
                            Conditional::Disable => false,
                            Conditional::Toggle => !loop_state.enabled,
                        };
                    } else {
                        error!("No loop set so can't enable!");
                    }
                }
                LoopKind::Remove => {
                    self.state.looping.take();
                }
                LoopKind::Resize(resize_kind) => {
                    let initial_bpm = self.state.initial_bpm();
                    if let Some(loop_state) = self.state.looping.as_mut() {
                        match resize_kind {
                            // TODO: Reposition playhead
                            ResizeKind::Multiply(multiplier) => {
                                let length =
                                    (loop_state.end - loop_state.start) as f64 * multiplier;
                                loop_state.end = loop_state.start + length as i64;
                            }
                            ResizeKind::Length(length) => {
                                let sample_rate = match self.state.file_info {
                                    Some(file_info) => file_info.sample_rate,
                                    None => {
                                        error!("No file loaded yet!");
                                        return;
                                    }
                                };

                                loop_state.end =
                                    loop_state.start + length.to_sample(sample_rate, initial_bpm);
                            }
                        }
                    } else {
                        error!("No loop set so can't resize!");
                    }
                }
                LoopKind::Move(move_by) => {
                    let sample_rate = match self.state.file_info {
                        Some(file_info) => file_info.sample_rate,
                        None => {
                            error!("No file loaded yet so can't move!");
                            return;
                        }
                    };
                    let move_by_samples = move_by.to_sample(sample_rate, self.state.initial_bpm());
                    if let Some(loop_state) = self.state.looping.as_mut() {
                        loop_state.start += move_by_samples;
                        loop_state.end += move_by_samples;
                        // TODO: Reposition playhead
                    }
                    self.state.playhead_samples += move_by_samples;
                }
            },
            PlayerCommand::Slip(_, _) => todo!(),
            PlayerCommand::JogWheel(jogwheel) => {
                macro_rules! set_absolute_jog_tempo {
                    ($ramp:expr) => {
                        self.state.reverse = !jogwheel.direction_forward;
                        self.set_tempo(0.1 + jogwheel.interval.abs() as f32 * 128.0, $ramp)
                    };
                    () => {
                        set_absolute_jog_tempo!(true)
                    };
                }
                macro_rules! set_relative_jog_tempo {
                    ($ramp:expr) => {
                        let original_tempo = match self.is_jogwheel_play {
                            Some(jogwheel) => jogwheel.backup_tempo,
                            None => self.state.tempo,
                        };
                        self.set_tempo(original_tempo + jogwheel.interval as f32 * 16.0, $ramp)
                    };
                    () => {
                        set_relative_jog_tempo!(true)
                    };
                }
                match self.is_jogwheel_play {
                    Some(_jogwheel_state) if jogwheel.interval == 0.0 && jogwheel.is_scratching => {
                        debug!("Stop platter");
                        self.ramping = Some(Ramping::Down);
                        self.add_after_effect(AfterEffect::SetPlay(false));
                    }
                    Some(jogwheel_state) if jogwheel.interval == 0.0 && !jogwheel.is_scratching => {
                        // Jogwheel stopped
                        debug!("Stop platter2");
                        if !jogwheel_state.backup_play {
                            self.add_after_effect(AfterEffect::SetPlay(false));
                            self.ramping = Some(Ramping::Down);
                        } else if !self.state.play {
                            self.ramping = Some(Ramping::Up);
                            self.state.play = true;
                        }
                        self.state.reverse = jogwheel_state.backup_reverse;

                        self.is_jogwheel_play = None;
                        debug!("Joghweel stoopppiiit");

                        self.add_after_effect(AfterEffect::SetTempo(jogwheel_state.backup_tempo));
                    }
                    Some(JogwheelState {
                        ref mut is_scratch, ..
                    }) if jogwheel.is_scratching => {
                        debug!("Still scratching or just start scratching");
                        *is_scratch = jogwheel.is_scratching;
                        if !self.state.play {
                            self.ramping = Some(Ramping::Up);
                        }
                        self.state.play = true;
                        set_absolute_jog_tempo!(true);
                    }
                    Some(JogwheelState {
                        is_scratch: true, ..
                    }) if !jogwheel.is_scratching => {
                        debug!("We are still scratching but scratching stopped. We just stay in scratch mode");
                        set_absolute_jog_tempo!();
                    }
                    Some(JogwheelState {
                        is_scratch: false,
                        backup_play: false,
                        ..
                    }) => {
                        // TODO: What exactly is the case we handling here?
                        debug!("Just apply tempo");
                        set_absolute_jog_tempo!(true);
                    }
                    Some(JogwheelState {
                        is_scratch: false, ..
                    }) if !jogwheel.is_scratching => {
                        debug!("Just apply tempo2");
                        set_relative_jog_tempo!(true);
                    }
                    None if self.state.play => {
                        debug!("We are playing and now we get the first jog event");
                        if jogwheel.is_scratching {
                            // We are scratching so change the tempo to the absolute value
                            self.is_jogwheel_play = Some(JogwheelState {
                                is_scratch: true,
                                backup_tempo: self.state.tempo,
                                backup_play: self.state.play,
                                backup_reverse: self.state.reverse,
                            });
                            // Also change the direction in case we have to
                            if jogwheel.interval != 0.0 {
                                set_absolute_jog_tempo!();
                            } else {
                                self.ramping = Some(Ramping::Down);
                                self.add_after_effect(AfterEffect::SetPlay(false));
                            }
                        } else {
                            // No scratching so just modulate the tempo
                            self.is_jogwheel_play = Some(JogwheelState {
                                is_scratch: false,
                                backup_tempo: self.state.tempo,
                                backup_play: self.state.play,
                                backup_reverse: self.state.reverse,
                            });
                            set_relative_jog_tempo!(true);
                        }
                    }
                    None if !self.state.play => {
                        debug!("We are not playing and this is the first jog event");
                        self.is_jogwheel_play = Some(JogwheelState {
                            is_scratch: jogwheel.is_scratching,
                            backup_tempo: self.state.tempo,
                            backup_play: self.state.play,
                            backup_reverse: self.state.reverse,
                        });
                        if jogwheel.interval != 0.0 {
                            self.state.play = true;
                            self.ramping = Some(Ramping::Up);
                            set_absolute_jog_tempo!(false);
                        }
                    }
                    _ => unreachable!(),
                }
            }
            PlayerCommand::SetMetadata(metadata) => {
                debug!("SetMetadata metadata={metadata:?}");
                self.state.metadata = metadata;
            }
            PlayerCommand::BeatSize(beat_size_kind) => match beat_size_kind {
                BeatSizeKind::Jump {
                    direction,
                    with_loop,
                } => {
                    let jump_beats = Unit::Beat(self.state.beat_size * direction.to_sign_f32());
                    if self.state.looping.is_some() && with_loop {
                        self.apply_command(PlayerCommand::Loop(LoopKind::Move(jump_beats)))
                    } else {
                        self.apply_command(PlayerCommand::Seek(Position::Relative(jump_beats)))
                    }
                }
                BeatSizeKind::Resize(resize_kind) => {
                    let beat_size = match resize_kind {
                        ResizeKind::Multiply(factor) => self.state.beat_size * factor as f32,
                        ResizeKind::Length(length) => {
                            if let Some(initial_sample_rate) = self.state.initial_sample_rate() {
                                length.to_beats(initial_sample_rate, self.state.initial_bpm())
                            } else {
                                error!("Cant set beat size without knowing the sample rate");
                                return;
                            }
                        }
                    };
                    self.state.beat_size = beat_size;
                    if self.state.looping.is_some() {
                        self.apply_command(PlayerCommand::Loop(LoopKind::Resize(resize_kind)));
                    }
                }
                BeatSizeKind::SetLoop(direction) => {
                    let length = Unit::Beat(self.state.beat_size * direction.to_sign_f32());
                    self.apply_command(PlayerCommand::Loop(LoopKind::Set(
                        Conditional::Enable,
                        LoopSize::Relative(length),
                    )))
                }
            },
        }
    }

    pub fn clone_from_player(&mut self, clone_player: &Player) -> Result<(), PlayerError> {
        clone_player.state.clone_into(&mut self.state);
        self.sample_loader
            .load_samples(&clone_player.sample_loader.samples)?;
        self.resampler
            .set_tempo(clone_player.state.tempo as f64, false)?;
        Ok(())
    }

    // Check if we are playing
    pub fn playing(&self) -> bool {
        self.state.play && self.state.file_info.is_some()
    }

    // Check if we are cueing
    pub fn cueing(&self) -> bool {
        self.state.cue
    }

    // Check if the playhead is in a loop (loop needs to be enabled)
    pub fn in_loop(&self) -> bool {
        if let Some(looping) = &self.state.looping {
            return looping.enabled
                && looping.start <= self.state.playhead_samples
                && looping.end >= self.state.playhead_samples;
        }
        false
    }

    // Check if the playhead is before loop (loop needs to be enabled)
    pub fn before_loop(&self) -> bool {
        if let Some(looping) = &self.state.looping {
            return looping.enabled && looping.start > self.state.playhead_samples;
        }
        false
    }
}

#[allow(clippy::expect_used)]
#[cfg(test)]
mod test {
    use crate::{Conditional, LoopKind, LoopSize, Player, PlayerCommand, Position, Unit};

    use test_log::test;
    use wavegen::{wf, PeriodicFunction};

    #[test]
    #[allow(non_snake_case)]
    fn should_properly_ramp_first_buffer() {
        let RATE: usize = 44100;
        let BUFFER_SIZE: usize = 1024;

        let wave = wf!(f32, RATE as f32, PeriodicFunction::custom(|_| 1.0))
            .iter()
            .take(2 * BUFFER_SIZE)
            .collect::<Vec<f32>>();
        let mut player = Player::from_mono_wave(RATE, RATE, BUFFER_SIZE, wave.clone());

        let mut buffer_left = vec![0.0; BUFFER_SIZE];
        let mut buffer_right = vec![0.0; BUFFER_SIZE];

        player.apply_command(PlayerCommand::Play(Some(true), None));
        player
            .proceed_and_fill_buffer(&mut buffer_left, &mut buffer_right, RATE, BUFFER_SIZE)
            .expect("proceed failed");

        println!("{:?}", buffer_left);

        // First buffer should be ramped
        let ramping_step_size = 1.0 / ((BUFFER_SIZE - 1) as f32);
        println!("ramping_step_size={:?}", ramping_step_size);
        for i in 0..BUFFER_SIZE {
            let ramped_wave_sample = wave[i] * (ramping_step_size * (i as f32));
            println!(
                "i: {} wave[i] (ramped): {} left[i]: {} right[i]: {}",
                i, ramped_wave_sample, buffer_left[i], buffer_right[i]
            );
            assert_eq!(
                buffer_left[i], ramped_wave_sample,
                "Left sample not properly ramped"
            );
            assert_eq!(
                buffer_right[i], ramped_wave_sample,
                "Right sample not properly ramped"
            );
        }

        player
            .proceed_and_fill_buffer(&mut buffer_left, &mut buffer_right, RATE, BUFFER_SIZE)
            .expect("proceed failed");

        // Second buffer shouldn't be ramped
        for i in 0..BUFFER_SIZE {
            let sample = wave[BUFFER_SIZE + i];
            println!(
                "i: {} wave[BUFFER_SIZE+i]: {} left[i]: {} right[i]: {}",
                i, sample, buffer_left[i], buffer_right[i]
            );
            assert_eq!(buffer_left[i], sample);
            assert_eq!(buffer_right[i], sample);
        }

        println!("{:?}", buffer_left);
    }

    #[test]
    #[allow(non_snake_case)]
    fn should_properly_loop() {
        let RATE: usize = 44100;
        let BUFFER_SIZE: usize = 64;

        // Generate 1 minute of a sine wave with BPM / 60.0hz
        let wave = wf!(f32, RATE as f32, PeriodicFunction::custom(|_| 1.0))
            .iter()
            .take(BUFFER_SIZE * 4)
            .collect::<Vec<f32>>();
        let mut player = Player::from_mono_wave(RATE, RATE, BUFFER_SIZE, wave.clone());

        let mut buffer_left = vec![0.0; BUFFER_SIZE];
        let mut buffer_right = vec![0.0; BUFFER_SIZE];

        player.apply_command(PlayerCommand::Play(Some(true), None));
        player.apply_command(PlayerCommand::Loop(LoopKind::Set(
            Conditional::Enable,
            LoopSize::Relative(Unit::Sample((BUFFER_SIZE * 3) as i64)),
        )));

        player
            .proceed_and_fill_buffer(&mut buffer_left, &mut buffer_right, RATE, BUFFER_SIZE)
            .expect("proceed failed");

        println!("{:?}", buffer_left);

        // First buffer should be ramped
        let ramping_step_size = 1.0 / (BUFFER_SIZE - 1) as f32;
        for i in 0..BUFFER_SIZE {
            let ramped_wave_sample = wave[i] * (ramping_step_size * i as f32);
            println!(
                "i: {} wave[i] (ramped): {} left[i]: {} right[i]: {}",
                i, ramped_wave_sample, buffer_left[i], buffer_right[i]
            );
            assert_eq!(
                buffer_left[i], ramped_wave_sample,
                "Left sample not properly ramped"
            );
            assert_eq!(
                buffer_right[i], ramped_wave_sample,
                "Right sample nto properly ramped"
            );
        }

        player
            .proceed_and_fill_buffer(&mut buffer_left, &mut buffer_right, RATE, BUFFER_SIZE)
            .expect("proceed failed");

        // Second buffer shouldn't be ramped
        for i in 0..BUFFER_SIZE {
            let sample = wave[BUFFER_SIZE + i];
            println!(
                "i: {} wave[BUFFER_SIZE+i]: {} left[i]: {} right[i]: {}",
                i, sample, buffer_left[i], buffer_right[i]
            );
            assert_eq!(buffer_left[i], sample);
            assert_eq!(buffer_right[i], sample);
        }

        println!("{:?}", buffer_left);

        player
            .proceed_and_fill_buffer(&mut buffer_left, &mut buffer_right, RATE, BUFFER_SIZE)
            .expect("proceed failed");

        // Third buffer shouldn't be ramped
        for i in 0..BUFFER_SIZE {
            let ramped_wave_sample =
                wave[2 * BUFFER_SIZE + i] * (1.0 - (ramping_step_size as f64 * i as f64) as f32);
            println!(
                "i: {} wave[2*BUFFER_SIZE+i] (ramped): {} left[i]: {} right[i]: {}",
                i, ramped_wave_sample, buffer_left[i], buffer_right[i]
            );
            let DELTA = 0.0000000000000001;
            assert_delta!(buffer_left[i], ramped_wave_sample + 1.0, DELTA);
            assert_delta!(buffer_right[i], ramped_wave_sample, DELTA);
        }

        println!("{:?}", buffer_left);
    }

    //#[test]
    #[allow(non_snake_case)]
    fn should_reset_player_after_loading_another_file() {
        let RATE: usize = 44100;
        let BUFFER_SIZE: usize = 64;

        let mut buffer_left = vec![0.0; BUFFER_SIZE];
        let mut buffer_right = vec![0.0; BUFFER_SIZE];

        let mut player = Player::new();
        player.apply_command(PlayerCommand::Load {
            path: "./test/assets/techno1_short.mp3".to_string(),
            play: Some(true),
        });

        // Wait for file being loaded
        while player.state.file_info.is_none() {
            player
                .proceed_and_fill_buffer(&mut buffer_left, &mut buffer_right, RATE, BUFFER_SIZE)
                .expect("proceed failed");
            std::thread::sleep(std::time::Duration::from_secs_f64(0.5));
        }
        tracing::debug!("File loaded!");

        tracing::debug!("Playhead: {}", player.state.playhead_samples);
        player.apply_command(PlayerCommand::Play(Some(true), None));

        tracing::debug!("Seek to sample 420");
        player.apply_command(PlayerCommand::Seek(Position::Absolute(Unit::Sample(420))));
        assert_eq!(player.state.playhead_samples, 420);

        println!(" ");
        player
            .proceed_and_fill_buffer(&mut buffer_left, &mut buffer_right, RATE, BUFFER_SIZE)
            .expect("proceed failed");
        assert_eq!(player.state.playhead_samples, 420 + (BUFFER_SIZE as i64));

        println!(" ");
        tracing::debug!("Load file again");
        player.apply_command(PlayerCommand::Load {
            path: "./test/assets/techno1_short.mp3".to_string(),
            play: Some(false),
        });
        player
            .proceed_and_fill_buffer(&mut buffer_left, &mut buffer_right, RATE, BUFFER_SIZE)
            .expect("proceed failed");
        assert!(!player.state.play);
        assert!(player.state.playhead_samples == 0);
    }
}
