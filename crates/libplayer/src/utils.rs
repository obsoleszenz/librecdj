use std::time::Duration;

/// Fills a destination array (buffer) from an input array
/// With start and chunk_size one can define the index and
/// length of the slice that should get written into
/// destination. If this range is out of bound of the source
/// array, all out of bound elements will be filled with zeroes
pub fn fill_buffer(
    start: i64,
    chunk_size: usize,
    source: &[f32],
    destination: &mut [f32],
) -> Result<(), String> {
    // TODO: handle start + chunk_size > destination[0].len()
    //debug!("filling buffer from source: {:?}", source);
    if start < 0 {
        // We are filling buffer before the start of any samples

        // So first fill those with zeros
        let pos_first_non_zero = start.unsigned_abs().min(chunk_size as u64) as usize;
        destination[..pos_first_non_zero].fill(0.0);

        // If we can write something, write that
        let pos_last_sample = chunk_size - pos_first_non_zero;
        if pos_last_sample > 0 {
            destination[pos_first_non_zero..chunk_size].clone_from_slice(&source[..pos_last_sample])
        }
    } else if start + chunk_size as i64 > source.len() as i64 {
        if (start as usize) >= source.len() {
            // No samples we can fill, just fill with zeros
            destination.fill(0.0);
            return Ok(());
        }

        // We still have source samples that we can fill into output
        let pos_dest_last_non_zero = (source.len() as i64 - start) as usize;

        destination[..pos_dest_last_non_zero]
            .clone_from_slice(&source[start as usize..source.len()]);
        // Fill remaining output buffer with zeros
        destination[pos_dest_last_non_zero..].fill(0.0);
    } else {
        let start = start as usize;
        let destination_len = destination.len();
        let destination_slice = destination.get_mut(..chunk_size).ok_or_else(|| format!("Failed subslicing destination. Tried to get range from 0..{chunk_size}. destination.len()={destination_len}"))?;
        let source_start = start;
        let source_end = start + chunk_size;
        let source_slice = source.get(start..start + chunk_size).ok_or_else(|| format!("Failed subslicing source. Tried to get range from {source_start}..{source_end}. source.len()={}", source.len()))?;
        destination_slice.clone_from_slice(source_slice);
    }
    //debug!("filled buffer: {:?}", destination);
    Ok(())
}

pub fn get_sample_or_zero(buffer: &[f32], position: i64) -> f32 {
    if position > buffer.len() as i64 || position < 0 {
        return 0.0;
    }

    buffer[position as usize]
}

/// Fill buffer with a crossfade between two chunks (chunk_a and chunk_b).
/// The chunks are defined like this:
/// chunk_a = &source[start_a..start_+chunk_size]
/// chunk_b = &source[end_b-chunk_size..end_b]
/// start_a and end_b can also exceed/preceed the source buffer and then will
/// get replaced with zeros.
pub fn crossfaded_fill_buffer(
    start_a: i64,
    start_b: i64,
    chunk_size: usize,
    source: &[f32],
    destination: &mut [f32],
) {
    let step_size = 1.0 / chunk_size as f32;
    (0..chunk_size).for_each(|i| {
        let i_i64 = i as i64;
        let sample_a =
            get_sample_or_zero(source, start_a + i_i64) * (1.0 - (i_i64 as f32 * step_size));
        let sample_b = get_sample_or_zero(source, start_b + i_i64) * (i_i64 as f32 * step_size);
        destination[i] = sample_a + sample_b;
    });
}
pub trait HHMMSS {
    fn as_hhmmss(&self) -> String;
}

impl HHMMSS for Duration {
    fn as_hhmmss(&self) -> String {
        let minutes = (self.as_secs() / 60) % 60;
        let seconds = self.as_secs() % 60;
        let milli_seconds = self.subsec_millis() / 10;
        format!("{:0>2}:{:0>2}:{:0>2}", minutes, seconds, milli_seconds)
    }
}

/// This represents the position inside a track if we have bpm and beatgrid/first beat
/// information.
/// BPM = Beats per Minute
/// Bar = 4 beats
/// Phrase = 4 bars
// TODO: this is only used in libbackend/tui so move it there.
#[derive(Default, Debug, Clone, Copy)]
pub struct BeatPosition {
    pub position_secs: f64,
    pub first_beat_secs: f64,
    pub bpm: f64,
}

impl BeatPosition {
    /// Returns the lenght of one beat in seconds
    fn one_beat_secs(&self) -> f64 {
        60.0 / self.bpm
    }

    fn absolute_beat(&self) -> f64 {
        let one_beat_secs = self.one_beat_secs();
        let position_sub_first_beat_secs = self.position_secs - self.first_beat_secs;

        1.0 + position_sub_first_beat_secs / one_beat_secs
    }

    /// Calculate the beat (1,2,3,4) inside the current bar.
    /// It's a f64 >= 1.0 and <5.0
    /// 1.0 is the first, 2.0 the second... and 4.0 the fourth.
    pub fn beat(&self) -> f64 {
        let beats = self.absolute_beat();
        1.0 + (beats - 1.0).rem_euclid(4.0)
    }

    /// Calculates the current bar. It wraps back to zero every time bar_length is reached.
    pub fn bar(&self, bar_length: f64) -> f64 {
        let beats = self.absolute_beat();
        let floored_beats_div_four = ((beats - 1.0) / bar_length).floor();
        1.0 + floored_beats_div_four.rem_euclid(bar_length)
    }
}

#[allow(clippy::expect_used)]
#[cfg(test)]
mod test {
    use crate::utils::fill_buffer;

    use super::BeatPosition;

    const TOLERANCE: f64 = 0.000001;

    #[test]
    fn test_fill_buffer() {
        let input = [1.0; 8];

        // Needs to fill whole output with input
        let mut output = [0.0; 8];
        fill_buffer(0, 8, &input, &mut output).expect("Failed filling buffer");
        assert_eq!(output, [1.0; 8]);

        // Needs to fill whole output with zeros
        let mut output = [0.0; 8];
        fill_buffer(-8, 8, &input, &mut output).expect("Failed filling buffer");
        assert_eq!(output, [0.0; 8]);

        // Needs to fill half output with zeros, rest with input
        let input = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0];
        let mut output = [0.0; 8];
        fill_buffer(-4, 8, &input, &mut output).expect("Failed filling buffer");
        assert_eq!(output, [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 2.0, 3.0]);

        // Needs to fill the whole buffer with zeros
        let input = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0];
        let mut output = [0.0; 8];
        fill_buffer(-16, 8, &input, &mut output).expect("Failed filling buffer");
        assert_eq!(output, [0.0; 8]);

        // Needs to fill first element with input, rest with zeros
        let input = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0];
        let mut output = [-1.0; 8];
        fill_buffer(7, 8, &input, &mut output).expect("Failed filling buffer");
        assert_eq!(output, [7.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]);

        // Needs to fill output with zeros
        let input = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0];
        let mut output = [-1.0; 8];
        fill_buffer(16, 8, &input, &mut output).expect("Failed filling buffer");
        assert_eq!(output, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]);
    }

    #[test]
    fn test_beat_position() {
        assert_eq!(1.0_f32.rem_euclid(4.0), 1.0);
        assert_eq!(2.0_f32.rem_euclid(4.0), 2.0);
        assert_eq!(3.0_f32.rem_euclid(4.0), 3.0);
        assert_eq!(4.0_f32.rem_euclid(4.0), 0.0);
        assert_eq!(5.0_f32.rem_euclid(4.0), 1.0);
        assert_eq!(6.0_f32.rem_euclid(4.0), 2.0);
        assert_eq!(7.0_f32.rem_euclid(4.0), 3.0);
        assert_eq!(8.0_f32.rem_euclid(4.0), 0.0);
        assert_eq!(9.0_f32.rem_euclid(4.0), 1.0);

        let bpm = 138.0;
        let one_beat_secs = 60.0 / bpm;

        let first_beat_secs = 1.2;

        // Test first beat
        let beat_position = BeatPosition {
            position_secs: first_beat_secs,
            first_beat_secs,
            bpm,
        };

        assert_eq!(beat_position.absolute_beat(), 1.0);
        assert_eq!(beat_position.beat(), 1.0);
        assert_eq!(beat_position.bar(4.0) as i64, 1);

        // Test second beat
        let beat_position = BeatPosition {
            position_secs: first_beat_secs + 1.0 * one_beat_secs,
            first_beat_secs,
            bpm,
        };

        println!("{} {}", one_beat_secs, first_beat_secs + one_beat_secs);

        assert_eq!(beat_position.absolute_beat(), 2.0);
        assert_eq!(beat_position.beat(), 2.0);
        assert_eq!(beat_position.bar(4.0) as i64, 1);

        // Test third beat
        let beat_position = BeatPosition {
            position_secs: first_beat_secs + 2.0 * one_beat_secs,
            first_beat_secs,
            bpm,
        };

        assert_eq_with_tolerance(beat_position.absolute_beat(), 3.0, TOLERANCE).unwrap();
        assert_eq_with_tolerance(beat_position.beat(), 3.0, TOLERANCE).unwrap();
        assert_eq!(beat_position.bar(4.0) as i64, 1);

        // Test fourth beat
        let beat_position = BeatPosition {
            position_secs: first_beat_secs + 3.0 * one_beat_secs,
            first_beat_secs,
            bpm,
        };

        assert_eq_with_tolerance(beat_position.absolute_beat(), 4.0, TOLERANCE).unwrap();
        assert_eq_with_tolerance(beat_position.beat(), 4.0, TOLERANCE).unwrap();
        assert_eq!(beat_position.bar(4.0) as i64, 1);

        // Test fourth beat plus a little
        let beat_position = BeatPosition {
            position_secs: first_beat_secs + 3.0 * one_beat_secs + 0.5 * one_beat_secs,
            first_beat_secs,
            bpm,
        };

        assert_eq_with_tolerance(beat_position.absolute_beat(), 4.5, TOLERANCE).unwrap();
        assert_eq_with_tolerance(beat_position.beat(), 4.5, TOLERANCE).unwrap();
        assert_eq!(beat_position.bar(4.0) as i64, 1);

        // Test fifth beat
        let beat_position = BeatPosition {
            position_secs: first_beat_secs + 4.0 * one_beat_secs,
            first_beat_secs,
            bpm,
        };

        assert_eq_with_tolerance(beat_position.absolute_beat(), 5.0, TOLERANCE).unwrap();
        assert_eq_with_tolerance(beat_position.beat(), 1.0, TOLERANCE).unwrap();
        assert_eq!(beat_position.bar(4.0) as i64, 2);

        // Test 16th beat
        let beat_position = BeatPosition {
            position_secs: first_beat_secs + 15.0 * one_beat_secs,
            first_beat_secs,
            bpm,
        };

        assert_eq_with_tolerance(beat_position.absolute_beat(), 16.0, TOLERANCE).unwrap();
        assert_eq_with_tolerance(beat_position.beat(), 4.0, TOLERANCE).unwrap();
        assert_eq!(beat_position.bar(4.0) as i64, 4);

        // Test 17th beat
        let beat_position = BeatPosition {
            position_secs: first_beat_secs + 16.0 * one_beat_secs,
            first_beat_secs,
            bpm,
        };

        assert_eq_with_tolerance(beat_position.absolute_beat(), 17.0, TOLERANCE).unwrap();
        assert_eq_with_tolerance(beat_position.beat(), 1.0, TOLERANCE).unwrap();
        assert_eq!(beat_position.bar(4.0) as i64, 1);
    }

    fn assert_eq_with_tolerance(a: f64, b: f64, tolerance: f64) -> Result<(), String> {
        if a < b - tolerance || a > b + tolerance {
            return Err(format!(
                "assertion failed: `(left == right)`\n left: `{}`, right: `{}`, tolerance: `{}`,",
                a, b, tolerance
            ));
        }
        Ok(())
    }
}
