use serde::{Deserialize, Serialize};

use crate::state::PlayerStateMetadata;

/// Different ways to modify the [`crate::Player`]
#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum PlayerCommand {
    /// Move/Seek the playhead to a specific position
    Seek(Position),

    /// BeatSize is an alternative to Move/Seek with a settable size (beat_size)
    /// which can optionally jump with the loop. This is similar to beat jump on
    /// Traktor controllers.
    BeatSize(BeatSizeKind),

    /// Cue the track
    Cue(CueKind, Option<Unit>),
    /// Put the player into reverse mode. This will make it play backwards.
    Reverse(bool),
    /// Set slip mode. When turned on this saves the current player state and
    /// when disabled returns to that. You can do cool DJ effects with this.
    Slip(bool, Option<Unit>),
    /// Put into play mode
    Play(
        /// Some(true) for playing, Some(false) for pausing, None for inverting.
        Option<bool>,
        /// Set to some position to play from. None for keeping current position.
        Option<Unit>,
    ),
    /// Change the tempo
    Tempo(TempoKind),
    /// Load a file into this player
    Load {
        /// Path to file
        // TODO: Shall this be a [`std::path::Path`] ?
        path: String,
        /// Set to Some(true) to play immediatly after loaded
        play: Option<bool>,
    },
    Nudge(NudgeKind),
    Loop(LoopKind),
    JogWheel(JogWheelKind),
    SetMetadata(PlayerStateMetadata),
}

/// A generic time unit container.
// TODO: Maybe TimeUnit is a better name?
#[derive(Debug, Copy, Clone, Deserialize, Serialize, PartialEq)]
pub enum Unit {
    /// Time unit in beats.
    /// As [`libplayer`] doesn't hold information about the bpm of the currently
    /// loaded song, you need to pass it with this unit.
    Beat(f32),
    /// Time unit in samples.
    // TODO: Is this in relation to the original sample rate of the
    /// loaded song? Or to the resampled?
    Sample(i64),
    /// Time unit in seconds
    // TODO: Maybe Seconds is a better name?
    Time(f32),
}

impl Unit {
    pub fn to_sample(&self, sample_rate: usize, bpm: Option<f32>) -> i64 {
        match self {
            Unit::Beat(beat) => {
                let one_beat_in_samples = sample_rate as f32 * 60.0 / bpm.unwrap_or(1.0);
                (*beat * one_beat_in_samples) as i64
            }
            Unit::Sample(sample) => *sample,
            Unit::Time(time) => (*time * sample_rate as f32) as i64,
        }
    }

    pub fn to_beats(&self, sample_rate: usize, bpm: Option<f32>) -> f32 {
        match self {
            Unit::Beat(beat) => *beat,
            Unit::Sample(sample) => {
                let one_beat_in_samples = sample_rate as f32 * 60.0 / bpm.unwrap_or(1.0);
                *sample as f32 / one_beat_in_samples
            }
            Unit::Time(time) => {
                let beats_per_second = bpm.unwrap_or(1.0) / 60.0;
                time * beats_per_second
            }
        }
    }
}

/// Generic container to specificy an absolute or relative Position.
#[derive(Debug, Copy, Clone, Deserialize, Serialize, PartialEq)]
pub enum Position {
    Absolute(Unit),
    Relative(Unit),
}

impl Position {
    pub fn from_absolute_beats(beat: f32) -> Self {
        Self::Absolute(Unit::Beat(beat))
    }
    pub fn from_relative_beats(beat: f32) -> Self {
        Self::Relative(Unit::Beat(beat))
    }
    pub fn from_absolute_sample(sample: i64) -> Self {
        Self::Absolute(Unit::Sample(sample))
    }
    pub fn from_relative_sample(sample: i64) -> Self {
        Self::Relative(Unit::Sample(sample))
    }
}

#[derive(Debug, Clone, Copy, Deserialize, Serialize, PartialEq)]
pub enum CueKind {
    /// This will set cue position and cue play the track
    Press,
    /// This will jump back to cue position if deck is not playing
    Release,
    /// Reset playhead to set cue position and always play
    Reset,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum TempoKind {
    /// Set absolute tempo
    Absolute(Tempo),
    /// Add or substract relative to the current tempo
    Relative(Tempo),

    Percent(Tempo),
}

pub type Tempo = f32;

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum NudgeKind {
    Enable {
        direction: Direction,
        tempo_playing: Tempo,
        tempo_paused: Tempo,
    },
    Disable,
}

#[derive(Clone, Debug, Default, Copy, Deserialize, Serialize, PartialEq)]
pub enum Direction {
    #[default]
    Forward,
    Backward,
}

impl Direction {
    pub fn is_reverse(&self) -> bool {
        matches!(self, Self::Backward)
    }

    pub fn from_bool(is_forward: bool) -> Self {
        match is_forward {
            true => Direction::Forward,
            false => Direction::Backward,
        }
    }

    pub fn to_single_beat_f32(&self) -> f32 {
        match self {
            Direction::Forward => 1.0,
            Direction::Backward => -1.0,
        }
    }
}

macro_rules! implement_to_sign {
    ($fn_name:tt, $type:tt) => {
        impl Direction {
            pub fn $fn_name(&self) -> $type {
                match self {
                    Direction::Forward => 1 as $type,
                    Direction::Backward => -1 as $type,
                }
            }
        }
    };
}
implement_to_sign!(to_sign_f32, f32);
implement_to_sign!(to_sign_f64, f64);
implement_to_sign!(to_sign_i32, i32);
implement_to_sign!(to_sign_i64, i64);

#[derive(Debug, Clone, Copy, Deserialize, Serialize, PartialEq)]
pub enum ResizeKind {
    Multiply(f64),
    Length(Unit),
}
impl ResizeKind {
    pub fn double() -> Self {
        Self::Multiply(2.0)
    }
    pub fn halve() -> Self {
        Self::Multiply(0.5)
    }
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum Conditional {
    Enable,
    Disable,
    Toggle,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum LoopSize {
    Absolute { position: Unit, length: Unit },
    Relative(Unit),
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum LoopKind {
    /// Set a new loop
    /// With conditional this can be immediatly enabled/disabled
    Set(Conditional, LoopSize),

    /// Resize an existing Loop
    Resize(ResizeKind),

    /// Enable/Disable/Toggle the current loop
    Enabled(Conditional),

    /// Remove the current loop
    Remove,

    /// Move the loop around
    Move(Unit),
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub struct JogWheelKind {
    pub is_scratching: bool,
    pub interval: f64,
    pub direction_forward: bool,
}

#[derive(Debug, Clone, Copy, Deserialize, Serialize, PartialEq)]
pub enum BeatSizeKind {
    Jump {
        direction: Direction,
        with_loop: bool,
    },
    SetLoop(Direction),
    Resize(ResizeKind),
}
