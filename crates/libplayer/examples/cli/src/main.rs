use std::time::Duration;

use crossterm::event::{poll, read, Event, KeyCode};

use libplayer::{PlayMode, Player, PlayerAction, PlayerActionSender, PlayerStateReceiver, HHMMSS};
use tracing::debug;

fn main() {
    tracing_subscriber::fmt::init();

    println!("Starting jack client");
    let (client, _status) =
        jack::Client::new("decks", jack::ClientOptions::NO_START_SERVER).unwrap();

    let sample_rate = client.sample_rate();
    let buffer_size = client.buffer_size();

    println!("Sample Rate [Jack]: {}", sample_rate);
    println!("Buffer Size [Jack]: {}", buffer_size);

    println!("Loading player");
    let (mut player, mut player_action_sender, mut player_state_receiver) = Player::new();

    let mut channel0_left = client.register_port("ch0-l", jack::AudioOut).unwrap();
    let mut channel0_right = client.register_port("ch0-r", jack::AudioOut).unwrap();

    let process_handler =
        jack::ClosureProcessHandler::new(move |client: &jack::Client, ps: &jack::ProcessScope| {
            let channel0_left = channel0_left.as_mut_slice(ps);
            let channel0_right = channel0_right.as_mut_slice(ps);

            let sample_rate_out = client.sample_rate();
            player
                .proceed_and_fill_buffer(channel0_left, channel0_right, sample_rate_out)
                .expect("Error proceeding the player");

            jack::Control::Continue
        });

    // An active asps.last_frame_time ync client is created, `client` is consumed.
    let _active_client = client.activate_async((), process_handler).unwrap();

    //player_action_sender.send(PlayerAction::JumpTo(Duration::from_secs(120)));
    /*player_action_sender
        .send(PlayerAction::ChangeDirection(
            libplayer::Direction::Backward,
        ))
        .expect("Failed sending ChangeDirection");
    */
    player_action_sender
        .send(PlayerAction::ChangeTempo(1.0))
        .expect("Failed sending change tempo");

    player_action_sender
        .send(PlayerAction::Load(
            "/home/kerle/Musik/Techno/2023/Januar/08-zisko_-_mental_fragmentation-tr.mp3"
                .to_string(),
        ))
        .expect("Failed sending load");

    loop {
        terminal_render(&mut player_state_receiver, &mut player_action_sender)
            .expect("Failed rendering tui");
    }
}

pub fn terminal_render(
    player_state_receiver: &mut PlayerStateReceiver,
    player_action_sender: &mut PlayerActionSender,
) -> Result<(), crossterm::ErrorKind> {
    //println!("hey!");
    // `read()` blocks until an `Event` is available

    // `poll()` waits for an `Event` for a given time period
    if poll(Duration::from_secs_f64(1.0 / 30.0))? {
        // It's guaranteed that the `read()` won't block when the `poll()`
        // function returns `true`
        if let Event::Key(event) = read()? {
            if event.code == KeyCode::Char('q') {
                debug!("Pressed q");
                player_action_sender
                    .send(PlayerAction::ChangeTempo(
                        player_state_receiver.state.tempo - 0.1,
                    ))
                    .unwrap();
            } else if event.code == KeyCode::Char('w') {
                debug!("Pressed w");
                player_action_sender
                    .send(PlayerAction::ChangeTempo(
                        player_state_receiver.state.tempo + 0.1,
                    ))
                    .unwrap();
            } else if event.code == KeyCode::Char('g') {
                debug!("Pressed g");
                player_action_sender
                    .send(PlayerAction::ChangeDirection(
                        libplayer::Direction::Backward,
                    ))
                    .unwrap();
            } else if event.code == KeyCode::Char('h') {
                debug!("Pressed g");
                player_action_sender
                    .send(PlayerAction::ChangeDirection(libplayer::Direction::Forward))
                    .unwrap();
            } else if event.code == KeyCode::Char('p') {
                debug!("Pressed p");
                let play_mode = match player_state_receiver.state.play_mode {
                    PlayMode::Play => Some(PlayMode::Stop),
                    PlayMode::Stop => Some(PlayMode::Play),
                    _ => None,
                };

                if let Some(play_mode) = play_mode {
                    player_action_sender
                        .send(PlayerAction::ChangePlayMode(play_mode))
                        .unwrap();
                }
            }
        }
    } else {
        // Timeout expired and no `Event` is available
    }
    player_state_receiver.refresh().ok();

    if true {
        println!(
            "Position: {} / {} Tempo: {} Direction: {:?} PlayMode: {:?}",
            player_state_receiver.state.position().to_string(),
            player_state_receiver.state.total().to_string(),
            player_state_receiver.state.tempo,
            player_state_receiver.state.direction,
            player_state_receiver.state.play_mode,
        );
    }
    Ok(())
}
