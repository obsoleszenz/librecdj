use crossterm::{
    event::{
        DisableMouseCapture, EnableMouseCapture, KeyboardEnhancementFlags,
        PopKeyboardEnhancementFlags, PushKeyboardEnhancementFlags,
    },
    execute,
    terminal::{enable_raw_mode, Clear, ClearType, EnterAlternateScreen, LeaveAlternateScreen},
};

use crossterm::cursor;
use crossterm::terminal;

use tracing_subscriber::prelude::*;

use crossterm::event::{self, Event, KeyCode, KeyEventKind};
use std::{
    error::Error,
    io,
    process::{Command, Stdio},
    time::Duration,
};
use tui::{
    backend::{Backend, CrosstermBackend},
    buffer::Buffer,
    layout::Rect,
    style::Style,
    widgets::Widget,
    Terminal,
};

fn restore_terminal() {
    let mut stdout = io::stdout();

    // restore terminal
    terminal::disable_raw_mode().unwrap();
    execute!(
        stdout,
        LeaveAlternateScreen,
        DisableMouseCapture,
        cursor::Show,
        PopKeyboardEnhancementFlags
    )
    .unwrap();
}

fn init_terminal() -> Result<(), Box<dyn Error>> {
    // setup terminal
    let mut stdout = io::stdout();
    enable_raw_mode()?;
    execute!(
        stdout,
        EnterAlternateScreen,
        EnableMouseCapture,
        PushKeyboardEnhancementFlags(KeyboardEnhancementFlags::DISAMBIGUATE_ESCAPE_CODES),
        PushKeyboardEnhancementFlags(KeyboardEnhancementFlags::REPORT_ALL_KEYS_AS_ESCAPE_CODES),
        PushKeyboardEnhancementFlags(KeyboardEnhancementFlags::REPORT_EVENT_TYPES),
    )?;

    Ok(())
}

fn register_panic_hook() {
    std::panic::set_hook(Box::new(move |panic_info| {
        restore_terminal();
        println!("{}", panic_info);
        //better_panic::Settings::auto().create_panic_handler()(panic_info);
    }));
}

fn main() -> Result<(), Box<dyn Error>> {
    tracing_subscriber::registry()
        .with(tui_logger::tracing_subscriber_layer())
        .init(); // create app and run it

    let stdout = io::stdout();
    init_terminal()?;

    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    register_panic_hook();

    tui_logger::set_default_level(log::LevelFilter::Trace);
    tui_logger::set_log_file("./log").unwrap();

    if let Err(err) = App::default().run(&mut terminal) {
        println!("{:?}", err)
    }

    // restore terminal
    restore_terminal();

    Ok(())
}

#[derive(Default)]
pub struct App {}

pub fn open_editor(command: &str, args: &[&str]) {
    restore_terminal();

    let mut child = Command::new(command)
        .args(args)
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .env_clear()
        .spawn()
        .expect("printenv failed to start");
    child.wait().unwrap();
    execute!(io::stdout(), Clear(ClearType::All)).unwrap();

    init_terminal().unwrap();
}

const TICK_RATE: Duration = Duration::from_millis(1000 / 30);
impl App {
    pub fn on_tick(&mut self) {}

    pub fn run<B: Backend>(&mut self, terminal: &mut Terminal<B>) -> io::Result<()> {
        let mut opened_nvim = false;
        loop {
            terminal.draw(|f| {
                f.render_widget(
                    GenericWidget::new(&|area, buf| {
                        buf.set_string(area.x, area.y, "Hello", Style::default());
                    }),
                    f.size(),
                )
            })?;

            if event::poll(TICK_RATE)? {
                if let Event::Key(key) = event::read()? {
                    if key.kind != KeyEventKind::Press {
                        continue;
                    }

                    if key.code == KeyCode::Char('q') {
                        break Ok(());
                    }
                    if !opened_nvim {
                        opened_nvim = true;
                        open_editor("nvim", &["/tmp/test"]);
                    }
                }
            }

            std::thread::sleep(TICK_RATE);
        }
    }
}

struct GenericWidget<'a> {
    render_cb: &'a dyn Fn(Rect, &mut Buffer),
}

impl<'a> GenericWidget<'a> {
    pub fn new(render_cb: &'a dyn Fn(Rect, &mut Buffer)) -> Self {
        Self { render_cb }
    }
}

impl<'a> Widget for GenericWidget<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        (*self.render_cb)(area, buf);
    }
}
