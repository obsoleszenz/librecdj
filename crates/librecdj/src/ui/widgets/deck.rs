use libbackend::{
    libplayer::{BeatPosition, LoopState},
    state::{BeatState, Deck, WaveformMode},
};
use once_cell::sync::Lazy;

use crate::{app::App, colors};
use tui::{
    buffer::Buffer,
    layout::Rect,
    style::{Color, Style},
    widgets::{Block, BorderType, Borders, Widget},
};

#[derive(Default, Debug)]
pub struct DeckWidget {
    is_selected: bool,
    deck: Deck,
    deck_index: usize,
    beat: BeatState,
    position: BeatPosition,
    waveform_mode: WaveformMode,
}

static WAVEFORM_TRESHOLDS: Lazy<Vec<f32>> = Lazy::new(|| {
    [0.0; 128]
        .iter()
        .enumerate()
        .map(|(i, _)| 0.0 - (0.0015 * (i as f32).powi(2)))
        .collect()
});

impl DeckWidget {
    pub fn new(app: &App, deck_index: usize) -> Self {
        let is_selected = app.backend.selected_deck() == deck_index;
        let deck = app.backend.state.decks[deck_index].clone();
        let beat = app.backend.state.beat[deck_index];
        let position = app.backend.state.positions[deck_index];
        let waveform_mode = app.backend.state.waveform_mode;

        DeckWidget {
            deck,
            is_selected,
            deck_index,
            beat,
            position,
            waveform_mode,
        }
    }

    fn render_waveform(&self, area: Rect, buf: &mut Buffer) {
        let mode = self.waveform_mode;
        let margin = 45;
        let waveform_width = area.width - margin - 2;
        let playhead_samples = self.deck.player_state.playhead_samples;
        let playheader_x: Option<i32> =
            if self.deck.waveform.is_some() && mode == WaveformMode::Overview {
                Some(if let Some(file_info) = self.deck.player_state.file_info {
                    if playhead_samples >= file_info.count_samples as i64 {
                        waveform_width as i32
                    } else if playhead_samples == 0 {
                        -1
                    } else if playhead_samples < 0 {
                        -2
                    } else {
                        ((playhead_samples as f32 / file_info.count_samples as f32)
                            * waveform_width as f32) as i32
                    }
                } else {
                    -1
                })
            } else if mode == WaveformMode::Overview {
                None
            } else {
                Some((waveform_width / 2).into())
            };

        if let Some(playheader_x) = playheader_x {
            let playheader_color = Color::Black;
            buf.set_string(
                (area.x as i32 + margin as i32 + playheader_x) as u16,
                area.y,
                tui::symbols::block::SEVEN_EIGHTHS,
                Style::fg(Style::default(), Color::White).bg(playheader_color),
            );
            buf.set_string(
                (area.x as i32 + margin as i32 + playheader_x) as u16,
                area.y + 4,
                tui::symbols::block::SEVEN_EIGHTHS,
                Style::fg(Style::default(), Color::White).bg(playheader_color),
            );
        }
        if let Some(waveform) = &self.deck.waveform {
            let waveform_len = waveform.bands.len();

            let resampled: Vec<[f32; 3]> = if mode == WaveformMode::Overview {
                let summary_chunk_size = waveform_len / waveform_width as usize;
                vec![0.0; waveform_width as usize]
                    .iter()
                    .enumerate()
                    .map(|(i, _)| {
                        let [mut summed_lo, mut summed_mi, mut summed_hi] = [0.0; 3];

                        for c in 0..summary_chunk_size {
                            let chunk_pos = summary_chunk_size * i + c;
                            let chunk_bands = waveform.bands[chunk_pos];
                            for (i, sum) in [&mut summed_lo, &mut summed_mi, &mut summed_hi]
                                .iter_mut()
                                .enumerate()
                            {
                                **sum += chunk_bands[i];
                            }
                        }

                        for sum in [&mut summed_lo, &mut summed_mi, &mut summed_hi] {
                            *sum /= summary_chunk_size as f32;
                        }

                        [summed_lo, summed_mi, summed_hi]
                    })
                    .collect()
            } else if let WaveformMode::Zoom = mode {
                let bpm = self.deck.player_state.initial_bpm().unwrap_or(128.0);

                let summary_chunk_size: i64 = (waveform.sample_rate as f32
                    / waveform.window_size as f32
                    / (bpm / 60.0)
                    / 7.0) as i64;

                let waveform_playhead_pos = (playhead_samples / waveform.window_size as i64)
                    .div_euclid(summary_chunk_size)
                    * summary_chunk_size;

                let waveform_start_pos: i64 =
                    waveform_playhead_pos - (waveform_width as i64 / 2) * summary_chunk_size;

                //
                vec![0.0; waveform_width as usize]
                    .iter()
                    .enumerate()
                    .map(|(i, _)| {
                        let [mut summed_lo, mut summed_mi, mut summed_hi] = [0.0; 3];

                        let waveform_sum_pos: i64 =
                            waveform_start_pos + summary_chunk_size * i as i64;

                        if waveform_sum_pos >= 0
                            && waveform_sum_pos < waveform_len as i64 - summary_chunk_size
                        {
                            let waveform_sum_pos = waveform_sum_pos as usize;
                            for c in 0..summary_chunk_size {
                                let chunk_bands = waveform.bands[waveform_sum_pos + c as usize];
                                for (i, sum) in [&mut summed_lo, &mut summed_mi, &mut summed_hi]
                                    .iter_mut()
                                    .enumerate()
                                {
                                    **sum += chunk_bands[i];
                                }
                            }

                            for sum in [&mut summed_lo, &mut summed_mi, &mut summed_hi] {
                                *sum /= summary_chunk_size as f32;
                            }
                        } else {
                            for sum in [&mut summed_lo, &mut summed_mi, &mut summed_hi] {
                                *sum = -70.0;
                            }
                        }

                        [summed_lo, summed_mi, summed_hi]
                    })
                    .collect()
            } else {
                unreachable!();
            };

            for (x, bands) in resampled.iter().enumerate() {
                for y in 0..3 {
                    let summed = bands[2 - y];
                    let b = WAVEFORM_TRESHOLDS
                        .iter()
                        .position(|t| summed >= *t)
                        .unwrap_or(WAVEFORM_TRESHOLDS.len() - 1);

                    let color = {
                        let brightness = 128 + b as u8;

                        if b == 127 {
                            None
                        } else if y == 0 {
                            Some(Color::Rgb(0, 0, brightness))
                        } else if y == 1 {
                            Some(Color::Rgb(0, brightness, 0))
                        } else {
                            Some(Color::Rgb(brightness, 0, 0))
                        }
                    };
                    if let Some(color) = color {
                        let background = Color::Black;
                        let style = Style::fg(Style::default(), color).bg(background);
                        buf.set_string(
                            area.x + margin + (x as u16),
                            area.y + 1 + (y as u16),
                            tui::symbols::block::FULL,
                            style,
                        );
                    }
                }
            }
        }
    }
}

impl Widget for DeckWidget {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let metadata = &self.deck.metadata;
        let title = if metadata.artist.is_empty() && metadata.title.is_empty() {
            " DISCHARGED ".to_string()
        } else {
            format!(" {} - {} ", metadata.artist, metadata.title)
        };
        let background = colors::deck_color(self.deck_index);
        let block = Block::default()
            .title(title)
            .borders(Borders::LEFT)
            .border_type(BorderType::Plain)
            .border_style(Style::default().fg(if self.is_selected {
                background
            } else {
                Color::Black
            }));

        block.render(area, buf);
        buf.set_string(
            area.x,
            area.y,
            self.deck.deck_short.to_string(),
            Style::default().fg(Color::Black).bg(background),
        );

        let (position, duration) = if let Some(file_info) = self.deck.player_state.file_info {
            let position =
                self.deck.player_state.playhead_samples as f32 / file_info.sample_rate as f32;
            let duration = file_info.count_samples as f32 / file_info.sample_rate as f32;

            (Some(position), Some(duration))
        } else {
            (None, None)
        };

        buf.set_string(
            area.x + 4,
            area.y + 1,
            format!(
                "{} / {}",
                if let Some(position) = position {
                    format_time_elapsed(position)
                } else {
                    " --:--:--".to_string()
                },
                if let Some(duration) = duration {
                    format_time_length(duration)
                } else {
                    "--:--".to_string()
                }
            ),
            Style::default(),
        );

        buf.set_string(
            area.x + 28,
            area.y + 1,
            format_deck_looping(&self.deck),
            if let Some(LoopState { enabled: true, .. }) = self.deck.player_state.looping {
                Style::default().fg(Color::Red)
            } else {
                Style::default()
            },
        );

        buf.set_string(
            area.x + 4,
            area.y + 2,
            format_beat_position_and_beat_size(self.position, self.deck.player_state.beat_size),
            Style::default(),
        );
        let tempo = self.deck.player_state.tempo + self.deck.player_state.jogwheel_tempo;
        buf.set_string(
            area.x + 4,
            area.y + 3,
            format!(
                "{}{:.03}%     {:.02} BPM {}",
                if tempo < 0.0 { '-' } else { '+' },
                tempo,
                self.beat.bpm,
                self.deck.metadata.key
            ),
            Style::default(),
        );

        buf.set_string(
            area.x + 4,
            area.y + 4,
            self.deck.metadata.comment.clone(),
            Style::default(),
        );

        self.render_waveform(area, buf);
    }
}

pub fn format_deck_looping(deck: &Deck) -> String {
    match deck.player_state.loop_length_beat() {
        None => "Loop: 00 OFF".to_string(),
        Some(loop_size_beat) => {
            let enabled = deck.player_state.is_playhead_inside_enabled_loop();
            format!(
                "Loop: {:02} {}",
                loop_size_beat,
                if enabled { "ON" } else { "OFF" }
            )
        }
    }
}

/*
 *
 *  61.0 => 01:01.0
 *
 */
pub fn format_time_elapsed(time_sec: f32) -> String {
    let sign = if time_sec >= 0.0 { "+" } else { "-" };
    let time_sec = time_sec.abs();
    let minutes = (time_sec / 60.0) as i32;
    let seconds = (time_sec).rem_euclid(60.0) as i32;
    let miliseconds = (time_sec * 100.0).rem_euclid(100.0) as i32;
    format!("{}{:02}:{:02}.{:02}", sign, minutes, seconds, miliseconds)
}
pub fn format_time_length(time_sec: f32) -> String {
    let minutes = (time_sec / 60.0) as i32;
    let seconds = (time_sec).rem_euclid(60.0) as i32;
    format!("{:02}:{:02}", minutes, seconds)
}

pub fn format_beat_position_and_beat_size(beat_position: BeatPosition, beat_size: f32) -> String {
    let mut beat = format!("{:.01}", beat_position.beat());
    if beat == "5.0" {
        beat = "1.0".to_string();
    }
    format!(
        " {}      / {:.00}           Size: {:.00}",
        beat,
        beat_position.bar(4.0),
        beat_size
    )
}
#[cfg(test)]
mod tests {
    use super::format_time_elapsed;

    #[test]
    fn test_format_time_elapsed() {
        assert_eq!(format_time_elapsed(1.0), "+00:01.00".to_string());
        assert_eq!(format_time_elapsed(1.1), "+00:01.10".to_string());
        assert_eq!(format_time_elapsed(60.0), "+01:00.00".to_string());
        assert_eq!(format_time_elapsed(61.3), "+01:01.30".to_string());
        assert_eq!(format_time_elapsed(61.03), "+01:01.03".to_string());
        assert_eq!(format_time_elapsed(-61.03), "-01:01.03".to_string());
    }
}
