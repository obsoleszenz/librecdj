use std::sync::{
    atomic::{AtomicU16, Ordering},
    Arc, LazyLock,
};

use libbackend::library::{LibraryItem, LibraryState};
use tui::{
    buffer::Buffer,
    layout::Rect,
    style::{Color, Style},
    widgets::{Block, BorderType, Borders, Widget},
};

use crate::{app::App, colors};

pub static LIBRARY_HEIGHT: LazyLock<Arc<AtomicU16>> = LazyLock::new(|| Arc::new(AtomicU16::new(0)));

#[derive(Debug)]
pub struct LibraryWidget<'a> {
    library_state: &'a LibraryState,
    selected_deck: usize,
    selected_deck_short: char,
}

impl<'a> LibraryWidget<'a> {
    pub fn new(app: &'a App, library_state: &'a LibraryState) -> Self {
        LibraryWidget {
            library_state,
            selected_deck_short: match app.backend.selected_deck() {
                0 => 'A',
                1 => 'B',
                2 => 'C',
                3 => 'D',
                _ => panic!("Invalid selected deck"),
            },
            selected_deck: app.backend.selected_deck(),
        }
    }
    fn format_track_line(&self, selected: bool, item: LibraryItem) -> String {
        let selected = if selected {
            format!("  {}>", self.selected_deck_short)
        } else {
            "    ".to_string()
        };
        let name = match item {
            LibraryItem::File {
                name,
                path: _,
                metadata: None,
            } => name,
            LibraryItem::File {
                name,
                path: _,
                metadata: Some(metadata),
            } => {
                let (artist, title) = (&metadata.artist, &metadata.title);
                if artist.is_empty() && title.is_empty() {
                    name
                } else {
                    format!(
                        "{:03} | {: <3.3} |  {: <16.16} | {: <32.32} | {: <12.12} |  {}",
                        metadata.bpm.unwrap_or(0.0),
                        metadata.key,
                        metadata.artist,
                        metadata.title,
                        metadata.mood,
                        metadata.comment
                    )
                }
            }
            LibraryItem::Dir(name) => name,
            LibraryItem::Section { name } => {
                format!(" -- {} --", name)
            }
        };
        format!("{} {}", selected, name)
    }
}

impl<'a> Widget for LibraryWidget<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        // Little hack to hand out the library height
        LIBRARY_HEIGHT.store(area.height - 1, Ordering::Relaxed);

        let block = Block::default()
            .title(format!("{} ", self.library_state.path.to_string_lossy()))
            .borders(Borders::LEFT)
            .border_type(BorderType::Plain);
        //trace!("area.height: {}", area.height);

        let virtual_list_range = {
            let half_area_height = (area.height) / 2;
            let start = self.library_state.selected as i64 - half_area_height as i64;
            let end = self.library_state.selected as i64 - 1 + half_area_height as i64;
            (start, end)
        };
        for i in 0..(area.height - 1) as usize {
            let track_i = virtual_list_range.0 + i as i64;
            let selected = track_i as usize == self.library_state.selected;

            let track_line = if track_i > self.library_state.items.len() as i64 - 1
                || track_i < 0
                || self.library_state.items.is_empty()
            {
                "     ...".to_string()
            } else {
                let track_i = track_i as usize;
                let item = self.library_state.items[track_i].clone();
                self.format_track_line(selected, item)
            };

            buf.set_string(
                area.x + 2,
                area.y + 1 + (i as u16),
                format!("{: <128.128}", track_line),
                if selected {
                    let background = colors::deck_color(self.selected_deck);
                    Style::default().fg(Color::Black).bg(background)
                } else {
                    Style::default()
                },
            );
        }
        block.render(area, buf);
    }
}
