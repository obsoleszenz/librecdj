use libbackend::state::BeatState;
use tui::{
    buffer::Buffer,
    layout::Rect,
    style::Style,
    widgets::{Block, Widget},
};

use crate::app::App;

#[derive(Debug)]
pub struct TopBar {
    main_beat: BeatState,
}

impl TopBar {
    pub fn new(app: &App) -> Self {
        TopBar {
            main_beat: app.backend.state.beat[4],
        }
    }
}

impl Widget for TopBar {
    fn render(self, area: Rect, buf: &mut Buffer) {
        //trace!("area.height: {}", area.height);

        let bpm_text = format!("bpm: {:3.02}", self.main_beat.bpm);

        let block = Block::default();

        block.render(area, buf);
        buf.set_string(
            area.x + area.width - bpm_text.len() as u16 - 2,
            area.y,
            bpm_text,
            Style::default(),
        );
    }
}
