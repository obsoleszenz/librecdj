pub mod popups;
pub mod screens;
pub mod widgets;

use libbackend::state::Screen;
use tui::{
    backend::Backend,
    layout::{Constraint, Direction, Layout, Rect},
    Frame,
};

use crate::app::App;

use self::{
    screens::{exit::ShowExitScreen, log::ShowLogScreen, main::MainScreen},
    widgets::{deck::DeckWidget, library::LibraryWidget, top_bar::TopBar},
};

pub fn ui<B: Backend>(f: &mut Frame<B>, app: &App) {
    match app.backend.state.screen {
        Screen::Log => ShowLogScreen(f, &app.tui_widget_state),
        Screen::Exit => ShowExitScreen(f),
        Screen::Main => {
            MainScreen {
                top_bar: TopBar::new(app),
                decks: (0..4)
                    .map(|i| DeckWidget::new(app, i))
                    .collect::<Vec<DeckWidget>>()
                    .try_into()
                    .expect("Unreachable: This should not happen"),

                library: LibraryWidget::new(app, &app.backend.state.library),
                popup: &app.backend.state.popup,
            }
            .show(f);
        }
    };
}

/// helper function to create a centered rect using up certain percentage of the available rect `r`
fn centered_rect(percent_x: u16, percent_y: u16, r: Rect) -> Rect {
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage((100 - percent_y) / 2),
                Constraint::Percentage(percent_y),
                Constraint::Percentage((100 - percent_y) / 2),
            ]
            .as_ref(),
        )
        .split(r);

    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage((100 - percent_x) / 2),
                Constraint::Percentage(percent_x),
                Constraint::Percentage((100 - percent_x) / 2),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}

fn side_bottom_rect(percent_x: u16, percent_y: u16, r: Rect) -> Rect {
    let popup_layout = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            [
                Constraint::Percentage(100 - percent_y),
                Constraint::Percentage(percent_y),
            ]
            .as_ref(),
        )
        .split(r);

    Layout::default()
        .direction(Direction::Horizontal)
        .constraints(
            [
                Constraint::Percentage(100 - percent_x),
                Constraint::Percentage(percent_x),
            ]
            .as_ref(),
        )
        .split(popup_layout[1])[1]
}
