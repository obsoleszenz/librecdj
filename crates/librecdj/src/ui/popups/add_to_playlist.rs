use libbackend::state::{AddToPlaylistState, Playlist};
use tui::{
    buffer::Buffer,
    layout::Rect,
    style::{Color, Style},
    widgets::{Block, Borders, Widget},
};

pub struct AddToPlaylistPopup<'a> {
    pub state: &'a AddToPlaylistState,
}

impl<'a> AddToPlaylistPopup<'a> {
    fn format_playlist_line(&self, selected: bool, item: Playlist) -> String {
        let selected = if selected {
            " >".to_string()
        } else {
            "  ".to_string()
        };
        format!("{} {}", selected, item.name)
    }
}

impl<'a> Widget for AddToPlaylistPopup<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let block = Block::default()
            .title(" Add to Playlist")
            .borders(Borders::ALL);

        block.render(area, buf);

        // Somehow the area we get for the popup is too big
        let real_area_y = area.height / 2;

        let virtual_list_range = {
            let half_area_height = (real_area_y) / 2;
            let start = self.state.vlist.selected as i64 - half_area_height as i64;
            let end = self.state.vlist.selected as i64 - 1 + half_area_height as i64;
            (start, end)
        };

        for i in 0..(area.y) / 2 - 2 {
            let playlist_i = virtual_list_range.0 + i as i64;
            let selected = playlist_i as usize == self.state.vlist.selected;
            let track_line = if playlist_i > self.state.vlist.items.len() as i64 - 1
                || playlist_i < 0
                || self.state.vlist.items.is_empty()
            {
                "   ...".to_string()
            } else {
                let playlist_i = playlist_i as usize;
                let item = self.state.vlist.items[playlist_i].clone();
                self.format_playlist_line(selected, item)
            };

            buf.set_string(
                area.x + 2,
                area.y + 1 + i,
                format!("{: <128.128}", track_line),
                if selected {
                    Style::default().fg(Color::Black).bg(Color::Gray)
                } else {
                    Style::default()
                },
            );
        }
    }
}
