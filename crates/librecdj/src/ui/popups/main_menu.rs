use libbackend::state::{MainMenuState, Playlist};

use tui::{
    buffer::Buffer,
    layout::Rect,
    style::Style,
    widgets::{Block, Borders, Widget},
};

pub struct MainMenuPopup<'a> {
    pub state: &'a MainMenuState,
}

impl<'a> MainMenuPopup<'a> {}

impl<'a> Widget for MainMenuPopup<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let block = Block::default().title(" Menu  ").borders(Borders::ALL);

        block.render(area, buf);

        // Somehow the area we get for the popup is too big
        let real_area_y = area.height / 2;

        buf.set_string(area.x + 2, area.y + 1, "[q] Quit", Style::default());
        buf.set_string(
            area.x + 2,
            area.y + 2,
            "[a] Add to playlist",
            Style::default(),
        );
        buf.set_string(
            area.x + 2,
            area.y + 3,
            "[d] Delete Playlist Item",
            Style::default(),
        );
        buf.set_string(
            area.x + 2,
            area.y + 4,
            "[j] Jump to Playlist Section",
            Style::default(),
        );
        buf.set_string(
            area.x + 2,
            area.y + 5,
            "[m] Move to Section",
            Style::default(),
        );
    }
}
