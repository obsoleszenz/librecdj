use libbackend::state::DeletePlaylistItemState;
use tui::{
    buffer::Buffer,
    layout::Rect,
    style::Style,
    widgets::{Block, Borders, Widget},
};

#[derive(Debug, Clone)]
pub struct DeletePlaylistItemPopup<'a> {
    pub state: &'a DeletePlaylistItemState,
}

impl<'a> Widget for DeletePlaylistItemPopup<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let block = Block::default()
            .title(" Remove from Playlist")
            .borders(Borders::ALL);

        block.render(area, buf);
        buf.set_string(
            area.x + 2,
            area.y + 1,
            format!("Do you really want to remove {} ?", self.state.filename),
            Style::default(),
        );
    }
}
