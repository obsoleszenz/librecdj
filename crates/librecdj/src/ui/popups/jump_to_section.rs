use libbackend::state::JumpToSectionState;
use tui::{
    buffer::Buffer,
    layout::Rect,
    style::{Color, Style},
    widgets::{Block, Borders, Widget},
};

use super::move_to_section::SectionItem;

pub struct JumpToSectionPopup<'a> {
    pub state: &'a JumpToSectionState,
}

impl<'a> JumpToSectionPopup<'a> {
    fn format_section_line(&self, selected: bool, item: SectionItem) -> String {
        let selected = if selected {
            " >".to_string()
        } else {
            "  ".to_string()
        };
        format!("{} {}", selected, item.0)
    }
}

impl<'a> Widget for JumpToSectionPopup<'a> {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let block = Block::default()
            .title(" Jump to Section")
            .borders(Borders::ALL);

        block.render(area, buf);

        // Somehow the area we get for the popup is too big
        let real_area_y = area.height / 2;

        let virtual_list_range = {
            let half_area_height = (real_area_y) / 2;
            let start = self.state.vlist.selected as i64 - half_area_height as i64;
            let end = self.state.vlist.selected as i64 - 1 + half_area_height as i64;
            (start, end)
        };

        for i in 0..(area.y) / 2 - 2 {
            let section_i = virtual_list_range.0 + i as i64;
            let selected = section_i as usize == self.state.vlist.selected;
            let section_line = if section_i > self.state.vlist.len() as i64 - 1
                || section_i < 0
                || self.state.vlist.is_empty()
            {
                "   ...".to_string()
            } else {
                let section_i = section_i as usize;
                let item = self.state.vlist.items[section_i].clone();
                self.format_section_line(selected, item)
            };

            buf.set_string(
                area.x + 2,
                area.y + 1 + i,
                format!("{: <128.128}", section_line),
                if selected {
                    Style::default().fg(Color::Black).bg(Color::Gray)
                } else {
                    Style::default()
                },
            );
        }
    }
}
