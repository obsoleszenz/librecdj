use libbackend::state::PopupState;
use tui::{
    backend::Backend,
    layout::{Constraint, Direction, Layout},
    widgets::Clear,
    Frame,
};

use crate::ui::{
    centered_rect,
    popups::{
        add_to_playlist::AddToPlaylistPopup, delete_playlist_item::DeletePlaylistItemPopup,
        jump_to_section::JumpToSectionPopup, main_menu::MainMenuPopup,
        move_to_section::MoveToSectionPopup,
    },
    side_bottom_rect,
    widgets::{deck::DeckWidget, library::LibraryWidget, top_bar::TopBar},
};

pub struct MainScreen<'a> {
    pub top_bar: TopBar,
    pub decks: [DeckWidget; 4],
    pub library: LibraryWidget<'a>,
    pub popup: &'a Option<PopupState>,
}

impl<'a> MainScreen<'a> {
    pub fn show<B: Backend>(self, f: &mut Frame<B>) {
        let chunks_horizontal = Layout::default()
            .direction(Direction::Horizontal)
            .constraints([Constraint::Length(1), Constraint::Percentage(100)])
            .split(f.size());
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([
                Constraint::Length(1),
                Constraint::Length(7),
                Constraint::Length(7),
                Constraint::Length(7),
                Constraint::Length(7),
                Constraint::Length(75),
            ])
            .split(chunks_horizontal[1]);

        f.render_widget(self.top_bar, chunks[0]);

        for (i, deck) in self.decks.into_iter().enumerate() {
            f.render_widget(deck, chunks[i + 1]);
        }
        f.render_widget(self.library, chunks[5]);

        if let Some(popup) = self.popup {
            match popup {
                PopupState::AddToPlaylist(state) => {
                    let area = centered_rect(60, 20, f.size());
                    f.render_widget(Clear, area); //this clears out the background
                    f.render_widget(AddToPlaylistPopup { state }, area)
                }
                PopupState::DeletePlaylistItem(state) => {
                    let area = centered_rect(60, 20, f.size());
                    f.render_widget(Clear, area); //this clears out the background
                    f.render_widget(DeletePlaylistItemPopup { state }, area)
                }
                PopupState::MainMenu(main_menu_state) => {
                    let area = side_bottom_rect(60, 20, f.size());
                    f.render_widget(Clear, area); //this clears out the background
                    f.render_widget(
                        MainMenuPopup {
                            state: main_menu_state,
                        },
                        area,
                    )
                }
                PopupState::MoveToM3USection(state) => {
                    let area = centered_rect(60, 20, f.size());
                    f.render_widget(Clear, area); //this clears out the background
                    f.render_widget(MoveToSectionPopup { state }, area)
                }
                PopupState::JumpToM3USection(state) => {
                    let area = centered_rect(60, 20, f.size());
                    f.render_widget(Clear, area); //this clears out the background
                    f.render_widget(JumpToSectionPopup { state }, area)
                }
            };
        }
    }
}
