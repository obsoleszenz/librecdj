use tui::{
    layout::{Constraint, Direction, Layout},
    style::{Color, Style},
    Frame,
};
use tui_logger::{TuiLoggerLevelOutput, TuiLoggerSmartWidget, TuiWidgetState};

#[allow(non_snake_case)]
pub fn ShowLogScreen<B>(f: &mut Frame<B>, tui_state: &TuiWidgetState)
where
    B: tui::backend::Backend,
{
    let tui_sm = TuiLoggerSmartWidget::default()
        .style_error(Style::default().fg(Color::Red))
        .style_debug(Style::default().fg(Color::Green))
        .style_warn(Style::default().fg(Color::Yellow))
        .style_trace(Style::default().fg(Color::Magenta))
        .style_info(Style::default().fg(Color::Cyan))
        .output_separator(':')
        .output_timestamp(Some("%H:%M:%S".to_string()))
        .output_level(Some(TuiLoggerLevelOutput::Abbreviated))
        .output_target(true)
        .output_file(true)
        .output_line(true)
        .state(tui_state);

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Length(100)])
        .split(f.size());
    f.render_widget(tui_sm, chunks[0]);
}
