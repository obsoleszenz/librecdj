use tui::{
    buffer::Buffer,
    layout::{Constraint, Direction, Layout, Rect},
    style::{Color, Style},
    widgets::{Block, BorderType, Borders, Widget},
    Frame,
};

#[allow(non_snake_case)]
pub fn ShowExitScreen<B>(f: &mut Frame<B>)
where
    B: tui::backend::Backend,
{
    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([
            Constraint::Percentage(33),
            Constraint::Length(5),
            Constraint::Percentage(33),
        ])
        .split(f.size());

    f.render_widget(ExitPopupWidget {}, chunks[1]);
}

struct ExitPopupWidget {}

impl Widget for ExitPopupWidget {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let block = Block::default()
            .title(" QUIT ")
            .borders(Borders::TOP | Borders::LEFT | Borders::BOTTOM | Borders::RIGHT)
            .border_type(BorderType::Plain)
            .border_style(Style::default().fg(Color::Black));

        block.render(area, buf);
        buf.set_string(
            area.x + 2,
            area.y + 2,
            "Shutting down LibreCDJ... Normally you shouldn't see this ;)",
            Style::default(),
        );
    }
}
