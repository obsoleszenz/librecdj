use std::io::Write;

use tracing::error;

pub use libmetadata::*;

pub fn edit_metadata_with_editor(file_path: &str) {
    let metadata = read_metadata(file_path).unwrap_or_default();

    // serialize metadata
    let metadata_serialized = metadata.serialize();

    // create temp file

    let mut tempfile = tempfile::Builder::new()
        .prefix("my-temporary-note")
        .suffix(".txt")
        .rand_bytes(5)
        .tempfile()
        .expect("Failed creating temp file");

    tempfile
        .write_all(metadata_serialized.as_bytes())
        .expect("Failed writing to tempfile");

    let tempfile_path = tempfile
        .path()
        .to_str()
        .expect("Failed creating tempfile path str");

    crate::open_editor("nvim", &[tempfile_path]);
    let tempfile_string =
        std::fs::read_to_string(tempfile_path).expect("Failed reading from tempfile");

    match Metadata::deserialize(&tempfile_string) {
        Ok(metadata) => write_metadata(file_path, &metadata).expect("Failed writing metadata"),
        Err(err) => error!("Failed deserializing metadata from tempfile: {}", err),
    }
}
