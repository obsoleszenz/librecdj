use crossterm::event::{self, KeyCode};
use std::{
    collections::HashMap,
    io,
    time::{Duration, Instant},
};
use tui::Terminal;
use tui_logger::TuiWidgetState;

use crate::{
    key::HandleKeyEvent,
    ui::{ui, widgets::library::LIBRARY_HEIGHT},
};
use libbackend::{
    audio_engine::{AudioEngine, AudioEngineCommand, AudioEngineToGUIMessage},
    backend::Backend,
    jack::JackBackend,
};

const TICK_RATE: Duration = Duration::from_millis(1000 / 60);

pub struct App {
    pub backend: Backend,
    pub jack_backend: JackBackend,
    pub tui_widget_state: TuiWidgetState,
}

impl Default for App {
    fn default() -> App {
        let (to_gui_tx, from_audio_rx) = rtrb::RingBuffer::<AudioEngineToGUIMessage>::new(2048);
        let (to_audio_tx, from_gui_rx) = rtrb::RingBuffer::<AudioEngineCommand>::new(2048);
        let audio_engine = AudioEngine::new(to_gui_tx, from_gui_rx);
        let jack_backend = JackBackend::new(audio_engine);
        let mut backend = Backend::new(from_audio_rx, to_audio_tx);
        backend
            .library
            .set_ui_height_arc(Some(LIBRARY_HEIGHT.clone()));

        App {
            backend,
            jack_backend,
            tui_widget_state: Default::default(),
        }
    }
}
impl App {
    pub fn on_tick(&mut self) {}

    pub fn run<B: tui::backend::Backend>(&mut self, terminal: &mut Terminal<B>) -> io::Result<()> {
        let mut last_tick = Instant::now();

        let mut is_press: HashMap<KeyCode, bool> = HashMap::new();
        is_press.insert(KeyCode::Esc, false);
        loop {
            self.backend.calculate_state();

            terminal.draw(|f| ui(f, self))?;

            let timeout = TICK_RATE
                .checked_sub(last_tick.elapsed())
                .unwrap_or_else(|| Duration::from_secs(0));

            if event::poll(timeout)? {
                match self.handle_key(&mut is_press, terminal)? {
                    HandleKeyEvent::Continue => continue,
                    HandleKeyEvent::Noop => (),
                    HandleKeyEvent::Break => break Ok(()),
                };
            }

            if last_tick.elapsed() >= TICK_RATE {
                self.on_tick();
                last_tick = Instant::now();
            }
        }
    }
}
