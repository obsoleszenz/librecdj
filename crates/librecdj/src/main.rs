mod app;
mod key;
mod metadata;
mod ui;

pub use crossterm::{
    cursor,
    event::{
        DisableMouseCapture, EnableMouseCapture, KeyboardEnhancementFlags,
        PopKeyboardEnhancementFlags, PushKeyboardEnhancementFlags,
    },
    execute, terminal,
    terminal::{enable_raw_mode, Clear, ClearType, EnterAlternateScreen, LeaveAlternateScreen},
};

use tracing::error;
use tracing_subscriber::prelude::*;

use std::{
    error::Error,
    io,
    process::{Command, Stdio},
};
use tui::{backend::CrosstermBackend, Terminal};

use crate::app::App;

// hack around https://github.com/Windfisch/rust-assert-no-alloc/issues/7
#[cfg(not(all(windows, target_env = "gnu")))]
#[global_allocator]
static A: assert_no_alloc::AllocDisabler = assert_no_alloc::AllocDisabler;

pub mod colors {
    use tui::style::Color;

    pub static DECK_A: Color = Color::Rgb(152, 216, 170);
    pub static DECK_B: Color = Color::Rgb(247, 208, 96);
    pub static DECK_C: Color = Color::Rgb(201, 120, 255);
    pub static DECK_D: Color = Color::Rgb(255, 109, 96);

    pub fn deck_color(deck_index: usize) -> Color {
        match deck_index {
            0 => DECK_A,
            1 => DECK_B,
            2 => DECK_C,
            3 => DECK_D,
            _ => unreachable!(),
        }
    }
}

fn restore_terminal() {
    let mut stdout = io::stdout();

    // restore terminal
    terminal::disable_raw_mode().unwrap();
    execute!(
        stdout,
        LeaveAlternateScreen,
        DisableMouseCapture,
        cursor::Show,
        PopKeyboardEnhancementFlags
    )
    .unwrap();
}

fn init_terminal() -> Result<(), Box<dyn Error>> {
    let mut stdout = io::stdout();
    // setup terminal
    enable_raw_mode()?;
    execute!(
        stdout,
        EnterAlternateScreen,
        EnableMouseCapture,
        PushKeyboardEnhancementFlags(KeyboardEnhancementFlags::DISAMBIGUATE_ESCAPE_CODES),
        PushKeyboardEnhancementFlags(KeyboardEnhancementFlags::REPORT_ALL_KEYS_AS_ESCAPE_CODES),
        PushKeyboardEnhancementFlags(KeyboardEnhancementFlags::REPORT_EVENT_TYPES),
    )?;

    Ok(())
}

pub fn open_editor(command: &str, args: &[&str]) {
    restore_terminal();

    let mut child = Command::new(command)
        .args(args)
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .env_clear()
        .spawn()
        .expect("printenv failed to start");
    child.wait().unwrap();
    execute!(io::stdout(), Clear(ClearType::All)).unwrap();

    init_terminal().unwrap();
}

fn register_panic_hook() {
    std::panic::set_hook(Box::new(move |panic_info| {
        let bt = std::backtrace::Backtrace::capture();
        restore_terminal();
        error!("panic hook: {:?}", panic_info);
        println!("{:?}", panic_info);
        println!("{}", bt);
    }));
}

fn main() -> Result<(), Box<dyn Error>> {
    tracing_subscriber::registry()
        .with(tui_logger::tracing_subscriber_layer())
        .init(); // create app and run it

    register_panic_hook();
    let stdout = io::stdout();
    init_terminal()?;

    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    tui_logger::set_default_level(log::LevelFilter::Trace);
    tui_logger::set_log_file("./log").unwrap();

    if let Err(err) = App::default().run(&mut terminal) {
        error!("Error in App: {err:?}");
    }

    // restore terminal
    restore_terminal();

    Ok(())
}
