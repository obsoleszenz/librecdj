use std::{collections::HashMap, io, time::Duration};

use crossterm::event::{self, Event, KeyCode, KeyEvent, KeyEventKind, KeyModifiers};
use libbackend::{
    keyboardbindings::{
        key_event::LCDJKeyEvent, LCDJKeyCode, LCDJKeyCombination, LCDJKeyEventKind,
    },
    library::{LibraryCommand, LibraryItem},
    state::Screen,
};
use tracing::trace;
use tui::Terminal;

use crate::{app::App, metadata::edit_metadata_with_editor};

pub enum HandleKeyEvent {
    Continue,
    Noop,
    Break,
}

impl App {
    pub fn handle_key<B: tui::backend::Backend>(
        &mut self,
        is_press: &mut HashMap<KeyCode, bool>,
        terminal: &mut Terminal<B>,
    ) -> io::Result<HandleKeyEvent> {
        macro_rules! cont {
            () => {
                return Ok(HandleKeyEvent::Continue)
            };
        }
        if self.backend.state.screen == Screen::Exit {
            return Ok(HandleKeyEvent::Break);
        }
        if let Event::Key(key) = event::read()? {
            // Quick&dirty fix to get press&release on enter key
            let key = if is_press.contains_key(&key.code) {
                let press = !is_press.get(&key.code).unwrap();
                is_press.insert(key.code, press);
                KeyEvent {
                    code: key.code,
                    modifiers: key.modifiers,
                    kind: if press {
                        KeyEventKind::Press
                    } else {
                        KeyEventKind::Release
                    },
                    state: key.state,
                }
            } else {
                key
            };
            trace!(
                "Key: {:?} {:?} {:?} {:?}",
                key.code,
                key.kind,
                key.state,
                key.modifiers
            );

            // We use a tui specific widget, so we can't handle this in the backend
            if self.backend.state.screen == Screen::Log {
                if let KeyEvent {
                    code:
                        KeyCode::Up
                        | KeyCode::Down
                        | KeyCode::Left
                        | KeyCode::Right
                        | KeyCode::PageUp
                        | KeyCode::PageDown
                        | KeyCode::Enter,
                    kind: KeyEventKind::Press | KeyEventKind::Repeat,
                    ..
                } = key
                {
                    let transition = match key.code {
                        KeyCode::Left => tui_logger::TuiWidgetEvent::LeftKey,
                        KeyCode::Right => tui_logger::TuiWidgetEvent::RightKey,
                        KeyCode::Up => tui_logger::TuiWidgetEvent::UpKey,
                        KeyCode::Down => tui_logger::TuiWidgetEvent::DownKey,
                        KeyCode::PageUp => tui_logger::TuiWidgetEvent::PrevPageKey,
                        KeyCode::PageDown => tui_logger::TuiWidgetEvent::NextPageKey,
                        KeyCode::Enter => tui_logger::TuiWidgetEvent::FocusKey,
                        _ => unreachable!(),
                    };
                    self.tui_widget_state.transition(&transition)
                }
            }

            // Handle keyboard mapping
            let key_code = match convert_crossterm_key_code_to_keyboard_mapping_key_code(key.code) {
                Some(key_code) => key_code,
                None => cont!(),
            };
            let event_kind =
                convert_crossterm_key_event_kind_to_keyboard_mapping_key_event_kind(key.kind);

            let key_combination =
                convert_crossterm_key_modifiers_to_lcdj_key_combination(key_code, key.modifiers);

            if let Some(key_combination) = key_combination {
                let key_event = LCDJKeyEvent::new(key_combination, event_kind);
                self.backend.on_key(key_event);
            }

            // TODO: Port to backend
            if key.kind != KeyEventKind::Press {
                cont!();
            }

            if let KeyCode::Char('g') = key.code {
                let library_item =
                    &self.backend.state.library.items[self.backend.state.library.selected];
                let file_path = match &library_item {
                    LibraryItem::Dir(_) => cont!(),
                    LibraryItem::File { path, .. } => path,
                    LibraryItem::Section { .. } => cont!(),
                };

                edit_metadata_with_editor(file_path);
                terminal.clear().expect("Failed clearing terminal");
                self.backend
                    .library
                    .dispatch(LibraryCommand::Refresh)
                    .expect("Failed dispatching library action");
                // Flush events
                while let Ok(true) = event::poll(Duration::from_millis(125)) {
                    event::read().ok();
                }
            }
        }
        Ok(HandleKeyEvent::Noop)
    }
}

pub fn convert_crossterm_key_code_to_keyboard_mapping_key_code(
    key_code: KeyCode,
) -> Option<LCDJKeyCode> {
    Some(match key_code {
        KeyCode::Backspace => LCDJKeyCode::Backspace,
        KeyCode::Enter => LCDJKeyCode::Enter,
        KeyCode::Left => LCDJKeyCode::Left,
        KeyCode::Right => LCDJKeyCode::Right,
        KeyCode::Up => LCDJKeyCode::Up,
        KeyCode::Down => LCDJKeyCode::Down,
        KeyCode::Home => LCDJKeyCode::Home,
        KeyCode::End => LCDJKeyCode::End,
        KeyCode::PageUp => LCDJKeyCode::PageUp,
        KeyCode::PageDown => LCDJKeyCode::PageDown,
        KeyCode::Tab => LCDJKeyCode::Tab,
        KeyCode::BackTab => LCDJKeyCode::BackTab,
        KeyCode::Delete => LCDJKeyCode::Delete,
        KeyCode::Insert => LCDJKeyCode::Insert,
        KeyCode::F(n) => LCDJKeyCode::F(n),
        KeyCode::Char(c) => LCDJKeyCode::Char(c),
        KeyCode::Null => LCDJKeyCode::Null,
        KeyCode::Esc => LCDJKeyCode::Esc,
        KeyCode::CapsLock => LCDJKeyCode::CapsLock,
        KeyCode::ScrollLock => LCDJKeyCode::ScrollLock,
        KeyCode::NumLock => LCDJKeyCode::NumLock,
        KeyCode::PrintScreen => LCDJKeyCode::PrintScreen,
        KeyCode::Pause => LCDJKeyCode::Pause,
        KeyCode::Menu => LCDJKeyCode::Menu,
        KeyCode::KeypadBegin => LCDJKeyCode::KeypadBegin,
        KeyCode::Media(_) => return None,
        KeyCode::Modifier(_) => return None,
    })
}

pub fn convert_crossterm_key_event_kind_to_keyboard_mapping_key_event_kind(
    event_kind: KeyEventKind,
) -> LCDJKeyEventKind {
    match event_kind {
        KeyEventKind::Press => LCDJKeyEventKind::Press,
        KeyEventKind::Repeat => LCDJKeyEventKind::Repeat,
        KeyEventKind::Release => LCDJKeyEventKind::Release,
    }
}

pub fn convert_crossterm_key_modifiers_to_lcdj_key_combination(
    key_code: LCDJKeyCode,
    modifiers: KeyModifiers,
) -> Option<LCDJKeyCombination> {
    Some(if modifiers.is_empty() {
        LCDJKeyCombination::Single(key_code)
    } else if modifiers == KeyModifiers::SHIFT {
        LCDJKeyCombination::Shift(key_code)
    } else if modifiers == KeyModifiers::CONTROL {
        LCDJKeyCombination::Ctrl(key_code)
    } else if modifiers == KeyModifiers::ALT {
        LCDJKeyCombination::Alt(key_code)
    } else if modifiers == KeyModifiers::SHIFT ^ KeyModifiers::CONTROL {
        LCDJKeyCombination::CtrlShift(key_code)
    } else if modifiers == KeyModifiers::CONTROL ^ KeyModifiers::ALT {
        LCDJKeyCombination::CtrlAlt(key_code)
    } else if modifiers == KeyModifiers::CONTROL ^ KeyModifiers::SHIFT ^ KeyModifiers::ALT {
        LCDJKeyCombination::CtrlAltShift(key_code)
    } else {
        return None;
    })
}
