use crate::{audio_engine::AudioEngineCommand, backend::BackendCommand, library::LibraryCommand};

pub enum Command {
    Backend(BackendCommand),
    AudioEngine(AudioEngineCommand),
    Library(LibraryCommand),
}

impl From<BackendCommand> for Command {
    fn from(value: BackendCommand) -> Self {
        Self::Backend(value)
    }
}

impl From<AudioEngineCommand> for Command {
    fn from(value: AudioEngineCommand) -> Self {
        Self::AudioEngine(value)
    }
}

impl From<LibraryCommand> for Command {
    fn from(value: LibraryCommand) -> Self {
        Self::Library(value)
    }
}
