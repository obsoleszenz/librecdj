pub struct LowPass {
    b: f64,
    y: f64,
}
impl LowPass {
    pub fn new(decay: f64) -> Self {
        Self {
            b: 1.0 - decay,
            y: 0.0,
        }
    }
    pub fn reset(&mut self) {
        self.y = 0.0;
    }

    pub fn insert(&mut self, x: f64) -> f64 {
        self.y += self.b * (x - self.y);
        self.y
    }

    pub fn value(&self) -> f64 {
        self.y
    }
}
pub struct Jogwheel {
    intervals_per_rev: i32,
    rpm: f32,
    filter: LowPass,

    dx: f32,

    received_tick: bool,
    direction_forward: bool,
    is_scratching: bool,
    last_tempo: f32,

    

    interval_collector: f64,
}

impl Jogwheel {
    pub fn new(intervals_per_rev: i32, rpm: f32) -> Self {
        let intervals_per_second = (rpm * intervals_per_rev as f32) / 60.0;
        let dx = 1.0 / intervals_per_second;

        Self {
            intervals_per_rev,
            rpm,
            filter: LowPass::new(0.2),

            dx,

            received_tick: false,
            direction_forward: true,
            is_scratching: false,
            last_tempo: 0.0,
            interval_collector: 0.0,

        }
    }

    pub fn set_scratching(&mut self, is_scratching: bool) {
        self.is_scratching = is_scratching;
        self.received_tick = true;
    }

    pub fn scratching(&self) -> bool {
        self.is_scratching
    }

    pub fn tick(&mut self, _time: u32, interval: i32) {
        let interval = interval as f64 / self.intervals_per_rev as f64;
        self.direction_forward = interval >= 0.0;
        self.interval_collector += self.filter.insert(interval);
        self.received_tick = true;
    }

    /// Return the relative tempo change
    pub fn process(&mut self, sample_rate: f32, frame_size: usize) -> Option<(bool, f32)> {
        self.interval_collector = 0.0;
        if self.received_tick {
            let relative_tempo_change = self.filter.value() as f32;
            self.last_tempo = relative_tempo_change;

            debug!("relative_tempo_change={relative_tempo_change}");
            self.received_tick = false;
            Some((self.direction_forward, self.last_tempo))
        } else if self.last_tempo != 0.0 {
            if self.filter.insert(self.last_tempo as f64 / 4.0_f64) < 0.0001 {
                self.filter.reset();
            }
            let relative_tempo_change = self.filter.value() as f32;
            self.last_tempo = relative_tempo_change;
            self.direction_forward = true;
            Some((self.direction_forward, self.last_tempo))
        } else {
            None
        }
    }
}
