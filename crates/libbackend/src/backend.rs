use libplayer::{
    BeatPosition, Conditional, LoopKind, LoopSize, PlayerCommand, PlayerStateMetadata, Position,
    TempoKind, Unit,
};
use rtrb::{Consumer, Producer};
use serde::{Deserialize, Serialize};
use std::{
    any::Any,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
    thread::JoinHandle,
    time::Duration,
};

use crate::{
    audio_engine::{AudioEngineCommand, AudioEngineToGUIMessage},
    command::Command,
    keyboardbindings::{
        key_event::LCDJKeyEvent, KeyboardBindings, LCDJKeyCode, LCDJKeyCombination,
    },
    library::{Library, LibraryCommand, LibraryEvent},
    state::{MainMenuState, PopupResult, PopupState, PopupTrait, Screen, State, WaveformMode},
};

const FROM_AUDIO_RECV_LOOP_SLEEP: Duration = Duration::from_millis(1000 / 30);

pub type DeckIndex = usize;

#[derive(Debug)]
pub enum BeatSize {
    Absolute(f32),
    Double,
    Halve,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum BackendCommand {
    SelectDeck(DeckIndex),
    CloneDeck(DeckIndex, DeckIndex),
    WaveformMode(Option<WaveformMode>),
    Test,
}

/// Start and control the backend.
/// This starts multiple threads including the audio thread.
pub struct Backend {
    stop: Arc<AtomicBool>,
    to_audio_dispatcher_thread: JoinHandle<()>,
    from_audio_dispatcher_thread: JoinHandle<()>,
    pub to_audio_mpsc_tx: crossbeam::channel::Sender<AudioEngineCommand>,
    from_audio_mpsc_rx: crossbeam::channel::Receiver<AudioEngineToGUIMessage>,
    #[allow(dead_code)]
    from_audio_mpsc_tx: crossbeam::channel::Sender<AudioEngineToGUIMessage>,
    pub state: State,
    pub library: Library,

    pub keyboard_bindings: KeyboardBindings,
}

impl Backend {
    pub fn new(
        mut from_audio_rx: Consumer<AudioEngineToGUIMessage>,
        mut to_audio_tx: Producer<AudioEngineCommand>,
    ) -> Self {
        let (to_audio_mpsc_tx, to_audio_mpsc_rx) = crossbeam::channel::bounded(2048);
        let (from_audio_mpsc_tx, from_audio_mpsc_rx) = crossbeam::channel::bounded(2048);

        let stop = Arc::new(AtomicBool::new(false));

        let cloned_stop = stop.clone();
        let to_audio_dispatcher_thread = std::thread::spawn(move || {
            debug!("to_audio_dispatcher_thread started");
            while !cloned_stop.load(Ordering::Relaxed) {
                if let Ok(to_audio_message) = to_audio_mpsc_rx.recv() {
                    to_audio_tx.push(to_audio_message).unwrap();
                }
            }
            debug!("to_audio_dispatcher_thread stopped");
        });

        let cloned_stop = stop.clone();
        let from_audio_mpsc_tx_clone = from_audio_mpsc_tx.clone();
        let from_audio_dispatcher_thread = std::thread::spawn(move || {
            debug!("from_audio_dispatcher_thread started");
            while !cloned_stop.load(Ordering::Relaxed) {
                if let Ok(message) = from_audio_rx.pop() {
                    from_audio_mpsc_tx_clone.send(message).unwrap();
                } else {
                    std::thread::sleep(FROM_AUDIO_RECV_LOOP_SLEEP);
                }
            }
            debug!("from_audio_dispatcher_thread stopped");
        });

        // Load keyboard bindings
        let keyboard_bindings = if let Some(config_dir) = dirs::config_dir() {
            match std::fs::read_to_string(config_dir.join("librecdj/keyboard.ron")) {
                Ok(input) => match KeyboardBindings::load_from_str(&input) {
                    Ok(keyboard_bindings) => keyboard_bindings,
                    Err(err) => {
                        panic!(
                            "Failed loading keyboard bindings.\n{}",
                            err.to_pretty_error(&input)
                        );
                    }
                },
                Err(_) => KeyboardBindings::default(),
            }
        } else {
            KeyboardBindings::default()
        };

        Self {
            stop,
            to_audio_dispatcher_thread,
            from_audio_dispatcher_thread,
            to_audio_mpsc_tx,
            from_audio_mpsc_rx,
            from_audio_mpsc_tx,
            state: State::default(),
            library: Library::new(),
            keyboard_bindings,
        }
    }

    pub fn calculate_state(&mut self) {
        // trace!("Calculating state");
        let mut state_changed = false;
        // trace!("Processing messages from audio thread");
        while let Ok(message) = self.from_audio_mpsc_rx.try_recv() {
            state_changed = true;
            // debug!("message={message:?}");

            match message {
                AudioEngineToGUIMessage::PlayerStates(player_states) =>
                {
                    #[allow(clippy::needless_range_loop)]
                    for deck_index in 0..4 {
                        self.state.decks[deck_index].player_state =
                            player_states[deck_index].clone();
                    }
                }
                AudioEngineToGUIMessage::Waveform(deck_index, waveform) => {
                    debug!("Received waveform for deck {}", deck_index);
                    self.state.decks[deck_index].waveform = Some(waveform);
                }
                AudioEngineToGUIMessage::LibraryCommand(library_action) => {
                    self.library_command(library_action);
                }
                AudioEngineToGUIMessage::BackendCommand(backend_action) => {
                    self.backend_command(backend_action);
                }
            }
        }

        // trace!("Updating library state");
        self.library.update(&mut self.state.library);

        while let Some(event) = self.library.event() {
            match event {
                LibraryEvent::Load {
                    path,
                    deck_index,
                    name: _,
                    metadata,
                    play,
                } => {
                    let main_bpm = self.calculate_main_bpm();
                    let sync_bpm = if main_bpm == 0.0 {
                        None
                    } else {
                        Some(main_bpm)
                    };
                    debug!("main_bpm={main_bpm:?} sync_bpm={sync_bpm:?}");
                    self.state.decks[deck_index].waveform.take();
                    self.deck_command(
                        deck_index,
                        PlayerCommand::Load {
                            path: path.clone(),
                            play: Some(play),
                        },
                    );
                    self.deck_command(deck_index, PlayerCommand::Play(Some(play), None));

                    if let Some(metadata) = &metadata {
                        self.state.decks[deck_index].metadata = metadata.clone().into();
                    }
                    let bpm = metadata.and_then(|m| m.bpm);
                    if let (Some(bpm), Some(sync_bpm)) = (bpm, sync_bpm) {
                        let tempo = calculate_tempo_to_match_bpm(bpm, sync_bpm);
                        self.deck_command(
                            deck_index,
                            PlayerCommand::Tempo(libplayer::TempoKind::Absolute(tempo)),
                        );
                    }
                    self.deck_command(
                        deck_index,
                        PlayerCommand::SetMetadata(PlayerStateMetadata { bpm }),
                    );
                }
            }
        }

        for i in 0..4 {
            let deck = &self.state.decks[i];
            let bpm = deck.player_state.bpm().unwrap_or(0.0);
            let sample_rate = deck.player_state.initial_sample_rate();
            let playhead_secs = deck.player_state.playhead_secs().unwrap_or(0.0);
            self.state.beat[i].bpm = bpm;
            let cue_pos_secs = deck.player_state.cue_position_secs().unwrap_or(0.0);
            self.state.positions[i] = BeatPosition {
                position_secs: playhead_secs,
                first_beat_secs: cue_pos_secs,
                bpm: bpm as f64,
            }
        }

        self.state.beat[4].bpm = self.calculate_main_bpm();
        if state_changed {
            //debug!("calculate_state: state_changed: {:?}", self.state);
        }
        // trace!("Done calculating state");
    }

    fn calculate_main_bpm(&self) -> f32 {
        let mut count_bpm = [(0.0, 0); 4];

        for i in 0..4 {
            let is_playing = self.state.decks[i].player_state.playing();
            if !is_playing {
                continue;
            }

            let bpm = match self.state.decks[i].player_state.bpm() {
                Some(bpm) => bpm,
                None => continue,
            };

            // We only want and need a precision of 2 decimal places
            let bpm = (bpm * 100.0).round();
            #[allow(clippy::needless_range_loop)]
            for c in 0..4 {
                if count_bpm[c].0 == bpm {
                    count_bpm[c].1 += 1;
                    break;
                } else if count_bpm[c].0 == 0.0 {
                    count_bpm[c].0 = bpm;
                    count_bpm[c].1 = 1;
                    break;
                }
            }
        }
        count_bpm.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());

        let main_bpm = if count_bpm[3].1 == 1 && count_bpm[2].1 == 1 {
            0.0
        } else {
            count_bpm.last().unwrap().0 / 100.0
        };

        if main_bpm == 0.0 {
            if let Some(first_loaded_deck) = self
                .state
                .decks
                .iter()
                .find(|deck| deck.player_state.loaded())
            {
                first_loaded_deck.player_state.bpm().unwrap_or(0.0)
            } else {
                0.0
            }
        } else {
            main_bpm
        }
    }

    pub fn play_pause(&mut self, deck_index: usize) {
        self.to_audio_mpsc_tx
            .send(AudioEngineCommand::Deck {
                index: deck_index,
                time: 0,
                command: PlayerCommand::Play(None, None),
            })
            .expect("GUI to audio channel full!");
    }

    pub fn seek(&mut self, deck_index: usize, diff_seconds: f32) {
        self.to_audio_mpsc_tx
            .send(AudioEngineCommand::Deck {
                index: deck_index,
                time: 0,
                command: PlayerCommand::Seek(Position::Relative(Unit::Time(diff_seconds))),
            })
            .expect("GUI to audio channel full!");
    }
    pub fn tempo(&mut self, deck_index: Option<usize>, value: f32) {
        if let Some(index) = deck_index {
            self.to_audio_mpsc_tx
                .send(AudioEngineCommand::Deck {
                    index,
                    time: 0,
                    command: PlayerCommand::Tempo(TempoKind::Relative(value)),
                })
                .expect("GUI to audio channel full!");
        } else {
            self.to_audio_mpsc_tx
                .send(AudioEngineCommand::AllDecks {
                    time: 0,

                    command: PlayerCommand::Tempo(TempoKind::Percent(value)),
                })
                .expect("GUI to audio channel full!");
        }
    }

    pub fn deck_command(&mut self, deck_index: usize, command: PlayerCommand) {
        self.audio_engine_command(AudioEngineCommand::Deck {
            index: deck_index,
            time: 0,
            command,
        });
    }

    pub fn library_command(&mut self, action: LibraryCommand) {
        if let Err(_err) = self.library.dispatch(action) {
            error!("GUI to library channel full");
        }
    }

    /// TODO: Move into AudioEngineCommand
    pub fn backend_command(&mut self, action: BackendCommand) {
        match action {
            BackendCommand::SelectDeck(deck) => {
                self.set_selected_deck(deck);
            }
            BackendCommand::WaveformMode(waveform_mode) => {
                self.state.waveform_mode = match waveform_mode {
                    Some(waveform_mode) => waveform_mode,
                    None => self.state.waveform_mode.toggle(),
                }
            }
            BackendCommand::Test => todo!(),
            BackendCommand::CloneDeck(from, to) => {
                // We can't clone from the same deck to the same deck
                if from == to {
                    error!("Tried to clone deck to itself");
                    return;
                }
                self.state.decks[to].metadata = self.state.decks[from].metadata.clone();
                self.state.decks[to].waveform = self.state.decks[from].waveform.clone();
                if let Err(_err) = self
                    .to_audio_mpsc_tx
                    .send(AudioEngineCommand::CloneDeck { from, to })
                {
                    error!("Gui to audio channel full!");
                    return;
                }
                self.set_selected_deck(to);
            }
        }
    }

    pub fn audio_engine_command(&mut self, audio_engine_command: AudioEngineCommand) {
        if let Err(_err) = self.to_audio_mpsc_tx.send(audio_engine_command) {
            error!("GUI to audio channel full!");
        };
    }

    pub fn command(&mut self, command: Command) {
        match command {
            Command::Backend(backend_command) => self.backend_command(backend_command),
            Command::AudioEngine(ae_command) => self.audio_engine_command(ae_command),
            Command::Library(library_command) => self.library_command(library_command),
        }
    }

    pub fn jump(&mut self, deck_index: usize, beat: f32) {
        let bpm = self.state.decks[deck_index].player_state.initial_bpm();
        if bpm.is_some() {
            self.deck_command(
                deck_index,
                PlayerCommand::Seek(Position::Relative(Unit::Beat(beat))),
            );
        }
    }

    pub fn relative_loop(&mut self, deck_index: usize, beat: f32) {
        self.deck_command(
            deck_index,
            PlayerCommand::Loop(LoopKind::Set(
                Conditional::Enable,
                LoopSize::Relative(Unit::Beat(beat)),
            )),
        )
    }

    pub fn on_key(&mut self, key: LCDJKeyEvent) -> bool {
        debug!("on_key: key_event={key:?}");
        if let Some(mut popup) = self.state.popup.take() {
            let interaction_result = popup.on_interaction(self, &key);
            match interaction_result {
                PopupResult::Continue => self.state.popup = Some(popup),
                PopupResult::Close => {}
            }
            // No further keybindings should get executed
            return false;
        }

        // Some basic keys that we prioritize over the keybindings
        // for now
        'handle_by_keybinding: {
            match key.combination() {
                LCDJKeyCombination::Single(LCDJKeyCode::F(10)) if key.is_press() => {
                    self.state.screen = match self.state.screen {
                        Screen::Log => Screen::Main,
                        _ => Screen::Log,
                    }
                }
                LCDJKeyCombination::Single(LCDJKeyCode::Esc) if key.is_press() => {
                    if self.state.popup.is_none() {
                        self.state.popup = Some(PopupState::MainMenu(MainMenuState::default()));
                    } else {
                        self.state.popup = None;
                    }
                }
                LCDJKeyCombination::Single(LCDJKeyCode::Enter) if key.is_press() => {
                    // Load track to deck
                    self.library_command(LibraryCommand::Move {
                        load: Some((self.selected_deck(), false)),
                        position: crate::library::Position::Relative(0),
                    });
                }
                LCDJKeyCombination::Single(LCDJKeyCode::Up)
                | LCDJKeyCombination::Single(LCDJKeyCode::Down)
                | LCDJKeyCombination::Single(LCDJKeyCode::Left)
                | LCDJKeyCombination::Single(LCDJKeyCode::Right)
                | LCDJKeyCombination::Single(LCDJKeyCode::PageUp)
                | LCDJKeyCombination::Single(LCDJKeyCode::PageDown)
                    if key.is_press_or_repeat() =>
                {
                    let action = {
                        match key.combination() {
                            LCDJKeyCombination::Single(LCDJKeyCode::Left) => LibraryCommand::Back,
                            LCDJKeyCombination::Single(LCDJKeyCode::Right) => {
                                LibraryCommand::Select { load: None }
                            }
                            LCDJKeyCombination::Ctrl(LCDJKeyCode::Up) => LibraryCommand::Move {
                                position: crate::library::Position::Relative(-1),
                                load: Some((self.selected_deck(), true)),
                            },
                            LCDJKeyCombination::Single(LCDJKeyCode::Up) => LibraryCommand::Move {
                                position: crate::library::Position::Relative(-1),
                                load: None,
                            },
                            LCDJKeyCombination::Single(LCDJKeyCode::Down) => LibraryCommand::Move {
                                position: crate::library::Position::Relative(1),
                                load: None,
                            },
                            LCDJKeyCombination::Ctrl(LCDJKeyCode::Down) => LibraryCommand::Move {
                                position: crate::library::Position::Relative(1),
                                load: Some((self.selected_deck(), true)),
                            },
                            _ => break 'handle_by_keybinding,
                        }
                    };
                    self.library.dispatch(action).expect("Dispatch failed");
                }
                _ => (),
            }
        }

        let event_kind = key.kind();
        let key_combination = key.combination();
        let selected_deck = self.selected_deck();

        debug!("on_key combination={key_combination:?} kind={event_kind:?}");
        let commands = self
            .keyboard_bindings
            .on_key_combination(key_combination, event_kind)
            .map(|kcmds| {
                kcmds
                    .into_iter()
                    .map(|kcmd| kcmd.into_command(selected_deck))
            });
        if let Some(commands) = commands {
            for command in commands.into_iter() {
                self.command(command);
            }
        }
        false
    }

    #[allow(unused)]
    pub fn stop(self) -> Result<(), Box<dyn Any + Send>> {
        self.stop.store(true, Ordering::Relaxed);
        self.to_audio_dispatcher_thread.join()?;
        self.from_audio_dispatcher_thread.join()?;
        Ok(())
    }

    pub fn set_selected_deck(&mut self, deck_index: usize) {
        if deck_index > self.state.decks.len() {
            error!("deck_index > self.state.decks.len()");
            return;
        }
        self.state.selected_deck_index = deck_index;
    }

    pub fn selected_deck(&self) -> usize {
        self.state.selected_deck_index
    }
}

fn calculate_tempo_to_match_bpm(original_bpm: f32, target_bpm: f32) -> f32 {
    target_bpm / original_bpm
}

#[cfg(test)]
mod tests {
    use crate::backend::calculate_tempo_to_match_bpm;

    #[test]
    fn test_calculate_tempo_to_match_bpm() {
        assert_eq!(calculate_tempo_to_match_bpm(100.0, 200.0), 2.0);
        assert_eq!(calculate_tempo_to_match_bpm(100.0, 50.0), 0.5);
    }
}
