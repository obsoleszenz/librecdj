use libplayer::{Player, PlayerCommand, PlayerState};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use waveform::{calculate_waveform, Waveform};

#[cfg(target_arch = "x86_64")]
use std::arch::x86_64::_MM_FLUSH_ZERO_ON;
use std::arch::x86_64::_MM_SET_FLUSH_ZERO_MODE;
use std::sync::Arc;
use std::time::Duration;

use crate::{backend::BackendCommand, library::LibraryCommand};

const JOB_LOOP_SLEEP: Duration = Duration::from_millis(1000 / 60);

#[derive(Debug, Deserialize, Serialize)]
pub enum AudioEngineCommand {
    Deck {
        index: usize,
        time: u32,
        command: PlayerCommand,
    },
    AllDecks {
        time: u32,
        command: PlayerCommand,
    },
    CloneDeck {
        from: usize,
        to: usize,
    },
    Library(LibraryCommand),
    Backend(BackendCommand),
}

impl From<BackendCommand> for AudioEngineCommand {
    fn from(value: BackendCommand) -> Self {
        AudioEngineCommand::Backend(value)
    }
}

impl From<LibraryCommand> for AudioEngineCommand {
    fn from(value: LibraryCommand) -> Self {
        AudioEngineCommand::Library(value)
    }
}

/// TODO: Could this not just be [`Command`] ?
#[derive(Debug)]
pub enum GuiToAudioEngineMessage {}

#[derive(Debug)]
pub enum AudioEngineToGUIMessage {
    PlayerStates([PlayerState; 4]),
    Waveform(usize, Waveform),
    LibraryCommand(LibraryCommand),
    BackendCommand(BackendCommand),
}

#[derive(Error, Debug)]
pub enum AudioEngineError {}

/// AudioEngine is the central entry point into the audio processing code.
/// When the audio backend API (currently only JACK) requests new buffers
/// of audio to pass to the audio interface, AudioEngine is responsible for
/// telling everything that generates audio to do so, then mixing the signals
/// into buffers that get passed to the audio backend (currently only
/// JackProcessHandler).
pub struct AudioEngine {
    pub decks: [Player; 4],

    /// Stores the currently used sample_rate
    /// This equals to last sample_rate_out value passed to [`AudioEngine::process`]
    /// In case [`AudioEngine::process`] got never called, the value is -1.0
    sample_rate: f32,

    /// Stores the currently used sample_rate
    /// This equals to last len of buffer a out l passed to [`AudioEngine::process`]
    /// In case [`AudioEngine::process`] got never called, the value is 0
    frame_size: usize,

    pub to_gui_tx: rtrb::Producer<AudioEngineToGUIMessage>,
    from_gui_rx: rtrb::Consumer<AudioEngineCommand>,

    send_job: rtrb::Producer<Job>,
    recv_job_result: rtrb::Consumer<JobResult>,

    /// The number of frames in the current process cycle.
    frame_count: u32,
}

impl AudioEngine {
    pub fn new(
        to_gui_tx: rtrb::Producer<AudioEngineToGUIMessage>,
        from_gui_rx: rtrb::Consumer<AudioEngineCommand>,
    ) -> AudioEngine {
        let decks = [Player::new(), Player::new(), Player::new(), Player::new()];

        let (send_job, recv_job) = rtrb::RingBuffer::new(8);
        let (send_job_result, recv_job_result) = rtrb::RingBuffer::new(8);

        std::thread::spawn(move || {
            let mut recv_job = recv_job;
            let mut send_job_result = send_job_result;

            loop {
                if let Ok(job) = recv_job.pop() {
                    match job {
                        Job::CalculateWaveform {
                            deck_index,
                            stereo_samples,
                            sample_rate,
                        } => {
                            let waveform = calculate_waveform(stereo_samples, sample_rate, 64);
                            send_job_result
                                .push(JobResult::Waveform(deck_index, waveform))
                                .expect("Failed sending job result");
                        }
                    }
                } else {
                    std::thread::sleep(JOB_LOOP_SLEEP);
                }
            }
        });

        AudioEngine {
            decks,
            to_gui_tx,
            from_gui_rx,

            send_job,
            recv_job_result,

            sample_rate: -1.0,
            frame_size: 0,

            frame_count: 0,
        }
    }

    pub fn set_frame_count(&mut self, frame_count: u32) {
        self.frame_count = frame_count
    }

    pub fn frame_count(&self) -> u32 {
        self.frame_count
    }

    pub fn command(&mut self, action: AudioEngineCommand) {
        debug!("send_action action={action:?}");
        match action {
            AudioEngineCommand::Deck {
                index: deck_index,
                time,
                command,
            } => match self.decks.get_mut(deck_index) {
                Some(deck) => deck.command(time, command),
                None => error!("Invalid deck index: {deck_index}"),
            },

            AudioEngineCommand::Library(library_action) => self
                .to_gui_tx
                .push(AudioEngineToGUIMessage::LibraryCommand(library_action))
                .expect("channel full"),
            AudioEngineCommand::Backend(backend_action) => self
                .to_gui_tx
                .push(AudioEngineToGUIMessage::BackendCommand(backend_action))
                .expect("channel full"),
            AudioEngineCommand::AllDecks { time, command } => self
                .decks
                .iter_mut()
                .for_each(|d| d.command(time, command.clone())),
            AudioEngineCommand::CloneDeck { from, to } => {
                if from == to {
                    return;
                }
                let (from, to) = get_two_mut_references_of_slice(&mut self.decks, from, to);
                to.clone_from_player(from).expect("Failed cloning deck");
            }
        };
    }

    pub fn before_process(&mut self) {
        self.process_gui_messages();
    }

    pub fn sample_rate(&self) -> f32 {
        self.sample_rate
    }
    pub fn frame_size(&self) -> usize {
        self.frame_size
    }

    pub fn process(
        &mut self,
        buffers_out_interleaved: [&mut [f32]; 8],
        sample_rate_out: usize,
    ) -> [PlayerState; 4] {
        // Store requested sample rate from audio host
        self.sample_rate = sample_rate_out as f32;

        //assert_no_alloc::assert_no_alloc(|| {

        // TODO: use no_denormals crate
        #[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
        unsafe {
            _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
        }

        let [out_deck_a_l, out_deck_a_r, out_deck_b_l, out_deck_b_r, out_deck_c_l, out_deck_c_r, out_deck_d_l, out_deck_d_r] =
            buffers_out_interleaved;

        {
            self.frame_size = out_deck_a_l.len();
            let decks = &mut self.decks;
            decks[0]
                .proceed_and_fill_buffer(
                    out_deck_a_l,
                    out_deck_a_r,
                    sample_rate_out,
                    self.frame_size,
                )
                .expect("failed");
            decks[1]
                .proceed_and_fill_buffer(
                    out_deck_b_l,
                    out_deck_b_r,
                    sample_rate_out,
                    self.frame_size,
                )
                .expect("failed");
            decks[2]
                .proceed_and_fill_buffer(
                    out_deck_c_l,
                    out_deck_c_r,
                    sample_rate_out,
                    self.frame_size,
                )
                .expect("failed");
            decks[3]
                .proceed_and_fill_buffer(
                    out_deck_d_l,
                    out_deck_d_r,
                    sample_rate_out,
                    self.frame_size,
                )
                .expect("failed");
        }

        // TODO: Instead of stack allocating these PlayerState, accept &mut [PlayerState; 4] as an argument to this method
        let mut states = [
            PlayerState::default(),
            PlayerState::default(),
            PlayerState::default(),
            PlayerState::default(),
        ];

        for (i, deck) in self.decks.iter_mut().enumerate() {
            while let Some(event) = deck.event() {
                debug!("Event {:?} on deck {}", event, i);
                match event {
                    libplayer::PlayerEvent::FileLoaded => {
                        if let Some(ref samples) = deck.sample_loader.samples {
                            let stereo_samples = samples.samples.clone();
                            let sample_rate = samples.rate;
                            self.send_job
                                .push(Job::CalculateWaveform {
                                    deck_index: i,
                                    stereo_samples,
                                    sample_rate,
                                })
                                .expect("Failed sending job");
                        }
                        debug!("Fil");
                    }
                    libplayer::PlayerEvent::ResamplerReady => todo!(),
                }
            }
            deck.state.clone_into(&mut states[i]);
        }

        states
    }

    pub fn after_process(&mut self, states: [PlayerState; 4]) {
        while let Ok(job_result) = self.recv_job_result.pop() {
            match job_result {
                JobResult::Waveform(deck_index, waveform) => {
                    self.to_gui_tx
                        .push(AudioEngineToGUIMessage::Waveform(deck_index, waveform))
                        .ok();
                }
            }
        }

        self.to_gui_tx
            .push(AudioEngineToGUIMessage::PlayerStates(states))
            .ok();
    }

    fn process_gui_messages(&mut self) {
        while self.from_gui_rx.slots() > 0 {
            let command = self.from_gui_rx.pop().unwrap();
            self.command(command);
        }
    }
}

// Get two mutable references of an array
// This is little bit ugly hack around the borrow checker.
// See https://stackoverflow.com/questions/30073684/how-to-get-mutable-references-to-two-array-elements-at-the-same-time
fn get_two_mut_references_of_slice<T, const C: usize>(
    arr: &mut [T; C],
    a_pos: usize,
    b_pos: usize,
) -> (&mut T, &mut T) {
    assert_ne!(a_pos, b_pos, "a_pos is not allowed to equal b_pos");
    if a_pos < b_pos {
        let (a, b) = arr.split_at_mut(b_pos);
        (&mut a[a_pos], &mut b[0])
    } else {
        let (b, a) = arr.split_at_mut(a_pos);
        (&mut a[0], &mut b[b_pos])
    }
}

enum Job {
    CalculateWaveform {
        deck_index: usize,
        stereo_samples: Arc<Vec<Vec<f32>>>,
        sample_rate: usize,
    },
}

enum JobResult {
    Waveform(usize, Waveform),
}
