use crate::{
    keyboardbindings::{LCDJKeyCode, LCDJKeyCombination, LCDJKeyEventKind},
    library::LibraryCommand,
};

use super::{PopupResult, PopupTrait};

#[derive(Debug, Clone)]
pub struct DeletePlaylistItemState {
    pub selected: usize,
    pub filename: String,
}

impl PopupTrait for DeletePlaylistItemState {
    fn on_interaction(
        &mut self,
        backend: &mut crate::backend::Backend,
        event: &crate::keyboardbindings::key_event::LCDJKeyEvent,
    ) -> super::PopupResult {
        if event.kind() == &LCDJKeyEventKind::Press {
            match event.combination() {
                LCDJKeyCombination::Single(LCDJKeyCode::Enter) => {
                    backend.library_command(LibraryCommand::RemoveItem);
                    return PopupResult::Close;
                }
                LCDJKeyCombination::Single(LCDJKeyCode::Esc) => {
                    return PopupResult::Close;
                }
                _ => (),
            }
        }

        PopupResult::Continue
    }
}
