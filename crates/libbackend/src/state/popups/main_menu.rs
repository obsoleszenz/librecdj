use crate::{
    backend::Backend,
    keyboardbindings::{key_event::LCDJKeyEvent, LCDJKeyCombination, LCDJKeyEventKind},
    library::LibraryItem,
    state::{DeletePlaylistItemState, PopupState, Screen},
};

use super::{PopupResult, PopupTrait};

#[derive(Default, Debug, PartialEq, Clone)]
pub struct MainMenuState {}

impl PopupTrait for MainMenuState {
    fn on_interaction(&mut self, backend: &mut Backend, event: &LCDJKeyEvent) -> PopupResult {
        debug!("Hello! event={event:?}");
        if event.kind() == &LCDJKeyEventKind::Press {
            match event.combination() {
                LCDJKeyCombination::Single(crate::keyboardbindings::LCDJKeyCode::Esc) => {
                    return PopupResult::Close;
                }
                LCDJKeyCombination::Single(crate::keyboardbindings::LCDJKeyCode::Char('q')) => {
                    backend.state.screen = Screen::Exit;
                }
                LCDJKeyCombination::Single(crate::keyboardbindings::LCDJKeyCode::Char('a')) => {
                    backend.state.popup = PopupState::open_add_to_playlist();
                    return PopupResult::Close;
                }
                LCDJKeyCombination::Single(crate::keyboardbindings::LCDJKeyCode::Char('d')) => {
                    if let Some((
                        selected,
                        LibraryItem::File {
                            name,
                            path: _,
                            metadata: _,
                        },
                    )) = backend.state.library.selected()
                    {
                        backend.state.popup =
                            Some(PopupState::DeletePlaylistItem(DeletePlaylistItemState {
                                selected,
                                filename: name.clone(),
                            }));
                    }
                    return PopupResult::Close;
                }
                LCDJKeyCombination::Single(crate::keyboardbindings::LCDJKeyCode::Char('j')) => {
                    if backend.state.library.sections.is_empty() {
                        return PopupResult::Continue;
                    }
                    backend.state.popup = PopupState::open_jump_to_section(
                        &backend.state.library.items,
                        &backend.state.library.sections,
                    );
                    return PopupResult::Close;
                }
                LCDJKeyCombination::Single(crate::keyboardbindings::LCDJKeyCode::Char('m')) => {
                    if backend.state.library.sections.is_empty() {
                        return PopupResult::Continue;
                    }
                    backend.state.popup = PopupState::open_move_to_section(
                        &backend.state.library.items,
                        &backend.state.library.sections,
                    );
                    return PopupResult::Close;
                }
                _ => (),
            }
        }
        PopupResult::Continue
    }
}
