use std::path::Path;

use crate::{
    keyboardbindings::{LCDJKeyCode, LCDJKeyCombination, LCDJKeyEventKind},
    library::{add_to_m3u, LibraryItem},
    state::{virtual_list::VirtualList, Playlist},
};

use super::{PopupResult, PopupTrait};

#[derive(Debug)]
pub struct AddToPlaylistState {
    pub vlist: VirtualList<Playlist>,
}

impl PopupTrait for AddToPlaylistState {
    fn on_interaction(
        &mut self,
        backend: &mut crate::backend::Backend,
        event: &crate::keyboardbindings::key_event::LCDJKeyEvent,
    ) -> PopupResult {
        if event.is_press_or_repeat() {
            match event.combination() {
                LCDJKeyCombination::Single(LCDJKeyCode::Esc) => return PopupResult::Close,
                LCDJKeyCombination::Single(LCDJKeyCode::Up) => self.vlist.select_relative(-1),
                LCDJKeyCombination::Single(LCDJKeyCode::Down) => self.vlist.select_relative(1),
                LCDJKeyCombination::Single(LCDJKeyCode::Enter) => {
                    let selected_library_item =
                        &backend.state.library.items[backend.state.library.selected];
                    let selected_playlist = self.vlist.selected();

                    let library_item_path = match selected_library_item {
                        LibraryItem::Dir(_) => return PopupResult::Continue,
                        LibraryItem::File { path, .. } => path,
                        LibraryItem::Section { name: _ } => return PopupResult::Continue,
                    };

                    add_to_m3u(
                        Path::new(&selected_playlist.path),
                        Path::new(&library_item_path),
                    )
                    .ok();
                    return PopupResult::Close;
                }
                _ => (),
            }
        }
        PopupResult::Continue
    }
}
