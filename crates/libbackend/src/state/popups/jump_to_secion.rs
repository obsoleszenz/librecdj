use crate::{
    keyboardbindings::{LCDJKeyCode, LCDJKeyCombination, LCDJKeyEventKind},
    library::{LibraryCommand, Position},
    state::virtual_list::VirtualList,
};

use super::{PopupResult, PopupTrait, SectionItem};

#[derive(Debug)]
pub struct JumpToSectionState {
    pub vlist: VirtualList<SectionItem>,
}

impl PopupTrait for JumpToSectionState {
    fn on_interaction(
        &mut self,
        backend: &mut crate::backend::Backend,
        event: &crate::keyboardbindings::key_event::LCDJKeyEvent,
    ) -> super::PopupResult {
        if event.is_press_or_repeat() {
            match event.combination() {
                LCDJKeyCombination::Single(LCDJKeyCode::Esc) => {
                    return PopupResult::Close;
                }
                LCDJKeyCombination::Single(LCDJKeyCode::Up) => self.vlist.select_relative(-1),
                LCDJKeyCombination::Single(LCDJKeyCode::Down) => self.vlist.select_relative(1),
                LCDJKeyCombination::Single(LCDJKeyCode::Enter) => {
                    let selected_section_id = self.vlist.selected();

                    backend.library_command(LibraryCommand::Move {
                        position: Position::StartOfSection(selected_section_id.1),
                        load: None,
                    });
                    return PopupResult::Close;
                }

                _ => (),
            }
        }

        PopupResult::Continue
    }
}
