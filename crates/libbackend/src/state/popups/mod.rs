pub mod add_to_playlist;
pub mod delete_playlist_item;
pub mod jump_to_secion;
pub mod main_menu;
pub mod move_to_section;

use std::{cell::RefCell, env, path::Path, time::SystemTime};

pub use add_to_playlist::AddToPlaylistState;
pub use delete_playlist_item::DeletePlaylistItemState;
pub use jump_to_secion::JumpToSectionState;
pub use main_menu::MainMenuState;
pub use move_to_section::MoveToSectionState;

use crate::{
    backend::Backend,
    keyboardbindings::key_event::LCDJKeyEvent,
    library::{LibraryItem, M3USections},
};

use super::{virtual_list::VirtualList, Playlist};

#[derive(Debug)]
pub enum PopupState {
    AddToPlaylist(AddToPlaylistState),
    DeletePlaylistItem(DeletePlaylistItemState),
    MainMenu(MainMenuState),
    MoveToM3USection(MoveToSectionState),
    JumpToM3USection(JumpToSectionState),
}

impl PopupState {
    pub fn load_playlists() -> Vec<Playlist> {
        let playlist_path =
            env::var("PLAYLISTS").unwrap_or("/home/k/Musik/Techno/Playlists".to_string());
        let path = Path::new(&playlist_path);
        if let Ok(dir_items) = path.read_dir() {
            let mut playlist_items = dir_items
                .filter_map(|f| {
                    if let Ok(f) = f {
                        let name = f.file_name().to_string_lossy().into_owned();
                        let file_type = f.file_type().expect("Failed getting file type");
                        if file_type.is_file() && name.ends_with(".m3u") || name.ends_with(".m3u8")
                        {
                            let path = path.join(&name).to_string_lossy().to_string();
                            let metadata = f.metadata().expect("Failed getting metadata");
                            let modified =
                                metadata.modified().expect("Failed getting modified date");
                            return Some((Playlist { name, path }, modified));
                        }
                    }

                    None
                })
                .collect::<Vec<(Playlist, SystemTime)>>();
            playlist_items.sort_by(|(_, modified_a), (_, modified_b)| {
                modified_b
                    .partial_cmp(modified_a)
                    .expect("Failed at partial_cmp")
            });

            return playlist_items
                .into_iter()
                .map(|(playlist, _)| playlist)
                .collect();
        }

        vec![]
    }

    pub fn open_add_to_playlist() -> Option<Self> {
        Some(Self::AddToPlaylist(AddToPlaylistState {
            vlist: VirtualList::new(0, PopupState::load_playlists()),
        }))
    }

    pub fn open_move_to_section(items: &Vec<LibraryItem>, sections: &M3USections) -> Option<Self> {
        let items = sections
            .iter()
            .enumerate()
            .map(|(section_id, section_pos)| {
                let section_name = items[*section_pos].name().to_string();
                (section_name, section_id)
            })
            .collect::<Vec<SectionItem>>();
        info!("SectionItems: {:?}", items);
        Some(Self::MoveToM3USection(MoveToSectionState {
            vlist: VirtualList::new(0, items),
        }))
    }

    pub fn open_jump_to_section(items: &Vec<LibraryItem>, sections: &M3USections) -> Option<Self> {
        let items = sections
            .iter()
            .enumerate()
            .map(|(section_id, section_pos)| {
                let section_name = items[*section_pos].name().to_string();
                (section_name, section_id)
            })
            .collect::<Vec<SectionItem>>();
        info!("SectionItems: {:?}", items);
        Some(Self::JumpToM3USection(JumpToSectionState {
            vlist: VirtualList::new(0, items),
        }))
    }
}

impl PopupTrait for PopupState {
    fn on_interaction(&mut self, backend: &mut Backend, event: &LCDJKeyEvent) -> PopupResult {
        match self {
            PopupState::MainMenu(main_menu) => main_menu.on_interaction(backend, event),
            PopupState::DeletePlaylistItem(del) => del.on_interaction(backend, event),
            PopupState::JumpToM3USection(jmp) => jmp.on_interaction(backend, event),
            PopupState::AddToPlaylist(atp) => atp.on_interaction(backend, event),
            PopupState::MoveToM3USection(mtms) => mtms.on_interaction(backend, event),
        }
    }
}

pub enum PopupResult {
    Continue,
    Close,
}

pub trait PopupTrait {
    fn on_interaction(&mut self, backend: &mut Backend, event: &LCDJKeyEvent) -> PopupResult {
        debug!("PopupTrait default impl");
        PopupResult::Continue
    }
}

pub type SectionItem = (String, usize);
