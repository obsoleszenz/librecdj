use crate::{
    backend::Backend,
    keyboardbindings::{key_event::LCDJKeyEvent, LCDJKeyCode, LCDJKeyCombination},
    library::{LibraryCommand, Position},
    state::virtual_list::VirtualList,
};

use super::{PopupResult, PopupTrait, SectionItem};

#[derive(Debug)]
pub struct MoveToSectionState {
    pub vlist: VirtualList<SectionItem>,
}

impl PopupTrait for MoveToSectionState {
    fn on_interaction(&mut self, backend: &mut Backend, key: &LCDJKeyEvent) -> PopupResult {
        match key.combination() {
            LCDJKeyCombination::Single(LCDJKeyCode::Esc) if key.is_press() => {
                return PopupResult::Close;
            }
            LCDJKeyCombination::Single(LCDJKeyCode::Up) if key.is_press() => {
                self.vlist.select_relative(-1)
            }
            LCDJKeyCombination::Single(LCDJKeyCode::Down) if key.is_press() => {
                self.vlist.select_relative(1)
            }
            LCDJKeyCombination::Single(LCDJKeyCode::Enter) if key.is_press() => {
                let selected_section_id = self.vlist.selected();

                backend.library_command(LibraryCommand::MoveItem(Position::EndOfSection(
                    selected_section_id.1,
                )));
                return PopupResult::Close;
            }
            _ => (),
        }
        PopupResult::Continue
    }
}
