#[derive(Debug, Clone, PartialEq)]
pub struct VirtualList<T> {
    pub selected: usize,
    pub items: Vec<T>,
}

impl<T> VirtualList<T> {
    pub fn new(selected: usize, items: Vec<T>) -> Self {
        Self { selected, items }
    }

    pub fn select_relative(&mut self, relative: i32) {
        self.selected = if self.items.is_empty() {
            0
        } else {
            ((self.selected as i32) + relative)
                .max(0)
                .min(self.items.len() as i32 - 1) as usize
        }
    }

    pub fn selected(&self) -> &T {
        &self.items[self.selected]
    }

    pub fn len(&self) -> usize {
        self.items.len()
    }

    pub fn is_empty(&self) -> bool {
        self.items.is_empty()
    }
}
