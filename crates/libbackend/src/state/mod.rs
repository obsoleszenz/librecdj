pub mod popups;
pub mod virtual_list;

use std::{env, path::Path, time::SystemTime};

use libmetadata::Metadata;
use libplayer::{BeatPosition, LoopKind, PlayerState};
pub use popups::*;
use serde::{Deserialize, Serialize};
use waveform::Waveform;

use crate::library::LibraryState;

#[derive(PartialEq, Default, Debug, Copy, Clone, Deserialize, Serialize)]
pub enum WaveformMode {
    #[default]
    Zoom,
    Overview,
}

impl WaveformMode {
    pub fn toggle(self) -> Self {
        match self {
            WaveformMode::Zoom => WaveformMode::Overview,
            WaveformMode::Overview => WaveformMode::Zoom,
        }
    }
}

#[derive(Debug)]
pub struct State {
    pub sample_rate: usize,
    pub buffer_size_frames: usize,
    pub time_elapsed: f32,
    pub decks: [Deck; 4],
    pub main_bpm: Option<f32>,
    pub library: LibraryState,
    pub selected_deck_index: usize,

    pub screen: Screen,
    // 0 - 3 is decks, 4 is main bpm
    pub beat: [BeatState; 5],
    pub positions: [BeatPosition; 5],
    pub popup: Option<PopupState>,
    pub waveform_mode: WaveformMode,
}

impl State {
    pub fn popup_mut(&mut self) -> &mut Option<PopupState> {
        &mut self.popup
    }

    pub fn active_deck(&self) -> &Deck {
        &self.decks[self.selected_deck_index]
    }
    pub fn active_deck_mut(&mut self) -> &mut Deck {
        &mut self.decks[self.selected_deck_index]
    }
}

impl Default for State {
    fn default() -> Self {
        let deck_a = Deck::with_deck_short('A');
        let deck_b = Deck::with_deck_short('B');
        let deck_c = Deck::with_deck_short('C');
        let deck_d = Deck::with_deck_short('D');
        Self {
            sample_rate: 0,
            buffer_size_frames: 0,
            time_elapsed: 0.0,
            decks: [deck_a, deck_b, deck_c, deck_d],
            main_bpm: None,
            library: LibraryState::default(),
            selected_deck_index: 0,
            screen: Screen::default(),
            beat: [BeatState::default(); 5],
            positions: [BeatPosition::default(); 5],
            popup: None,
            waveform_mode: WaveformMode::default(),
        }
    }
}

#[derive(Clone, Default, Debug)]
pub struct DeckMetadata {
    pub artist: String,
    pub title: String,
    pub key: String,
    pub comment: String,
    pub mood: String,
}

impl From<Metadata> for DeckMetadata {
    fn from(value: Metadata) -> Self {
        Self {
            artist: value.artist,
            title: value.title,
            key: value.key,
            comment: value.comment,
            mood: value.mood,
        }
    }
}

#[derive(Clone, Default, Debug)]
pub struct Deck {
    pub deck_short: char,
    pub metadata: DeckMetadata,
    pub player_state: PlayerState,
    pub waveform: Option<Waveform>,
    pub position: bool,
}

impl Deck {
    pub fn with_deck_short(deck_short: char) -> Self {
        Deck {
            deck_short,
            ..Default::default()
        }
    }
}

#[derive(Default, Debug, PartialEq)]
pub enum Screen {
    #[default]
    Main,
    Log,
    Exit,
}

#[derive(Default, Debug, Clone, Copy)]
pub struct BeatState {
    pub bpm: f32,
}

#[derive(Debug, Clone)]
pub struct Playlist {
    pub path: String,
    pub name: String,
}
