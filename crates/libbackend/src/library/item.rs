use libmetadata::Metadata;

#[derive(Debug, Clone)]
pub enum LibraryItem {
    Dir(String),
    File {
        name: String,
        path: String,
        metadata: Option<Metadata>,
    },
    Section {
        name: String,
    },
}

impl LibraryItem {
    pub fn is_dir(&self) -> bool {
        if let LibraryItem::Dir(_) = self {
            return true;
        }
        false
    }

    #[allow(unused)]
    pub fn is_file(&self) -> bool {
        if let LibraryItem::File { .. } = self {
            return true;
        }
        false
    }
    #[allow(unused)]
    pub fn is_section(&self) -> bool {
        if let LibraryItem::Section { .. } = self {
            return true;
        }
        false
    }

    pub fn name(&self) -> &str {
        match &self {
            LibraryItem::Dir(name) => name,
            LibraryItem::File { name, .. } => name,
            LibraryItem::Section { name } => name,
        }
    }
}
