use libmetadata::Metadata;

pub enum LibraryEvent {
    Load {
        deck_index: usize,
        path: String,
        metadata: Option<Metadata>,
        name: String,
        play: bool,
    },
}
