use std::{
    fs::OpenOptions,
    io::{LineWriter, Write},
    path::Path,
};

use super::LibraryItem;

pub fn add_to_m3u(m3u_path: &Path, file_path: &Path) -> Result<(), ()> {
    if !m3u_path.is_file() {
        error!("path is not file: {:?}", m3u_path);
        return Err(());
    }

    let mut file = match OpenOptions::new().append(true).open(m3u_path) {
        Ok(file) => file,
        Err(err) => {
            error!("failed opening file: {}", err);
            return Err(());
        }
    };
    file.write(format!("{}\n", file_path.to_string_lossy()).as_bytes())
        .map_err(|_| ())?;
    Ok(())
}

pub fn write_m3u(m3u_path: &Path, items: &Vec<LibraryItem>) -> Result<(), ()> {
    if !m3u_path.is_file() {
        error!("path is not file: {:?}", m3u_path);
        return Err(());
    }

    let file = match OpenOptions::new().write(true).truncate(true).open(m3u_path) {
        Ok(file) => file,
        Err(err) => {
            error!("failed opening file: {}", err);
            return Err(());
        }
    };
    let mut file = LineWriter::new(file);

    for item in items {
        let line = match item {
            LibraryItem::Dir(_) => unreachable!(),
            LibraryItem::File { path, .. } => path.to_string(),
            LibraryItem::Section { name } => format!("#EXTSECT:{}", name),
        };
        if let Err(err) = file.write_all(format!("{}\n", line).as_bytes()) {
            error!("failed writing: {} {:?}", err, m3u_path);
        }
    }
    Ok(())
}
