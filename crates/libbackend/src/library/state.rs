use std::path::PathBuf;

use super::LibraryItem;

pub type M3USections = Vec<usize>;

#[derive(Default, Debug, Clone)]
pub struct LibraryState {
    pub path: PathBuf,
    pub selected: usize,
    pub items: Vec<LibraryItem>,
    pub sections: M3USections,
    pub is_playlist: bool,
}

impl LibraryState {
    pub fn selected(&self) -> Option<(usize, &LibraryItem)> {
        let selected = self.selected;
        self.items.get(selected).map(|i| (selected, i))
    }
}
