use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum Position {
    Relative(i32),
    StartOfSection(usize),
    EndOfSection(usize),
    RelativePage(i32),
    Start,
    End,
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
pub enum LibraryCommand {
    Select {
        load: Option<(usize, bool)>,
    },
    Back,
    Refresh,
    Move {
        position: Position,
        load: Option<(usize, bool)>,
    },
    MoveItem(Position),
    RemoveItem,
}
