use super::{
    command::LibraryCommand, event::LibraryEvent, state::LibraryState, worker::LibraryWorker,
};
use std::{
    env,
    path::PathBuf,
    sync::{
        atomic::{AtomicU16, Ordering},
        Arc, RwLock,
    },
};

use crossbeam::channel::{self, Receiver, Sender};
use serde::Serialize;

#[derive(Clone)]
pub struct AtomicUIHeight(pub Option<Arc<AtomicU16>>);

impl AtomicUIHeight {
    pub fn new() -> Self {
        Self(None)
    }
    pub fn ui_height(&self) -> Option<u16> {
        self.0.as_ref().map(|a| a.load(Ordering::Relaxed))
    }
}

pub struct Library {
    state_rx: Receiver<LibraryState>,
    action_tx: Sender<LibraryCommand>,
    recv_event: Receiver<LibraryEvent>,
    pub(crate) ui_height: Arc<RwLock<AtomicUIHeight>>,
}

impl Library {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        let (action_tx, action_rx) = channel::bounded(2);
        let (state_tx, state_rx) = channel::bounded(2);
        let (send_event, recv_event) = channel::bounded(64);

        let ui_height = Arc::new(RwLock::new(AtomicUIHeight::new()));
        let library_path =
            PathBuf::from(env::var("LIBRARY").unwrap_or("/home/k/Musik/Techno/".to_string()));

        LibraryWorker::new(
            library_path,
            state_tx,
            action_rx,
            send_event,
            ui_height.clone(),
        )
        .spawn();

        action_tx
            .send(LibraryCommand::Refresh)
            .expect("Failed sending refresh");

        Self {
            action_tx,
            state_rx,
            recv_event,
            ui_height,
        }
    }

    pub fn dispatch(
        &mut self,
        action: LibraryCommand,
    ) -> Result<(), crossbeam::channel::TrySendError<LibraryCommand>> {
        self.action_tx.try_send(action)
    }

    pub fn update(&self, state_to_update: &mut LibraryState) {
        if let Ok(state) = self.state_rx.try_recv() {
            *state_to_update = state;
        }
    }

    pub fn event(&self) -> Option<LibraryEvent> {
        self.recv_event.try_recv().ok()
    }

    // Share the library height of the ui with the backend.
    // This is needed for some calculations.
    // The AtomicU16 should contain the information how many library
    // items can be displayed. This Atomic should get updated when
    // the ui resizes.
    pub fn set_ui_height_arc(&mut self, height: Option<Arc<AtomicU16>>) {
        let mut ui_height = self
            .ui_height
            .write()
            .expect("Failed aquiring ui_height rwlock");
        ui_height.0 = height;
    }

    pub fn ui_height(&self) -> Option<u16> {
        self.ui_height
            .read()
            .expect("Failed aquiring ui_height rwlock")
            .ui_height()
    }
}
