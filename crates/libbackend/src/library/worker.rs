use libmetadata::read_metadata;
use std::{
    fs::File,
    io::{BufRead, BufReader},
    path::{Path, PathBuf},
    sync::{Arc, RwLock},
};

use path_clean::PathClean;

use crossbeam::channel::{Receiver, Sender};

use super::{
    write_m3u, AtomicUIHeight, LibraryCommand, LibraryEvent, LibraryItem, LibraryState,
    M3USections, Position,
};

pub(crate) struct LibraryWorker {
    state_tx: Sender<LibraryState>,
    command_rx: Receiver<LibraryCommand>,
    send_event: Sender<LibraryEvent>,
    ui_height: Arc<RwLock<AtomicUIHeight>>,
    state: LibraryState,
}

impl LibraryWorker {
    pub fn new(
        path: PathBuf,
        state_tx: Sender<LibraryState>,
        command_rx: Receiver<LibraryCommand>,
        send_event: Sender<LibraryEvent>,
        ui_height: Arc<RwLock<AtomicUIHeight>>,
    ) -> Self {
        Self {
            state_tx,
            command_rx,
            send_event,
            ui_height,
            state: LibraryState {
                path,
                ..Default::default()
            },
        }
    }
    pub fn spawn(self) {
        std::thread::spawn(move || self.run());
    }

    pub fn run(mut self) {
        loop {
            if let Ok(command) = self.command_rx.recv() {
                self.command(command)
            }
        }
    }

    pub fn command(&mut self, command: LibraryCommand) {
        match command {
            LibraryCommand::Select { load } => {
                let item = match self.state.items.get(self.state.selected) {
                    Some(item) => item,
                    None => return,
                };
                match item {
                    LibraryItem::Dir(name) => {
                        self.state.path = self.state.path.join(name);
                        self.state.items = refresh_folder(&self.state.path);
                        self.state.selected = 0;
                        self.state.is_playlist = false;
                    }
                    LibraryItem::File { name, .. } => {
                        if name.ends_with(".m3u") || name.ends_with(".m3u8") {
                            let m3u_path = self.state.path.join(name);
                            if let Ok((m3u_items, m3u_sections)) = refresh_m3u(&m3u_path) {
                                self.state.path = m3u_path;
                                self.state.items = m3u_items;
                                self.state.sections = m3u_sections;
                                self.state.selected = 0;
                                self.state.is_playlist = true;
                            }
                        } else if load.is_some() {
                            self.load_audio_file(self.state.selected, load);
                        }
                    }
                    _ => {}
                };
            }
            LibraryCommand::Back => {
                if let Some(parent) = self.state.path.parent() {
                    let parent_name = self
                        .state
                        .path
                        .file_name()
                        .unwrap()
                        .to_string_lossy()
                        .to_string();
                    self.state.path = parent.to_path_buf();
                    self.state.items = refresh_folder(&self.state.path);
                    self.state.selected = self
                        .state
                        .items
                        .iter()
                        .position(|p| {
                            if self.state.is_playlist {
                                if let LibraryItem::File { name, .. } = p {
                                    return name == &parent_name;
                                }
                            } else if let LibraryItem::Dir(p) = p {
                                return *p == parent_name;
                            }
                            false
                        })
                        .unwrap_or(0);
                    self.state.sections.clear();
                    self.state.is_playlist = false;
                };
            }
            LibraryCommand::Move { position, load } => {
                if self.state.items.is_empty() {
                    return;
                }
                self.state.selected = match position {
                    Position::Relative(relative) => (self.state.selected as i32 + relative)
                        .clamp(0, (self.state.items.len() as i32 - 1).max(0))
                        as usize,
                    Position::Start => 0,
                    Position::End => self.state.items.len() - 1,
                    Position::EndOfSection(_) => return,
                    Position::StartOfSection(section_id) => {
                        match self.state.sections.get(section_id) {
                            Some(section_pos) => *section_pos,
                            None => {
                                error!("Tried moving section with section_id={section_id} but id does not exist");
                                0
                            }
                        }
                    }
                    Position::RelativePage(pages) => (self.state.selected as i32
                        + pages * self.page_size())
                    .clamp(0, (self.state.items.len() as i32 - 1).max(0))
                        as usize,
                };

                self.load_audio_file(self.state.selected, load);
            }
            LibraryCommand::MoveItem(position) => {
                if !self.state.is_playlist {
                    return;
                }
                let items_len = self.state.items.len() as i32;
                let item = self.state.items.remove(self.state.selected);
                let insert_pos = match position {
                    Position::Relative(relative) => {
                        let insert_pos = (self.state.selected as i32 + relative)
                            .clamp(0, items_len - 1)
                            as usize;
                        self.state.selected = insert_pos;
                        insert_pos
                    }
                    Position::Start => 0,
                    Position::End => (items_len - 1) as usize,
                    Position::EndOfSection(section_id) => {
                        match self.state.sections.get(section_id) {
                            Some(_section_pos) => {
                                if section_id == self.state.sections.len() - 1 {
                                    (items_len - 1) as usize
                                } else {
                                    self.state.sections[section_id + 1]
                                }
                            }
                            None => {
                                error!("Tried moving library item to section with section_id={section_id} but id does not exist");
                                0
                            }
                        }
                    }
                    Position::StartOfSection(_) => return,
                    Position::RelativePage(pages) => {
                        let insert_pos = (self.state.selected as i32 + pages * self.page_size())
                            .clamp(0, items_len - 1)
                            as usize;
                        self.state.selected = insert_pos;
                        insert_pos
                    }
                };
                self.state.items.insert(insert_pos, item);
                self.state.sections = refresh_sections(&self.state.items);
                write_m3u(&self.state.path, &self.state.items).expect("Failed writing m3u");
            }
            LibraryCommand::RemoveItem => {
                if !self.state.is_playlist {
                    return;
                }
                self.state.items.remove(self.state.selected);
                debug!("{:?}", self.state.items);
                self.state.sections = refresh_sections(&self.state.items);
                write_m3u(&self.state.path, &self.state.items).expect("Failed writing m3u");
            }
            LibraryCommand::Refresh => {
                (self.state.items, self.state.sections) = if self.state.is_playlist {
                    refresh_m3u(&self.state.path).unwrap()
                } else {
                    (refresh_folder(&self.state.path), vec![])
                }
            }
        }
        let current_state = self.state.clone();
        self.state_tx
            .send(current_state)
            .expect("Error sending library state");
    }

    fn load_audio_file(&mut self, item_index: usize, load: Option<(usize, bool)>) {
        if let Some((deck_index, play)) = load {
            if let Some(LibraryItem::File {
                name,
                path,
                metadata,
            }) = self.state.items.get(item_index)
            {
                if is_audio_file(name) {
                    self.send_event
                        .send(LibraryEvent::Load {
                            deck_index,
                            path: path.clone(),
                            name: name.clone(),
                            metadata: metadata.clone(),
                            play,
                        })
                        .expect("Failed sending event");
                }
            }
        }
    }

    pub fn page_size(&self) -> i32 {
        ((self.ui_height() / 2) - 1) as i32
    }

    pub fn ui_height(&self) -> u16 {
        self.ui_height
            .read()
            .expect("Failed aquiring ui_height rwlock")
            .ui_height()
            .unwrap_or(10)
    }
}
fn refresh_folder(path: &Path) -> Vec<LibraryItem> {
    if let Ok(dir_items) = path.read_dir() {
        let mut items: Vec<LibraryItem> = dir_items
            .filter_map(|f| {
                if let Ok(f) = f {
                    let name = f.file_name().to_string_lossy().into_owned();
                    let file_type = f.file_type().expect("Failed getting file type");
                    return Some(if file_type.is_dir() {
                        LibraryItem::Dir(name)
                    } else {
                        let path = path.join(&name).to_string_lossy().to_string();
                        let metadata = read_metadata(&path).ok();
                        LibraryItem::File {
                            name,
                            metadata,
                            path,
                        }
                    });
                }

                None
            })
            .collect();
        let items_len = items.len();
        items.sort_by(|a, b| {
            fn weighting(item: &LibraryItem, items_len: usize) -> usize {
                match item {
                    LibraryItem::Dir(name) => name.chars().next().unwrap() as usize,
                    LibraryItem::File { metadata, .. } => {
                        if let Some(metadata) = &metadata {
                            'z' as usize + items_len + metadata.bpm.unwrap_or(0.0) as usize
                        } else {
                            'z' as usize + items_len
                        }
                    }
                    LibraryItem::Section { .. } => unreachable!(),
                }
            }
            if a.is_dir() || b.is_dir() || !is_audio_file(a.name()) || !is_audio_file(b.name()) {
                a.name().partial_cmp(b.name()).unwrap()
            } else {
                weighting(a, items_len)
                    .partial_cmp(&weighting(b, items_len))
                    .unwrap()
            }
        });
        return items;
    };
    vec![]
}
fn refresh_m3u(path: &Path) -> Result<(Vec<LibraryItem>, M3USections), ()> {
    if !path.is_file() {
        error!("path is not file: {:?}", path);
        return Err(());
    }

    let file = match File::open(path) {
        Ok(file) => file,
        Err(err) => {
            error!("failed opening file: {}", err);
            return Err(());
        }
    };
    let reader = BufReader::new(file);
    let mut items = vec![];
    for file_path_str in reader.lines() {
        let file_path_str = file_path_str.unwrap();

        if file_path_str.starts_with("#EXTSECT:") {
            let section_name = file_path_str
                .split(':')
                .skip(1)
                .take(1)
                .collect::<Vec<&str>>();

            if let Some(name) = section_name.first() {
                items.push(LibraryItem::Section {
                    name: name.to_string(),
                });
            }
            continue;
        } else if file_path_str.starts_with('#') {
            continue;
        }
        let file_path = Path::new(&file_path_str);
        debug!("path={path:?}");
        debug!("file_path={file_path:?}");
        let file_path = match file_path.is_absolute() {
            true => file_path.to_path_buf(),
            false => {
                let canoncial_path = path.join("..").join(file_path);
                canoncial_path.to_path_buf().clean()
            }
        };
        debug!("file_path={file_path:?}");
        let file_path_str = file_path.display().to_string();
        if !file_path.is_file() {
            continue;
        }
        let metadata = read_metadata(&file_path_str).ok();
        items.push(LibraryItem::File {
            name: file_path.file_name().unwrap().to_string_lossy().to_string(),
            path: file_path_str,
            metadata,
        });
    }
    let sections = refresh_sections(&items);
    Ok((items, sections))
}

fn refresh_sections(items: &[LibraryItem]) -> M3USections {
    let mut sections = vec![];

    for (pos, item) in items.iter().enumerate() {
        if let LibraryItem::Section { name: _ } = item {
            sections.push(pos);
        }
    }
    sections
}

pub fn is_audio_file(name: &str) -> bool {
    name.ends_with(".mp3")
        || name.ends_with(".aiff")
        || name.ends_with(".aif")
        || name.ends_with(".flac")
        || name.ends_with(".wav")
        || name.ends_with(".ogg")
}
