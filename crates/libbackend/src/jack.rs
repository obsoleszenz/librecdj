use crate::{
    audio_engine::{self, AudioEngine},
    midimapping::{registered_midi_mappings, Midi, MidiMappings, MAX_MIDI},
};

use jack::{Client, MidiIn, MidiWriter, Port, RawMidi};

/// JackProcessHandler responds to JACK's requests to generate new buffers
/// of audio to pass to the audio interface (or another JACK client).
struct JackProcessHandler {
    audio_engine: audio_engine::AudioEngine,
    midi_mappings: MidiMappings,

    out_deck_a_l: jack::Port<jack::AudioOut>,
    out_deck_a_r: jack::Port<jack::AudioOut>,
    out_deck_b_l: jack::Port<jack::AudioOut>,
    out_deck_b_r: jack::Port<jack::AudioOut>,
    out_deck_c_l: jack::Port<jack::AudioOut>,
    out_deck_c_r: jack::Port<jack::AudioOut>,
    out_deck_d_l: jack::Port<jack::AudioOut>,
    out_deck_d_r: jack::Port<jack::AudioOut>,

    ports_midi_in: PortsMidiIn,
    ports_midi_out: PortsMidiOut,
}

impl JackProcessHandler {
    #[allow(clippy::too_many_arguments)]
    fn new(
        audio_engine: audio_engine::AudioEngine,
        midi_mappings: MidiMappings,

        out_deck_a_l: jack::Port<jack::AudioOut>,
        out_deck_a_r: jack::Port<jack::AudioOut>,
        out_deck_b_l: jack::Port<jack::AudioOut>,
        out_deck_b_r: jack::Port<jack::AudioOut>,
        out_deck_c_l: jack::Port<jack::AudioOut>,
        out_deck_c_r: jack::Port<jack::AudioOut>,
        out_deck_d_l: jack::Port<jack::AudioOut>,
        out_deck_d_r: jack::Port<jack::AudioOut>,

        ports_midi_in: PortsMidiIn,
        ports_midi_out: PortsMidiOut,
    ) -> JackProcessHandler {
        JackProcessHandler {
            audio_engine,
            midi_mappings,

            out_deck_a_l,
            out_deck_a_r,
            out_deck_b_l,
            out_deck_b_r,
            out_deck_c_l,
            out_deck_c_r,
            out_deck_d_l,
            out_deck_d_r,

            ports_midi_in,
            ports_midi_out,
        }
    }

    fn midi_mappings_midi_in(
        midi_mappings: &mut MidiMappings,
        midi_writers: &mut [MidiWriter<'_>],
        ports_midi_in: &[Port<MidiIn>],
        audio_engine: &mut AudioEngine,
        ps: &jack::ProcessScope,
    ) {
        let frame_count = ps.last_frame_time();

        for i in 0..midi_mappings.len() {
            let midi_in_port = ports_midi_in.get(i).unwrap();
            let midi_out_writer = midi_writers.get_mut(i).unwrap();
            let mut send_midi = |midi: Midi| {
                trace!("Send midi: {midi:?}");
                midi_out_writer
                    .write(&RawMidi {
                        time: 0,
                        bytes: &midi.data[..],
                    })
                    .expect("Failed sending midi");
            };
            for midi in midi_in_port.iter(ps) {
                let midi: Midi = jack_midi_to_midi(midi);
                // audio_engine.on_midi(i, midi, &mut send_midi).unwrap()
                let midi_mapping_index = i;
                debug!(
                    "index={midi_mapping_index} midi={midi:?} frame_count={} time={}",
                    audio_engine.frame_count(),
                    midi.time
                );

                match midi_mappings.get_mut(midi_mapping_index) {
                    Some(midi_mapping) => {
                        match midi_mapping.on_midi_in(audio_engine, &mut send_midi, midi) {
                            Ok(()) => (),
                            Err(err) => error!("Error in midi mapping. err={err:?}"),
                        }
                    }
                    None => todo!(),
                };
            }
        }
    }
    fn midi_mappings_before_process(
        midi_mappings: &mut MidiMappings,
        midi_writers: &mut [MidiWriter<'_>],
        audio_engine: &mut AudioEngine,
    ) {
        for midi_mapping_index in 0..midi_mappings.len() {
            let midi_out_writer = midi_writers.get_mut(midi_mapping_index).unwrap();
            let mut send_midi = |midi: Midi| {
                trace!("Send midi: {midi:?}");
                midi_out_writer
                    .write(&RawMidi {
                        time: 0,
                        bytes: &midi.data[..],
                    })
                    .expect("Failed sending midi");
            };
            let midi_mapping = midi_mappings.get_mut(midi_mapping_index);
            if let Some(midi_mapping) = midi_mapping {
                midi_mapping.before_process(audio_engine, &mut send_midi);
            }
        }
    }
    fn midi_mappings_after_process(
        midi_mappings: &mut MidiMappings,
        midi_writers: &mut [MidiWriter<'_>],
        audio_engine: &mut AudioEngine,
    ) {
        for midi_mapping_index in 0..midi_mappings.len() {
            let midi_out_writer = midi_writers.get_mut(midi_mapping_index).unwrap();
            let mut send_midi = |midi: Midi| {
                trace!("Send midi: {midi:?}");
                midi_out_writer
                    .write(&RawMidi {
                        time: 0,
                        bytes: &midi.data[..],
                    })
                    .expect("Failed sending midi");
            };
            let midi_mapping = midi_mappings.get_mut(midi_mapping_index);
            if let Some(midi_mapping) = midi_mapping {
                midi_mapping.after_process(audio_engine, &mut send_midi);
            }
        }
    }
}

impl jack::ProcessHandler for JackProcessHandler {
    fn process(&mut self, client: &jack::Client, ps: &jack::ProcessScope) -> jack::Control {
        //assert_no_alloc::assert_no_alloc(|| {

        let sample_rate_out = client.sample_rate();
        let frame_count = ps.last_frame_time();
        self.audio_engine.set_frame_count(frame_count);
        // Get all midi out writers in advance as they
        // clean the midi buffer whenever a new one is get.
        let mut midi_writers: Vec<MidiWriter<'_>> = self
            .ports_midi_out
            .iter_mut()
            .map(|midi_out_port| midi_out_port.writer(ps))
            .collect();

        JackProcessHandler::midi_mappings_midi_in(
            &mut self.midi_mappings,
            &mut midi_writers,
            &self.ports_midi_in,
            &mut self.audio_engine,
            ps,
        );

        self.audio_engine.before_process();
        JackProcessHandler::midi_mappings_before_process(
            &mut self.midi_mappings,
            &mut midi_writers,
            &mut self.audio_engine,
        );

        let states = self.audio_engine.process(
            [
                self.out_deck_a_l.as_mut_slice(ps),
                self.out_deck_a_r.as_mut_slice(ps),
                self.out_deck_b_l.as_mut_slice(ps),
                self.out_deck_b_r.as_mut_slice(ps),
                self.out_deck_c_l.as_mut_slice(ps),
                self.out_deck_c_r.as_mut_slice(ps),
                self.out_deck_d_l.as_mut_slice(ps),
                self.out_deck_d_r.as_mut_slice(ps),
            ],
            sample_rate_out,
        );

        self.audio_engine.after_process(states);
        JackProcessHandler::midi_mappings_after_process(
            &mut self.midi_mappings,
            &mut midi_writers,
            &mut self.audio_engine,
        );
        jack::Control::Continue
    }

    fn buffer_size(&mut self, _: &jack::Client, buffer_size_frames: jack::Frames) -> jack::Control {
        info!("buffer size changed to {buffer_size_frames} frames");
        jack::Control::Continue
    }
}

/// JackBackend initializes a new JACK client, creates and connects its ports,
/// and tells JACK to start processing.
pub struct JackBackend {
    _async_client: jack::AsyncClient<(), JackProcessHandler>,
}

impl JackBackend {
    pub fn new(audio_engine: audio_engine::AudioEngine) -> JackBackend {
        let (jack_client, _status) =
            jack::Client::new("librecdj", jack::ClientOptions::NO_START_SERVER).unwrap();

        macro_rules! register_port_audio_out {
            ($name:expr) => {
                jack_client
                    .register_port($name, jack::AudioOut)
                    .expect(&format!("Failed registering audio out port {}", $name))
            };
        }
        let out_deck_a_l = register_port_audio_out!("deck_a_L");
        let out_deck_a_r = register_port_audio_out!("deck_a_R");
        let out_deck_b_l = register_port_audio_out!("deck_b_L");
        let out_deck_b_r = register_port_audio_out!("deck_b_R");
        let out_deck_c_l = register_port_audio_out!("deck_c_L");
        let out_deck_c_r = register_port_audio_out!("deck_c_R");
        let out_deck_d_l = register_port_audio_out!("deck_d_L");
        let out_deck_d_r = register_port_audio_out!("deck_d_R");

        let midi_mappings = registered_midi_mappings();

        let (ports_midi_in, ports_midi_out) =
            jack_register_midi_ports(&midi_mappings, &jack_client);

        let process_handler = JackProcessHandler::new(
            audio_engine,
            midi_mappings,
            out_deck_a_l,
            out_deck_a_r,
            out_deck_b_l,
            out_deck_b_r,
            out_deck_c_l,
            out_deck_c_r,
            out_deck_d_l,
            out_deck_d_r,
            ports_midi_in,
            ports_midi_out,
        );
        let async_client = jack_client.activate_async((), process_handler).unwrap();
        // activate_async consumes the client but it is still needed below.

        JackBackend {
            _async_client: async_client,
        }
    }
}

pub type PortsMidiIn = Vec<Port<jack::MidiIn>>;
pub type PortsMidiOut = Vec<Port<jack::MidiOut>>;

pub fn jack_register_midi_ports(
    midi_mappings: &MidiMappings,
    jack_client: &Client,
) -> (PortsMidiIn, PortsMidiOut) {
    macro_rules! register_port_midi_in {
        ($name:expr) => {
            jack_client
                .register_port($name, jack::MidiIn)
                .expect(&format!("Failed registering midi in port {}", $name))
        };
    }
    macro_rules! register_port_midi_out {
        ($name:expr) => {
            jack_client
                .register_port($name, jack::MidiOut)
                .expect(&format!("Failed registering midi out port {}", $name))
        };
    }
    let mut ports_midi_in: PortsMidiIn = Vec::new();
    let mut ports_midi_out: PortsMidiOut = Vec::new();

    for midi_mapping in midi_mappings {
        let name = midi_mapping.name();
        ports_midi_in.push(register_port_midi_in!(&format!("{} in", name)));
        ports_midi_out.push(register_port_midi_out!(&format!("{} out", name)));
    }

    (ports_midi_in, ports_midi_out)
}

pub fn jack_midi_to_midi(jack_midi: jack::RawMidi<'_>) -> Midi {
    let len = std::cmp::min(MAX_MIDI, jack_midi.bytes.len());
    let mut data = [0; MAX_MIDI];
    data[..len].copy_from_slice(&jack_midi.bytes[..len]);
    Midi {
        len,
        data,
        time: jack_midi.time,
    }
}
