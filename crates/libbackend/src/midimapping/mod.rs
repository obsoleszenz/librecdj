mod midi;
pub mod midi_mapping;

#[macro_use]
mod register;
mod mappings;

pub use self::{
    mappings::*,
    midi::{Midi, MAX_MIDI},
    midi_mapping::{MidiMapping, MidiMappings},
};
