/// This macro allows simple registration of controllers
/// See [`lib.rs`] for example
macro_rules! register_midi_mappings {
    ($($(#[cfg$feature:tt])? $file:ident::$struct_name: ident),+) => {
        use crate::midimapping::MidiMapping;

        $(
            $(#[cfg$feature])?
            mod $file;
        )+

        $(
            $(#[cfg$feature])?
            use crate::midimapping::mappings::$file::$struct_name;
        )+

        pub fn registered_midi_mappings() -> Vec<Box<dyn MidiMapping + Send>> {
            vec![
                $(
                    $(#[cfg$feature])?
                    Box::new($struct_name::new())
                ),+
            ]
        }
    };
}
