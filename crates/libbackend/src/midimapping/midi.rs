pub const MAX_MIDI: usize = 3;

/// A fixed size container for MIDI to copy data out of real-time thread
#[derive(Copy, Clone)]
pub struct Midi {
    pub len: usize,

    pub data: [u8; MAX_MIDI],
    pub time: u32,
}

impl std::fmt::Debug for Midi {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Midi")
            .field("data", &self.data)
            .field("data (as hex)", &format!("{:02X?}", &self.data))
            .field("status", &format!("{:02x?}", self.status()))
            .field("channel", &format!("{:02x?}", self.channel()))
            .finish()
    }
}

impl Midi {
    #[allow(unused)]
    pub fn new(status: u8, note: u8, value: u8) -> Self {
        Midi {
            len: 3,
            data: [status, note, value],
            time: 0,
        }
    }

    pub fn from(status_type: u8, channel: u8, note: u8, value: u8) -> Self {
        let status = (status_type << 4) + channel;
        Self::new(status, note, value)
    }
    pub fn status(&self) -> u8 {
        self.data[0] >> 4
    }
    pub fn channel(&self) -> u8 {
        self.data[0] & 0x0F
    }
    pub fn note(&self) -> u8 {
        self.data[1]
    }
    pub fn value(&self) -> u8 {
        self.data[2]
    }

    pub fn value_normalized(&self) -> f32 {
        self.value() as f32 / 127.0
    }
}
