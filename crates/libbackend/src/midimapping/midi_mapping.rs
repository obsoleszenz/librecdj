use crate::{audio_engine::AudioEngine, midimapping::midi::Midi};

pub type MidiMappings = Vec<Box<dyn MidiMapping + Send>>;
pub type SendMidi<'a> = &'a mut dyn FnMut(Midi);

pub trait MidiMapping {
    fn name(&self) -> String;
    fn on_midi_in(
        &mut self,
        audio_engine: &mut AudioEngine,
        send_midi: SendMidi<'_>,
        midi_in: Midi,
    ) -> Result<(), String>;
    fn before_process(&mut self, audio_engine: &mut AudioEngine, send_midi: SendMidi<'_>);
    fn after_process(&mut self, audio_engine: &mut AudioEngine, send_midi: SendMidi<'_>);
}
