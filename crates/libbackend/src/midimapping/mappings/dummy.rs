use crate::prelude::*;

pub struct Dummy {}

impl Dummy {
    pub fn new() -> Self {
        Self {}
    }
}

impl MidiMapping for Dummy {
    fn name(&self) -> String {
        "DummyController".into()
    }

    fn on_midi_in(
        &mut self,
        audio_engine: &mut AudioEngine,
        send_midi: SendMidi<'_>,
        midi_in: Midi,
    ) -> Result<(), String> {
        Ok(())
    }

    fn before_process(&mut self, audio_engine: &mut AudioEngine, send_midi: SendMidi<'_>) {}
    fn after_process(&mut self, audio_engine: &mut AudioEngine, send_midi: SendMidi<'_>) {}
}
