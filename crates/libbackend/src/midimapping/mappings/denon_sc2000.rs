use std::time::Instant;

use crate::{
    audio_engine::{AudioEngine, AudioEngineCommand}, backend::BackendCommand, library::{LibraryCommand, Position}, midimapping::{
        midi_mapping::SendMidi, Midi, MidiMapping
    },
    jogwheel::Jogwheel
};
use libplayer::{BeatSizeKind::{self, Jump, SetLoop}, CueKind, Direction, Conditional, JogWheelKind, LoopKind, LoopSize, NudgeKind, PlayerCommand, Position::Relative, ResizeKind, Unit};
use num_derive::{FromPrimitive, ToPrimitive};
use num_traits::FromPrimitive;

#[derive(Debug, PartialEq, FromPrimitive, ToPrimitive)]
pub enum MidiNote {
    DeckChange = 0x03,
    Cue = 0x42,
    Play = 0x43,
    JogWheel = 0x51,
    TrackBrowse = 0x54,
    TrackSelect = 0x28,
    Previous = 0x30,
    Browser = 0x64,
    SwitchDeck1 = 0x15,
    SwitchDeck2 = 0x12,
    SwitchDeck3 = 0x13,
    SwitchDeck4 = 0x14,
    DryWetFx2 = 0x55,
    Param1Fx2 = 0x52,
    Param2Fx2 = 0x53,
    ToggleLoop = 0x1D,
    BeatSizeDouble = 0x6A,
    BeatSizeHalve = 0x69,
    SetLoop = 0x68,
    BeatJump = 0x5D,
    Shift = 0x60,
    KeyLock = 0x06,
    NextTrack = 0x10,
    PreviousTrack = 0x11,
    HotCue1 = 0x17,
    HotCue2 = 0x18,
    HotCue3 = 0x19,
    HotCue4 = 0x20,
    HotCue5 = 0x21,
    HotCue6 = 0x22,
    HotCue7 = 0x23,
    HotCue8 = 0x24,
    PitchBendPlus = 0xc,
    PitchBendMinus = 0xd,
}
impl TryFrom<u8> for MidiNote {
    type Error = String;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        Self::from_u8(value).ok_or(format!("{:#x} is an unknown midi note", value))
    }
}


const LOOP_SIZES: [f32; 4] = [1.0, 2.0, 4.0, 16.0];



pub struct DenonSC2000 {
    last_db_led_update: Instant,
    max_db: [f32; 11],
    jogwheel: Jogwheel,
    active_deck: usize,

    shift_pressed: bool,
    key_lock_pressed: bool,
    frames_since_last_led_update: usize,
    last_tempo: Option<f32>,
    force_led_update: bool,

    jog_search_accum: i64,
}

impl DenonSC2000 {
    pub fn new() -> Self {
        Self {
            last_db_led_update: Instant::now(),
            max_db: [-70.0; 11],
            jogwheel: Jogwheel::new(
                2048,
                33.333,
            ),
            active_deck: 0,
            shift_pressed: false,
            key_lock_pressed: false,

            frames_since_last_led_update: 0,
            force_led_update: true,
            last_tempo: None,

            jog_search_accum: 0
        }
    }
}

impl MidiMapping for DenonSC2000 {
    fn name(&self) -> String {
        "DN-SC2000".into()
    }

    fn on_midi_in(
        &mut self,
        audio_engine: &mut AudioEngine,
        _send_midi: SendMidi<'_>,
        midi: Midi,
    ) -> Result<(), String> {
        debug!("midi: {midi:?}");
        let time = midi.time;
        let status = midi.status();
        // let channel: MidiChannel = midi.channel().try_into()?;
        let value = midi.value();
        let channel = ((midi.channel()) as usize).min(3);
        let is_press = value == 64;

        // Tempo Fader is not encoded in midi note but status
        if status == 0x0E {
            let value = ((value as u16) << 8) | midi.note() as u16;
            debug!("tempo fader value = {value}");
            let value: f32 = if value == 1024 {
                0.0 // Perfectly center
            } else {
                ((value as f32 / 32639.0) - 0.5) * 2.0
            };
            let value = 1.0 + value * 0.05;
            debug!("tempo fader value = {value}");
            if !self.shift_pressed {
                if let Some(last_tempo) = self.last_tempo {
                    let tempo_delta = last_tempo - value;
                    debug!("tempo delta = {tempo_delta}");
                    let player_command =
                        PlayerCommand::Tempo(libplayer::TempoKind::Percent(tempo_delta));
                    if self.key_lock_pressed {
                        audio_engine.command(AudioEngineCommand::Deck {
                            index: self.active_deck,
                            time,
                            command: player_command,
                        })
                    } else {
                        audio_engine.command(AudioEngineCommand::AllDecks {
                            time,
                            command: player_command,
                        });
                    }
                }
            }
            self.last_tempo = Some(value);
            return Ok(());
        }

        let note =midi.note().try_into()?;
        match note {
            MidiNote::Shift => {
                self.shift_pressed = value == 0x40;
            }
            MidiNote::KeyLock => {
                self.key_lock_pressed = value == 0x40;
            }
            MidiNote::Cue => {
                audio_engine.command(AudioEngineCommand::Deck {
                    index: self.active_deck,
                    time,
                    command: PlayerCommand::Cue(
                        match is_press {
                            true => CueKind::Press,
                            false => CueKind::Release,
                        },
                        None,
                    ),
                });
                self.force_led_update = true;
            }
            MidiNote::Play if is_press => {
                audio_engine.command(AudioEngineCommand::Deck {
                    index: self.active_deck,
                    time,
                    command: PlayerCommand::Play(None, None),
                });
                self.force_led_update = true;
            }
            // Search through track
            MidiNote::JogWheel if self.shift_pressed && status == 0xB => {
                let interval = (value as i32) - 64;
                self.jog_search_accum += interval as i64;
                const INTENSITY: i64 = 9;
                if self.jog_search_accum.abs() < INTENSITY{
                    return Ok(());
                }
                audio_engine.command(AudioEngineCommand::Deck {
                    index: self.active_deck,
                    time,
                    command: PlayerCommand::Seek(Relative(Unit::Beat((self.jog_search_accum / INTENSITY) as f32)))});
                self.jog_search_accum = 0;
            }
            // Jogwheel tick
            MidiNote::JogWheel if !self.shift_pressed && status == 0xB => {
                let interval = (value as i32) - 64;
                self.jogwheel.tick(time, interval);
            }
            // Joghweel touch
            MidiNote::JogWheel if status == 0x8 => {
                self.jogwheel.set_scratching(false);
            }
            MidiNote::JogWheel if !self.shift_pressed && status == 0x9 => {
                self.jogwheel.set_scratching(true);
            }
            MidiNote::DeckChange => {
                debug!("Changing active_deck to {channel}");
                self.active_deck = match (channel, self.active_deck) {
                    (0 | 2, 0 | 1) => 0,
                    (0 | 2, 2 | 3) => 2,
                    (1 | 3, 0 | 1) => 1,
                    (1 | 3, 2 | 3) => 3,
                    _ => channel
                    
                };
                audio_engine.command(AudioEngineCommand::Backend(BackendCommand::SelectDeck(self.active_deck)));
                self.force_led_update = true;
            }
            MidiNote::SwitchDeck1
            | MidiNote::SwitchDeck2
            | MidiNote::SwitchDeck3
            | MidiNote::SwitchDeck4
            | MidiNote::DryWetFx2
            | MidiNote::Param1Fx2
            | MidiNote::Param2Fx2
            | MidiNote::TrackBrowse // This is also Param2Fx3..
                if status == 0x8 =>
            {
                self.active_deck = match midi.note().try_into()? {
                    MidiNote::SwitchDeck1 | MidiNote::DryWetFx2=> 0,
                    MidiNote::SwitchDeck2 | MidiNote::Param1Fx2 => 1,
                    MidiNote::SwitchDeck3 | MidiNote::Param2Fx2 => 2,
                    MidiNote::SwitchDeck4 | MidiNote::TrackBrowse => 3,
                    _ => unreachable!(),
                };
                audio_engine.command(AudioEngineCommand::Backend(BackendCommand::SelectDeck(self.active_deck)));
                self.force_led_update = true;
            }
            MidiNote::TrackBrowse if status == 0xB => {
                let relative = if value == 0x00 { 1 } else { -1 };
                audio_engine.command(LibraryCommand::Move {
                    position: Position::Relative(relative),
                    load: None,
                }.into())
            }
            MidiNote::TrackSelect if status == 0x8 => audio_engine.command(LibraryCommand::Select {
                load: Some((self.active_deck, false)),
            }.into()),
            MidiNote::Previous if status == 0x8 => audio_engine.command(LibraryCommand::Back.into()),
            MidiNote::Browser if status == 0x8 => {
                audio_engine.command(AudioEngineCommand::Backend(BackendCommand::WaveformMode(None)))
            }
            MidiNote::ToggleLoop if status == 0x8 => {
                audio_engine.command(AudioEngineCommand::Deck {
                    index: self.active_deck,
                    time,
                    command: PlayerCommand::Loop(LoopKind::Enabled(Conditional::Toggle)),
                });
                self.force_led_update = true;
            },
            MidiNote::BeatSizeDouble if status == 0x8  => audio_engine.command(AudioEngineCommand::Deck { index: self.active_deck, time, command: PlayerCommand::BeatSize(BeatSizeKind::Resize(ResizeKind::double())) }),
            MidiNote::BeatSizeHalve if status == 0x8  => audio_engine.command(AudioEngineCommand::Deck { index: self.active_deck, time, command: PlayerCommand::BeatSize(BeatSizeKind::Resize(ResizeKind::halve())) }),
            MidiNote::BeatJump => {
                let direction = Direction::from_bool(value == 0x00);
                if self.shift_pressed {
                    audio_engine.command(AudioEngineCommand::Deck {
                        index: self.active_deck,
                        time,
                        command: PlayerCommand::Seek(Relative(Unit::Beat(direction.to_single_beat_f32())))});
                } else {
                    audio_engine.command(AudioEngineCommand::Deck {
                        index: self.active_deck,
                        time,
                        command: PlayerCommand::BeatSize(Jump { direction, with_loop: true })});
                }
            },

            MidiNote::SetLoop if value == 0x40 => {
                    audio_engine.command(AudioEngineCommand::Deck {
                        index: self.active_deck,
                        time,
                        command: 
                            PlayerCommand::BeatSize(SetLoop(Direction::from_bool(!self.shift_pressed)))
                    });
                self.force_led_update = true;
            }
            MidiNote::NextTrack if status == 0x8 => {
                audio_engine.command(
                        LibraryCommand::Move {
                             position: Position::Relative(1), 
                            load: Some((self.active_deck, true)) 
                        }.into()
                );
                self.force_led_update = true;
                
            }
            MidiNote::PreviousTrack if status == 0x8 => {
                audio_engine.command(
                        LibraryCommand::Move {
                             position: Position::Relative(-1), 
                            load: Some((self.active_deck, true)) 
                        }
                        .into()
                );
                self.force_led_update = true;
                
            }
            MidiNote::HotCue1
            | MidiNote::HotCue2
            | MidiNote::HotCue3
            | MidiNote::HotCue4
            | MidiNote::HotCue5
            | MidiNote::HotCue6
            | MidiNote::HotCue7
            | MidiNote::HotCue8  if status == 0x8 => {
                let loop_size = match note {
                    MidiNote::HotCue1 | MidiNote::HotCue5 => LOOP_SIZES[0],
                    MidiNote::HotCue2 | MidiNote::HotCue6 => LOOP_SIZES[1],
                    MidiNote::HotCue3 | MidiNote::HotCue7 => LOOP_SIZES[2],
                    MidiNote::HotCue4 | MidiNote::HotCue8 => LOOP_SIZES[3],
                    _ => unreachable!()
                };
                let loop_size = Unit::Beat(loop_size * Direction::from_bool(!self.shift_pressed).to_sign_f32());
                audio_engine.command(AudioEngineCommand::Deck {
                    index: self.active_deck,
                    time,
                    command: 
                        PlayerCommand::Loop(LoopKind::Set(Conditional::Enable, LoopSize::Relative(loop_size)))
                });
                self.force_led_update = true;
            }

            MidiNote::PitchBendPlus | MidiNote::PitchBendMinus => {
                self.force_led_update = true;
                if status == 0x8 {
                    audio_engine.command(AudioEngineCommand::Deck { index: self.active_deck, time, command: 
                        PlayerCommand::Nudge(NudgeKind::Disable),
                    });
                    return Ok(());                    
                }
                let direction = Direction::from_bool(note == MidiNote::PitchBendPlus);
                let tempo_playing =
                    if self.shift_pressed {
                        0.1
                    } else {
                        0.2
                    };
                let tempo_paused =
                    if self.shift_pressed {
                        0.4
                    } else {
                        0.2
                    };
                audio_engine.command(AudioEngineCommand::Deck { index: self.active_deck, time, command: 
                    PlayerCommand::Nudge(NudgeKind::Enable {
                        direction,
                        tempo_playing,
                        tempo_paused,
                    }),
                });
                
            }
            _ => (),
        }

        Ok(())
    }
    fn before_process(
        &mut self,
        audio_engine: &mut AudioEngine,
        _send_midi: SendMidi<'_>,
    ) {
        if let Some((direction_forward, relative_tempo_change)) = self.jogwheel.process(audio_engine.sample_rate(), audio_engine.frame_size()) {
            audio_engine.command(AudioEngineCommand::Deck {
                index: self.active_deck,
                time: 0,
                command: PlayerCommand::JogWheel(JogWheelKind {
                    is_scratching: self.jogwheel.scratching(),
                    interval: relative_tempo_change as f64,
                    direction_forward,
                }),
            })
        }
    }

    fn after_process(&mut self, audio_engine: &mut AudioEngine, send_midi: SendMidi<'_>) {
        if !self.force_led_update && self.frames_since_last_led_update < 20 {
            self.frames_since_last_led_update += 1;
            return;
        }
        self.force_led_update = false;
        self.frames_since_last_led_update = 0;
        const LED_FX1_CH1: u8 = 0x5C;
        const LED_FX1_CH2: u8 = 0x5D;
        const LED_FX1_CH3: u8 = 0x5E;
        const LED_FX1_CH4: u8 = 0x5F;
        const LEDS_FX1: [u8; 4] = [LED_FX1_CH1, LED_FX1_CH2, LED_FX1_CH3, LED_FX1_CH4];
        const LED_FX2_CH1: u8 = 0x60;
        const LED_FX2_CH2: u8 = 0x61;
        const LED_FX2_CH3: u8 = 0x62;
        const LED_FX2_CH4: u8 = 0x63;
        const LEDS_FX2: [u8; 4] = [LED_FX2_CH1, LED_FX2_CH2, LED_FX2_CH3, LED_FX2_CH4];

        macro_rules! send_midi_all_ch {
            ($status:expr, $note:expr, $value:expr) => {
                send_midi(Midi::new($status << 4 | 0x00, $note, $value));
                send_midi(Midi::new($status << 4 | 0x01, $note, $value));
            };
        }
        for i in 0..LEDS_FX1.len() {
            if i == self.active_deck {
                send_midi(Midi::new(0xB0, 0x4A, LEDS_FX1[i]));
                send_midi(Midi::new(0xB0, 0x4A, LEDS_FX2[i]));
                send_midi(Midi::new(0xB1, 0x4A, LEDS_FX1[i]));
                send_midi(Midi::new(0xB1, 0x4A, LEDS_FX2[i]));
            } else {
                send_midi(Midi::new(0xB0, 0x4B, LEDS_FX1[i]));
                send_midi(Midi::new(0xB0, 0x4B, LEDS_FX2[i]));
                send_midi(Midi::new(0xB1, 0x4B, LEDS_FX1[i]));
                send_midi(Midi::new(0xB1, 0x4B, LEDS_FX2[i]));
            }
        }

       let active_deck = &audio_engine.decks[self.active_deck];

       const LED_ON:u8 = 0x4A;
       const LED_OFF:u8 = 0x4B;
       const LED_BLINK :u8= 0x4C;
        fn led_midi_note(expr: bool) -> u8 {
            if expr {
                LED_ON
            } else {
                LED_OFF
            }
        }
        fn led_blink_midi_note(expr: bool) -> u8 {
            if expr {
                LED_BLINK
            } else {
                LED_OFF
            }
        }

        const LED_PLAY: u8 = 0x27;
        send_midi_all_ch!(
            0xB,
            led_midi_note(!active_deck.cueing() && active_deck.playing()),
            LED_PLAY
        );

        const LED_CUE: u8 = 0x26;
        send_midi_all_ch!(0xB, led_midi_note(active_deck.cueing()), LED_CUE);

        const LED_KEY_LOCK: u8 = 0x08;
        send_midi_all_ch!(0xB, led_midi_note(false), LED_KEY_LOCK);

        const LED_CUE1: u8 = 0x11;
        const LED_CUE2: u8 = 0x13;
        const LED_CUE3: u8 = 0x15;
        const LED_CUE4: u8 = 0x17;
        const LED_CUE5: u8 = 0x19;
        const LED_CUE6: u8 = 0x1B;
        const LED_CUE7: u8 = 0x1D;
        const LED_CUE8: u8 = 0x20;

        let loop_length_beat = active_deck.state.loop_length_beat().unwrap_or(0.0) as i32;
        send_midi_all_ch!(0xB, led_midi_note(loop_length_beat == LOOP_SIZES[0] as i32), LED_CUE1);
        send_midi_all_ch!(0xB, led_midi_note(loop_length_beat == LOOP_SIZES[1] as i32), LED_CUE2);
        send_midi_all_ch!(0xB, led_midi_note(loop_length_beat == LOOP_SIZES[2] as i32), LED_CUE3);
        send_midi_all_ch!(0xB, led_midi_note(loop_length_beat == LOOP_SIZES[3] as i32), LED_CUE4);
        send_midi_all_ch!(0xB, led_midi_note(false), LED_CUE5);
        send_midi_all_ch!(0xB, led_midi_note(false), LED_CUE6);
        send_midi_all_ch!(0xB, led_midi_note(false), LED_CUE7);
        send_midi_all_ch!(0xB, led_midi_note(false), LED_CUE8);
        
        const LED_AUTO_LOOP: u8 = 0x2B;
        send_midi_all_ch!(0xB, led_midi_note(active_deck.in_loop() || active_deck.before_loop()), LED_AUTO_LOOP);
    }
}
