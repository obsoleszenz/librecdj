mod action;
mod key_code;
mod key_combination;
pub mod key_event;

use crossterm::event::KeyCode;
pub use key_code::*;
use thiserror::Error;

pub use crate::keyboardbindings::key_combination::LCDJKeyCombination;

pub use crate::keyboardbindings::LCDJKeyEventKind;
use crate::{
    audio_engine::AudioEngineCommand,
    backend::BackendCommand,
    command::Command,
    keyboardbindings::{action::Action, key_combination::MultipleKeyCombinations},
    libplayer::PlayerCommand,
    library::LibraryCommand,
};
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, sync::LazyLock};

const DEFAULT_KEYBOARD_BINDINGS: LazyLock<KeyboardBindings> = LazyLock::new(|| {
    KeyboardBindings::load_from_str(include_str!(
        "../../../../resources/keyboard_bindings/en_US.ron"
    ))
    .unwrap()
});

#[derive(Debug, Deserialize, Serialize, Clone)]
enum ActionOrVecExpr {
    Action(Action),
    Expr(Vec<Expr>),
}

impl ActionOrVecExpr {
    fn into_vec_expr(self) -> Vec<Expr> {
        match self {
            ActionOrVecExpr::Expr(e) => e,
            ActionOrVecExpr::Action(a) => a.into_expr(),
        }
    }
}

pub const fn default_bool<const V: bool>() -> bool {
    V
}

/// Parsing the KeyboardMapping needs a pre stage on which
/// we then optimize. This is done to have convenience on
/// human writing config side and performant lookups on
/// the evaluation side.
#[derive(Debug, Deserialize, Serialize)]
pub struct PreKeyboardBindings {
    #[serde(default = "default_bool::<true>")]
    pub inherit: bool,
    key_bindings: HashMap<MultipleKeyCombinations, ActionOrVecExpr>,
}

impl PreKeyboardBindings {
    pub fn load_from_str(input: &str) -> Result<KeyboardBindings, ron::de::SpannedError> {
        let kbdm: Self = ron::from_str(input)?;
        let inherit = kbdm.inherit;

        // Flatten MultipleKeyCombination into Single KeyCombination to
        // ease the lookup. Also turn any Action into Vec<Expr>.
        let key_bindings: HashMap<LCDJKeyCombination, Vec<Expr>> = kbdm
            .key_bindings
            .into_iter()
            .flat_map(|(kcs, aove)| {
                let expressions = aove.into_vec_expr();
                kcs.into_inner()
                    .into_iter()
                    .map(|kc| (kc, expressions.clone()))
                    .collect::<Vec<(LCDJKeyCombination, Vec<Expr>)>>()
            })
            .collect();

        let key_bindings = KeyboardBindings {
            inherit,
            bindings: key_bindings,
        };

        let key_bindings = if key_bindings.inherit {
            let default_keyboard_bindigs = DEFAULT_KEYBOARD_BINDINGS.clone();
            default_keyboard_bindigs.extend(key_bindings)
        } else {
            key_bindings
        };

        Ok(key_bindings)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct KeyboardBindings {
    #[serde(default = "default_bool::<true>")]
    pub inherit: bool,
    bindings: HashMap<LCDJKeyCombination, Vec<Expr>>,
}

impl Default for KeyboardBindings {
    fn default() -> Self {
        DEFAULT_KEYBOARD_BINDINGS.clone()
    }
}

impl KeyboardBindings {
    pub fn on_key_combination(
        &self,
        key_combination: &LCDJKeyCombination,
        event_kind: &LCDJKeyEventKind,
    ) -> Option<Vec<KeyboardMappingCommand>> {
        self.bindings.get(key_combination).map(|expressions| {
            expressions
                .iter()
                .flat_map(|e| e.eval(*event_kind))
                .collect::<Vec<KeyboardMappingCommand>>()
        })
    }

    pub fn extend(mut self, other: KeyboardBindings) -> Self {
        self.inherit = other.inherit;
        self.bindings.extend(other.bindings);

        self
    }

    #[allow(clippy::should_implement_trait)]
    pub fn load_from_str(input: &str) -> Result<Self, KeyboardBindingsError> {
        Ok(PreKeyboardBindings::load_from_str(input)?)
    }
}

#[derive(Error, Debug)]
pub enum KeyboardBindingsError {
    #[error("Parsing Error")]
    Ron(#[from] ron::de::SpannedError),
}

impl KeyboardBindingsError {
    pub fn to_pretty_error(self, src: &str) -> String {
        match self {
            KeyboardBindingsError::Ron(ron_error) => {
                let line = ron_error.position.line;
                let start = ron_error.position.col - 2;
                let end = ron_error.position.col;
                let message = ron_error.code.to_string();
                let skip_lines = line.saturating_sub(1);

                let src = src
                    .to_string()
                    .lines()
                    .skip(skip_lines)
                    .take(3)
                    .collect::<Vec<&str>>()
                    .join("\n");
                chic::Error::new(format!("{} line:{} col:{}", &message, &line, &start))
                    .error(line, start, start, src, format!("^ {}", message))
                    .to_string()
            }
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum Expr {
    Command(KeyboardMappingCommand),
    IfPress(Box<Expr>),
    IfRepeat(Box<Expr>),
    IfPressOrRepeat(Box<Expr>),
    IfRelease(Box<Expr>),
    Combine(Vec<Expr>),
    Action(Action),
}

impl Expr {
    pub fn eval(&self, event_kind: LCDJKeyEventKind) -> Vec<KeyboardMappingCommand> {
        match self {
            Expr::Command(cmd) => vec![cmd.clone()],
            Expr::IfPress(expr) if event_kind == LCDJKeyEventKind::Press => expr.eval(event_kind),
            Expr::IfRepeat(expr) if event_kind == LCDJKeyEventKind::Repeat => expr.eval(event_kind),
            Expr::IfPressOrRepeat(expr)
                if event_kind == LCDJKeyEventKind::Press
                    || event_kind == LCDJKeyEventKind::Repeat =>
            {
                expr.eval(event_kind)
            }
            Expr::IfRelease(expr) if event_kind == LCDJKeyEventKind::Release => {
                expr.eval(event_kind)
            }
            _ => vec![],
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
pub enum KeyboardMappingCommand {
    Deck(usize, PlayerCommand),
    AllDecks(PlayerCommand),
    SelectedDeck(PlayerCommand),
    Library(LibraryCommand),
    CloneDeck(Deck, Deck),
    Backend(BackendCommand),
}

#[derive(Debug, Deserialize, Serialize, Copy, Clone, PartialEq)]
pub(crate) enum Deck {
    One,
    Two,
    Three,
    Four,
    All,
    Selected,
}

impl Deck {
    pub fn as_usize(&self) -> usize {
        match self {
            Deck::One => 0,
            Deck::Two => 1,
            Deck::Three => 2,
            Deck::Four => 3,
            _ => panic!("Not supported"),
        }
    }
    pub fn to_index(&self, selected: usize) -> usize {
        match self {
            Deck::One | Deck::Two | Deck::Three | Deck::Four => self.as_usize(),
            Deck::All => panic!("Not supported"),
            Deck::Selected => selected,
        }
    }
}

impl KeyboardMappingCommand {
    pub fn into_command(self, selected_deck: usize) -> Command {
        let time = 0;
        match self {
            KeyboardMappingCommand::Deck(index, command) => AudioEngineCommand::Deck {
                index,
                time,
                command,
            }
            .into(),
            KeyboardMappingCommand::AllDecks(command) => {
                AudioEngineCommand::AllDecks { time, command }.into()
            }
            KeyboardMappingCommand::Library(library_command) => {
                AudioEngineCommand::Library(library_command).into()
            }
            KeyboardMappingCommand::Backend(backend_command) => {
                AudioEngineCommand::Backend(backend_command).into()
            }
            KeyboardMappingCommand::SelectedDeck(command) => AudioEngineCommand::Deck {
                index: selected_deck,
                time,
                command,
            }
            .into(),
            KeyboardMappingCommand::CloneDeck(from, to) => {
                BackendCommand::CloneDeck(from.to_index(selected_deck), to.to_index(selected_deck))
                    .into()
            }
        }
    }
}

pub(crate) trait WithDeck {
    fn with_deck(self, deck: Deck) -> KeyboardMappingCommand;
}

impl WithDeck for PlayerCommand {
    fn with_deck(self, deck: Deck) -> KeyboardMappingCommand {
        match deck {
            Deck::One => KeyboardMappingCommand::Deck(0, self),
            Deck::Two => KeyboardMappingCommand::Deck(1, self),
            Deck::Three => KeyboardMappingCommand::Deck(2, self),
            Deck::Four => KeyboardMappingCommand::Deck(3, self),
            Deck::All => KeyboardMappingCommand::AllDecks(self),
            Deck::Selected => KeyboardMappingCommand::SelectedDeck(self),
        }
    }
}

impl From<KeyboardMappingCommand> for Expr {
    fn from(value: KeyboardMappingCommand) -> Self {
        Expr::Command(value)
    }
}
impl From<KeyboardMappingCommand> for Box<Expr> {
    fn from(value: KeyboardMappingCommand) -> Self {
        std::convert::Into::<Expr>::into(value).into()
    }
}

impl From<BackendCommand> for KeyboardMappingCommand {
    fn from(value: BackendCommand) -> Self {
        KeyboardMappingCommand::Backend(value)
    }
}

#[cfg(test)]
mod tests {
    use libplayer::CueKind;

    use crate::keyboardbindings::{action::DeckAction, key_code::LCDJKeyCode};

    use super::*;

    #[test]
    pub fn check_basic_keyboard_mapping() {
        let kbd = KeyboardBindings::load_from_str(
            r#"
                (
                    key_bindings: {
                       // Map `q` and `q` to this action
                       "q o": Expr([
                           IfPress(Command(SelectedDeck(Cue(Press, None)))),
                           IfRelease(Command(SelectedDeck(Cue(Release, None))))
                        ]),
                        "z S-z": Action(Selected(Cue)),
                        "F1": Action(Deck(One,   Select)),

                    }
                )
            "#,
        )
        .expect("Failed parsing keyboard mapping");

        println!("{:#?}", kbd.bindings);

        let test_case = |key_combination: LCDJKeyCombination,
                         event_kind: LCDJKeyEventKind,
                         expect: Option<Vec<KeyboardMappingCommand>>| {
            assert_eq!(
                kbd.on_key_combination(&key_combination, &event_kind),
                expect
            );
        };

        test_case(
            LCDJKeyCombination::Single(LCDJKeyCode::Char('q')),
            LCDJKeyEventKind::Press,
            Some(vec![KeyboardMappingCommand::SelectedDeck(
                PlayerCommand::Cue(CueKind::Press, None),
            )]),
        );
        test_case(
            LCDJKeyCombination::Single(LCDJKeyCode::Char('q')),
            LCDJKeyEventKind::Release,
            Some(vec![KeyboardMappingCommand::SelectedDeck(
                PlayerCommand::Cue(CueKind::Release, None),
            )]),
        );
        test_case(
            LCDJKeyCombination::Single(LCDJKeyCode::Char('o')),
            LCDJKeyEventKind::Release,
            Some(vec![KeyboardMappingCommand::SelectedDeck(
                PlayerCommand::Cue(CueKind::Release, None),
            )]),
        );
        test_case(
            LCDJKeyCombination::Single(LCDJKeyCode::Char('x')),
            LCDJKeyEventKind::Press,
            None,
        );
        test_case(
            LCDJKeyCombination::Single(LCDJKeyCode::Char('z')),
            LCDJKeyEventKind::Press,
            Some(
                DeckAction::Cue
                    .into_expr(Deck::Selected)
                    .into_iter()
                    .flat_map(|e| e.eval(LCDJKeyEventKind::Press))
                    .collect(),
            ),
        );
        test_case(
            LCDJKeyCombination::Single(LCDJKeyCode::F(1)),
            LCDJKeyEventKind::Press,
            Some(
                DeckAction::Select
                    .into_expr(Deck::One)
                    .into_iter()
                    .flat_map(|e| e.eval(LCDJKeyEventKind::Press))
                    .collect(),
            ),
        );
    }
}
