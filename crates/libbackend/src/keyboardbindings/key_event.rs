use super::{LCDJKeyCombination, LCDJKeyEventKind};

#[derive(Clone, PartialEq, Debug)]
pub struct LCDJKeyEvent {
    combination: LCDJKeyCombination,
    kind: LCDJKeyEventKind,
}

impl LCDJKeyEvent {
    pub fn new(combination: LCDJKeyCombination, kind: LCDJKeyEventKind) -> LCDJKeyEvent {
        Self { combination, kind }
    }
    pub fn combination(&self) -> &LCDJKeyCombination {
        &self.combination
    }
    pub fn kind(&self) -> &LCDJKeyEventKind {
        &self.kind
    }

    pub fn is_press(&self) -> bool {
        self.kind() == &LCDJKeyEventKind::Press
    }

    pub fn is_press_or_repeat(&self) -> bool {
        self.kind() == &LCDJKeyEventKind::Press || self.kind() == &LCDJKeyEventKind::Repeat
    }
}
