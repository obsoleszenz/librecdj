/// Copyright belongs to crossterm.
/// Shamelessly copied
use serde::{Deserialize, Serialize};

use crate::keyboardbindings::key_combination::KeyFromStrError;

use super::LCDJKeyCombination;

/// Represents a key.
#[derive(Debug, PartialOrd, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub enum LCDJKeyCode {
    /// Backspace key.
    Backspace,
    /// Enter key.
    Enter,
    /// Left arrow key.
    Left,
    /// Right arrow key.
    Right,
    /// Up arrow key.
    Up,
    /// Down arrow key.
    Down,
    /// Home key.
    Home,
    /// End key.
    End,
    /// Page up key.
    PageUp,
    /// Page down key.
    PageDown,
    /// Tab key.
    Tab,
    /// Shift + Tab key.
    BackTab,
    /// Delete key.
    Delete,
    /// Insert key.
    Insert,
    /// F key.
    ///
    /// `KeyCode::F(1)` represents F1 key, etc.
    F(u8),
    /// A character.
    ///
    /// `KeyCode::Char('c')` represents `c` character, etc.
    Char(char),
    NumPad(u8),
    /// Null.
    Null,
    /// Escape key.
    Esc,
    /// Caps Lock key.
    CapsLock,
    /// Scroll Lock key.
    ScrollLock,
    /// Num Lock key.
    NumLock,
    /// Print Screen key.
    PrintScreen,
    /// Pause key.
    Pause,
    /// Menu key.
    Menu,
    /// The "Begin" key (often mapped to the 5 key when Num Lock is turned on).
    KeypadBegin,
    Copy,
    Cut,
    Paste,
}

impl TryFrom<String> for LCDJKeyCode {
    type Error = KeyFromStrError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Ok(if value.len() == 1 {
            LCDJKeyCode::Char(value.chars().nth(0).unwrap())
        } else if value.starts_with('F') && value.len() == 2 {
            let num = value.chars().nth(1).ok_or(KeyFromStrError)?;
            let num = num
                .to_string()
                .as_str()
                .parse::<u8>()
                .map_err(|_| KeyFromStrError)?;
            LCDJKeyCode::F(num)
        } else if value == "Up" {
            LCDJKeyCode::Up
        } else if value == "Down" {
            LCDJKeyCode::Down
        } else if value == "Left" {
            LCDJKeyCode::Left
        } else if value == "Right" {
            LCDJKeyCode::Right
        } else if value == "Esc" {
            LCDJKeyCode::Esc
        } else if value == "Enter" {
            LCDJKeyCode::Enter
        } else if value == "PageUp" {
            LCDJKeyCode::PageUp
        } else if value == "PageDown" {
            LCDJKeyCode::PageDown
        } else if value == "PrintScreen" {
            LCDJKeyCode::PrintScreen
        } else if value == "Insert" {
            LCDJKeyCode::Insert
        } else if value == "Backspace" {
            LCDJKeyCode::Backspace
        } else if value == "Tab" {
            LCDJKeyCode::Tab
        } else if value == "Home" {
            LCDJKeyCode::Home
        } else if value == "End" {
            LCDJKeyCode::End
        } else if value == "Delete" {
            LCDJKeyCode::Delete
        } else if value == "Spacebar" {
            LCDJKeyCode::Char(' ')
        } else {
            return Err(KeyFromStrError);
        })
    }
}

impl From<LCDJKeyCode> for LCDJKeyCombination {
    fn from(value: LCDJKeyCode) -> Self {
        LCDJKeyCombination::Single(value)
    }
}

/// Represents a keyboard event kind.
#[derive(Debug, PartialOrd, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
pub enum LCDJKeyEventKind {
    Press,
    Repeat,
    Release,
}
