use serde::{Deserialize, Serialize};

use crate::keyboardbindings::key_code::LCDJKeyCode;

#[derive(Debug, Deserialize, Serialize, Hash, PartialEq, Eq, Clone)]
#[serde(try_from = "String")]
pub struct MultipleKeyCombinations(Vec<LCDJKeyCombination>);

impl MultipleKeyCombinations {
    pub fn into_inner(self) -> Vec<LCDJKeyCombination> {
        self.0
    }
}

impl TryFrom<String> for MultipleKeyCombinations {
    type Error = KeyFromStrError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        let key_combinations = value
            .split(" ")
            .map(LCDJKeyCombination::try_from)
            .collect::<Result<Vec<LCDJKeyCombination>, KeyFromStrError>>()?;

        if key_combinations.is_empty() {
            return Err(KeyFromStrError);
        }
        Ok(Self(key_combinations))
    }
}

#[derive(Debug, Deserialize, Serialize, Hash, PartialEq, Eq, Clone)]
#[serde(try_from = "String")]
pub enum LCDJKeyCombination {
    Single(LCDJKeyCode),
    Alt(LCDJKeyCode),
    AltShift(LCDJKeyCode),
    Ctrl(LCDJKeyCode),
    Shift(LCDJKeyCode),
    CtrlShift(LCDJKeyCode),
    CtrlAlt(LCDJKeyCode),
    CtrlAltShift(LCDJKeyCode),
}

impl std::fmt::Display for LCDJKeyCombination {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        todo!()
    }
}

#[derive(Debug, PartialEq, Eq)]
pub struct KeyFromStrError;

impl TryFrom<&str> for LCDJKeyCombination {
    type Error = KeyFromStrError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Ok(if value.starts_with("C-") {
            LCDJKeyCombination::Ctrl(LCDJKeyCode::try_from(
                value.chars().skip(2).collect::<String>(),
            )?)
        } else if value.starts_with("S-") {
            LCDJKeyCombination::Shift(LCDJKeyCode::try_from(
                value.chars().skip(2).collect::<String>(),
            )?)
        } else if value.starts_with("A-") {
            LCDJKeyCombination::Alt(LCDJKeyCode::try_from(
                value.chars().skip(2).collect::<String>(),
            )?)
        } else if value.starts_with("AS-") {
            LCDJKeyCombination::AltShift(LCDJKeyCode::try_from(
                value.chars().skip(3).collect::<String>(),
            )?)
        } else if value.starts_with("CS-") {
            LCDJKeyCombination::CtrlShift(LCDJKeyCode::try_from(
                value.chars().skip(3).collect::<String>(),
            )?)
        } else if value.starts_with("CA-") {
            LCDJKeyCombination::CtrlAlt(LCDJKeyCode::try_from(
                value.chars().skip(3).collect::<String>(),
            )?)
        } else if value.starts_with("CAS-") {
            LCDJKeyCombination::CtrlAltShift(LCDJKeyCode::try_from(
                value.chars().skip(4).collect::<String>(),
            )?)
        } else {
            LCDJKeyCombination::Single(LCDJKeyCode::try_from(value.to_string())?)
        })
    }
}
impl TryFrom<String> for LCDJKeyCombination {
    type Error = KeyFromStrError;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        Self::try_from(value.as_str())
    }
}

impl std::fmt::Display for KeyFromStrError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str("KeyFromStrError")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn check_key_combination_from_string() {
        macro_rules! test_case {
            ($s: tt, $expect: expr) => {
                assert_eq!(LCDJKeyCombination::try_from($s.to_string()), Ok($expect));
            };
        }

        test_case!("C-S", LCDJKeyCombination::Ctrl(LCDJKeyCode::Char('S')));
        test_case!("A-S", LCDJKeyCombination::Alt(LCDJKeyCode::Char('S')));
        test_case!("CA-S", LCDJKeyCombination::CtrlAlt(LCDJKeyCode::Char('S')));
        test_case!("AS-S", LCDJKeyCombination::AltShift(LCDJKeyCode::Char('S')));
        test_case!(
            "CS-S",
            LCDJKeyCombination::CtrlShift(LCDJKeyCode::Char('S'))
        );
        test_case!(
            "CAS-S",
            LCDJKeyCombination::CtrlAltShift(LCDJKeyCode::Char('S'))
        );
        test_case!("CAS-Up", LCDJKeyCombination::CtrlAltShift(LCDJKeyCode::Up));
    }

    #[test]
    fn check_multiple_key_combination_from_string() {
        macro_rules! test_case {
            ($s: tt, $expect: expr) => {
                assert_eq!(
                    MultipleKeyCombinations::try_from($s.to_string()),
                    Ok($expect)
                );
            };
        }
        test_case!(
            "q o",
            MultipleKeyCombinations(vec![
                LCDJKeyCombination::Single(LCDJKeyCode::Char('q')),
                LCDJKeyCombination::Single(LCDJKeyCode::Char('o')),
            ])
        );
    }
}
