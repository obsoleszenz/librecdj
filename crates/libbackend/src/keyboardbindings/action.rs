use serde::{Deserialize, Serialize};

use crate::{
    backend::BackendCommand,
    keyboardbindings::{
        Deck,
        Expr::{self, Command, IfPress, IfPressOrRepeat, IfRelease},
        KeyboardMappingCommand, WithDeck,
    },
    libplayer::{
        Conditional::*, CueKind, Direction, LoopKind::*, LoopSize, NudgeKind, PlayerCommand::*,
        Position::*, TempoKind, Unit::*,
    },
    library::LibraryCommand,
};

/// Actions are a little bit like a Shortcut or a macro.
/// They can expand into [`Vec<Expr>`].
/// The reason we have them is to have easy to use and understand
/// blocks for the config language, hiding the expressiveness
/// of the Command System for the generic use case.
#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum Action {
    Selected(DeckAction),
    All(DeckAction),
    Deck(Deck, DeckAction),
    Library(LibraryCommand),
    ToggleWaveformZoom,
}

impl Action {
    pub fn into_expr(self) -> Vec<Expr> {
        match self {
            Action::ToggleWaveformZoom => vec![IfPress(
                Command(KeyboardMappingCommand::Backend(
                    BackendCommand::WaveformMode(None),
                ))
                .into(),
            )],
            Action::Selected(a) => a.into_expr(Deck::Selected),
            Action::All(a) => a.into_expr(Deck::All),
            Action::Deck(deck, a) => a.into_expr(deck),
            Action::Library(library_command) => vec![IfPressOrRepeat(Box::new(Expr::Command(
                KeyboardMappingCommand::Library(library_command),
            )))],
        }
    }
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum NudgeSize {
    Small,
    Big,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum RelativeOrAbsolute {
    Relative,
    Absolute,
}

#[derive(Debug, Deserialize, Serialize, Clone)]
pub enum DeckAction {
    Cue,
    CueReset,
    Play,
    Nudge(Direction, NudgeSize),
    Tempo(TempoKind),
    BeatLoop(f32),
    ToggleLoop,
    BeatJump(f32),
    ToggleWaveformZoom,
    Select,
    CloneTo(Deck),
}

impl DeckAction {
    pub fn into_expr(self, deck: Deck) -> Vec<Expr> {
        match self {
            DeckAction::Cue => vec![
                IfPress(Cue(CueKind::Press, None).with_deck(deck).into()),
                IfRelease(Cue(CueKind::Release, None).with_deck(deck).into()),
            ],
            DeckAction::CueReset => vec![IfPress(Cue(CueKind::Reset, None).with_deck(deck).into())],
            DeckAction::Play => vec![IfPress(Play(None, None).with_deck(deck).into())],
            DeckAction::Nudge(direction, size) => vec![
                IfPress(
                    Nudge(NudgeKind::Enable {
                        direction,
                        tempo_playing: match size {
                            NudgeSize::Small => 0.1,
                            NudgeSize::Big => 0.2,
                        },
                        tempo_paused: match size {
                            NudgeSize::Small => 0.2,
                            NudgeSize::Big => 0.4,
                        },
                    })
                    .with_deck(deck)
                    .into(),
                ),
                IfRelease(Nudge(NudgeKind::Disable).with_deck(deck).into()),
            ],
            DeckAction::Tempo(tempo_kind) => {
                vec![IfPressOrRepeat(Tempo(tempo_kind).with_deck(deck).into())]
            }

            DeckAction::BeatLoop(size) => vec![IfPress(
                Loop(Set(Enable, LoopSize::Relative(Beat(size))))
                    .with_deck(deck)
                    .into(),
            )],
            DeckAction::ToggleLoop => vec![IfPress(Loop(Enabled(Toggle)).with_deck(deck).into())],
            DeckAction::BeatJump(size) => {
                vec![IfPress(Seek(Relative(Beat(size))).with_deck(deck).into())]
            }
            DeckAction::ToggleWaveformZoom => vec![IfPress(
                Command(KeyboardMappingCommand::Backend(
                    BackendCommand::WaveformMode(None),
                ))
                .into(),
            )],
            DeckAction::Select => vec![IfPress(
                Command(KeyboardMappingCommand::Backend(BackendCommand::SelectDeck(
                    deck as usize,
                )))
                .into(),
            )],
            DeckAction::CloneTo(to) => vec![IfPress(
                Command(KeyboardMappingCommand::CloneDeck(deck, to)).into(),
            )],
        }
    }
}
