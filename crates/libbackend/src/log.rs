#[allow(unused)]
macro_rules! trace { ($($x:tt)*) => (
    {
        #[cfg(feature = "log")] {
            tracing::trace!($($x)*)
        }
    }
) }
#[allow(unused)]
macro_rules! debug { ($($x:tt)*) => (
    #[cfg(feature = "log")] {
        tracing::debug!($($x)*)
    }
) }
#[allow(unused)]
macro_rules! info { ($($x:tt)*) => (
    {
        #[cfg(feature = "log")] {
            tracing::info!($($x)*)
        }
    }
) }
#[allow(unused)]
macro_rules! warn { ($($x:tt)*) => (
    {
        // #[cfg(feature = "log")] {
            tracing::warn!($($x)*)
        // }
    }
) }
#[allow(unused)]
macro_rules! error { ($($x:tt)*) => (
    {
        // #[cfg(feature = "log")] {
            tracing::error!($($x)*)
        // }
    }
) }

#[allow(unused)]
macro_rules! assert_delta {
    ($x:expr, $y:expr, $d:expr) => {
        if !($x - $y < $d || $y - $x < $d) {
            panic!();
        }
    };
}

#[allow(unused)]
macro_rules! log_error {
    ($expr:expr, $($x:tt)*) => {
        match $expr {
            Ok(t) => Ok(t),
            Err(err) => {
                error!($($x)*, err);
                Err(err)

            }
        }
    };
}

#[allow(unused)]
macro_rules! log_error_ok {
    ($($x:tt)*) => {
        log_error!($($x)*).ok()
    };
}
