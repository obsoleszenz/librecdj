use std::sync::atomic::{AtomicU16, Ordering};
use std::sync::{Arc, LazyLock};

use eframe::egui::{self, Color32, InnerResponse, Label, Rounding, TextWrapMode, Ui, Vec2};

use crate::egui_extension::LibreCDJUI;

const LIBRARY_PADDING_LEFT: f32 = 10.0;
const LIBRARY_COLUMN_SPACING: f32 = 28.0;
const LIBRARY_MIN_ITEM_HEIGHT: f32 = 20.0;

const COLOR_LIBRARY_BACKGROUND: Color32 = Color32::WHITE;
const COLOR_DECK_A: Color32 = Color32::LIGHT_BLUE;
const COLOR_DECK_B: Color32 = Color32::LIGHT_GREEN;
const COLOR_DECK_C: Color32 = Color32::LIGHT_YELLOW;
const COLOR_DECK_D: Color32 = Color32::LIGHT_RED;

pub fn selected_deck_to_color(deck: usize) -> Color32 {
    match deck {
        0 => COLOR_DECK_A,
        1 => COLOR_DECK_B,
        2 => COLOR_DECK_C,
        3 => COLOR_DECK_D,
        _ => COLOR_DECK_A,
    }
}

pub static SIGNAL_LIBRARY_CURRENT_AMOUNT_ITEMS: LazyLock<Arc<AtomicU16>> =
    LazyLock::new(|| Arc::new(AtomicU16::new(0)));

pub struct LibraryWidget<'a> {
    pub library_state: &'a libbackend::library::LibraryState,
    pub selected_deck: usize,
}

use libbackend::backend::Backend;
impl<'a> LibraryWidget<'a> {
    pub fn new(backend: &'a Backend) -> Self {
        Self {
            library_state: &backend.state.library,
            selected_deck: backend.selected_deck(),
        }
    }
}

impl egui::Widget for LibraryWidget<'_> {
    fn ui(mut self, ui: &mut Ui) -> egui::Response {
        let available_size = ui.available_size();
        ui.style_no_spacing();

        ui.add_full_space(ui.available_size(), |ui| {
            let height = available_size.y;
            let width = available_size.x;

            ui.painter().rect_filled(
                ui.available_rect(),
                Rounding::default(),
                COLOR_LIBRARY_BACKGROUND,
            );

            // Draw library path
            ui.add_horizontal_full_space(Vec2::new(width, 40.0), |ui| {
                ui.label(self.library_state.path.to_string_lossy().to_string())
            });
            let height = height - 40.0;
            let count_fitting_items = (height / LIBRARY_MIN_ITEM_HEIGHT).floor() as usize;
            SIGNAL_LIBRARY_CURRENT_AMOUNT_ITEMS.store(
                TryInto::<u16>::try_into(count_fitting_items).expect("U16 to small") + 1,
                Ordering::Relaxed,
            );

            let virtual_list_range = {
                let half_fitting_items = count_fitting_items / 2;
                let selected = self.library_state.selected as i64;
                let start = selected - half_fitting_items as i64;
                let end = selected - 1 + half_fitting_items as i64;
                (start, end)
            };
            let real_item_height = height / (count_fitting_items as f32);

            let first_item_display = self
                .library_state
                .selected
                .saturating_sub(count_fitting_items.saturating_sub(1) / 2);

            let _last_item_display = first_item_display + (count_fitting_items.saturating_sub(0));
            for library_item_index in virtual_list_range.0..=virtual_list_range.1 {
                ui.add_horizontal_full_space(Vec2::new(width, real_item_height), |ui| {
                    self.item(ui, library_item_index);
                });
            }
        })
        .response
    }
}

impl LibraryWidget<'_> {
    pub fn item(&mut self, ui: &mut Ui, library_item_index: i64) {
        let available_rect = ui.available_rect();
        fn add_col<R>(
            ui: &mut Ui,
            width_percent: f32,
            height: f32,
            add_contents: impl FnOnce(&mut Ui) -> R,
        ) -> InnerResponse<R> {
            let width = ui.available_width() * width_percent;
            ui.add_full_space(Vec2::new(width, height), |ui| {
                ui.set_min_width(width);
                ui.style_mut().wrap_mode = Some(TextWrapMode::Truncate);
                add_contents(ui)
            })
        }

        if library_item_index > self.library_state.items.len() as i64 - 1
            || library_item_index < 0
            || self.library_state.items.is_empty()
        {
            ui.painter().rect_filled(
                available_rect,
                Rounding::default(),
                COLOR_LIBRARY_BACKGROUND,
            );
            ui.add_space(LIBRARY_PADDING_LEFT);
            add_col(ui, 30.0, available_rect.height(), |ui| ui.label("..."));
            return;
        }
        let library_item_index: usize = library_item_index as usize;
        if let Some(item) = self.library_state.items.get(library_item_index) {
            let is_selected = library_item_index == self.library_state.selected;

            ui.painter().rect_filled(
                available_rect,
                Rounding::default(),
                if is_selected {
                    selected_deck_to_color(self.selected_deck)
                } else {
                    COLOR_LIBRARY_BACKGROUND
                },
            );

            let mut render_library_item =
                |bpm: Option<f32>, key: &str, artist: &str, track_title: &str, comment: &str| {
                    ui.add_space(LIBRARY_PADDING_LEFT);
                    add_col(ui, 0.05, available_rect.height(), |ui| {
                        ui.label(format!("{:.2}", bpm.unwrap_or(0.0)))
                    });
                    ui.add_space(LIBRARY_COLUMN_SPACING);
                    add_col(ui, 0.025, available_rect.height(), |ui| ui.label(key));
                    ui.add_space(LIBRARY_COLUMN_SPACING);
                    add_col(ui, 0.2, available_rect.height(), |ui| ui.label(artist));
                    ui.add_space(LIBRARY_COLUMN_SPACING);
                    add_col(ui, 0.2, available_rect.height(), |ui| ui.label(track_title));
                    ui.add_space(LIBRARY_COLUMN_SPACING);
                    add_col(ui, 0.5, available_rect.height(), |ui| ui.label(comment));
                };
            match item {
                libbackend::library::LibraryItem::Dir(dir_name) => {
                    ui.add_space(LIBRARY_PADDING_LEFT);
                    add_col(ui, ui.available_width(), available_rect.height(), |ui| {
                        ui.label(dir_name)
                    });
                }
                libbackend::library::LibraryItem::File {
                    name,
                    path: _,
                    metadata,
                } => {
                    let bpm = metadata.as_ref().and_then(|m| m.bpm);
                    let key = metadata
                        .as_ref()
                        .map(|m| m.key.clone())
                        .unwrap_or("".into());
                    let artist = metadata
                        .as_ref()
                        .map(|m| m.artist.clone().to_string())
                        .unwrap_or(name.to_string());
                    let title = metadata
                        .as_ref()
                        .map(|m| m.title.clone().to_string())
                        .unwrap_or("".to_string());
                    let comment = metadata
                        .as_ref()
                        .map(|m| m.comment.clone().to_string())
                        .unwrap_or("".to_string());
                    render_library_item(bpm, &key, &artist, &title, &comment);
                }

                libbackend::library::LibraryItem::Section { name } => {
                    ui.add_space(LIBRARY_PADDING_LEFT);
                    ui.add_sized(
                        [40.0, available_rect.height()],
                        Label::new(format!("-- {} --", name)),
                    );
                }
            }
        } else {
            ui.painter().rect_filled(
                available_rect,
                Rounding::default(),
                COLOR_LIBRARY_BACKGROUND,
            );
        }
    }
}
