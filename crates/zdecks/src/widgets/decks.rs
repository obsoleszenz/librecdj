use eframe::{
    egui::{
        self, text::LayoutJob, Align, Color32, FontSelection, Layout, Pos2, Rect, RichText,
        Rounding, Sense, Shape, Stroke, Style, UiBuilder, Vec2,
    },
    epaint,
};
use libbackend::{
    libplayer::BeatPosition,
    state::{Deck, WaveformMode},
};
use once_cell::sync::Lazy;
use tracing::trace;

use crate::egui_extension::LibreCDJUI;

use super::library::selected_deck_to_color;

pub struct DeckWidget<'a> {
    decks: &'a [Deck; 4],
    positions: &'a [BeatPosition; 5],
    waveform_mode: &'a WaveformMode,
}

const DECK_PADDING_LEFT: f32 = 28.0;
const DECK_HEIGHT: f32 = 100.0;
const PLAYHEAD_MARGIN_TOP_BOTTOM: f32 = 10.0;

static WAVEFORM_TRESHOLDS: Lazy<Vec<f32>> = Lazy::new(|| {
    [0.0; 128]
        .iter()
        .enumerate()
        .map(|(i, _)| 0.0 - (0.0015 * (i as f32).powi(2)))
        .collect()
});

impl<'a> DeckWidget<'a> {
    pub fn new(
        decks: &'a [Deck; 4],
        positions: &'a [BeatPosition; 5],
        waveform_mode: &'a WaveformMode,
    ) -> Self {
        Self {
            decks,
            positions,
            waveform_mode,
        }
    }

    pub fn add_single_deck(&mut self, ui: &mut egui::Ui, deck_index: usize) {
        let deck = &self.decks[deck_index];
        let deck_position = &self.positions[deck_index];
        let metadata = &deck.metadata;

        let INFO_WIDTH = ui.available_width() * 0.2;

        ui.with_layout(egui::Layout::left_to_right(egui::Align::TOP), |ui| {
            // ui.label(format!("{:?}", self.decks[deck_index].metadata))
            ui.with_layout(Layout::top_down(egui::Align::Min), |ui| {
                let (_id, rect) = ui.allocate_space(Vec2::new(INFO_WIDTH, ui.available_height()));
                ui.allocate_new_ui(UiBuilder::new().max_rect(rect), |mut ui| {
                    // Artist & Title
                    let title = if metadata.artist.is_empty() && metadata.title.is_empty() {
                        "DISCHARGED ".to_string()
                    } else {
                        format!("{} - {} ", metadata.artist, metadata.title)
                    };
                    ui.label(title);

                    // Elapsed / Duration and Loop size
                    draw_time_and_loop_size(&mut ui, deck);

                    // Beat Position And Beat Size
                    ui.label(format_beat_position_and_beat_size(
                        deck_position.to_owned(),
                        deck.player_state.beat_size,
                    ));

                    // Tempo
                    let tempo = deck.player_state.tempo + deck.player_state.jogwheel_tempo;
                    ui.label(format!(
                        "  {}{:.03}%     {:.02} BPM {}",
                        if tempo < 0.0 { '-' } else { '+' },
                        tempo,
                        deck.player_state.bpm().unwrap_or(0.0),
                        deck.metadata.key
                    ));

                    // Comment
                    ui.label(format!("  {}", metadata.comment.to_owned()));
                });
            });
            ui.add_full_space(
                Vec2::new(ui.available_width(), ui.available_height()),
                |ui| {
                    self.render_waveform(ui, deck_index);
                },
            );
        });
    }

    pub fn render_waveform(&mut self, ui: &mut egui::Ui, deck_index: usize) {
        let ppp = ui.ctx().pixels_per_point();
        let deck = &self.decks[deck_index];
        let mode = self.waveform_mode;
        let waveform_width_px = ui.available_width();
        let bar_width_px = 2.0;
        let count_bars = (waveform_width_px / bar_width_px) as usize;

        let playhead_samples = deck.player_state.playhead_samples;

        if let Some(waveform) = &deck.waveform {
            let waveform_len = waveform.bands.len();

            let bpm = deck.player_state.initial_bpm().unwrap_or(128.0);

            // Size of each waveform chunk
            let len_waveform_chunk_as_samples: i64 =
                (waveform.sample_rate as f32 / waveform.window_size as f32 / (bpm / 60.0) / 10.0)
                    as i64;

            let waveform_playhead_pos = (playhead_samples / waveform.window_size as i64)
                .div_euclid(len_waveform_chunk_as_samples)
                * len_waveform_chunk_as_samples;

            let waveform_sums: Vec<[f32; 3]> = match self.waveform_mode {
                WaveformMode::Overview => {
                    let len_waveform_chunk_as_samples: usize =
                        waveform_len / (waveform_width_px as usize);

                    let mut max_s = 0;
                    let result = vec![0.0; waveform_width_px as usize]
                        .iter()
                        .enumerate()
                        .map(|(i, _)| {
                            let [mut summed_lo, mut summed_mi, mut summed_hi] = [0.0; 3];

                            for c in 0..len_waveform_chunk_as_samples {
                                let chunk_pos = len_waveform_chunk_as_samples * i + c;
                                max_s = chunk_pos;
                                let chunk_bands = waveform.bands[chunk_pos];
                                for (i, sum) in [&mut summed_lo, &mut summed_mi, &mut summed_hi]
                                    .iter_mut()
                                    .enumerate()
                                {
                                    **sum += chunk_bands[i];
                                }
                            }

                            for sum in [&mut summed_lo, &mut summed_mi, &mut summed_hi] {
                                *sum /= len_waveform_chunk_as_samples as f32;
                            }

                            [summed_lo, summed_mi, summed_hi]
                        })
                        .collect();
                    println!(
                        "waveform_width_px={waveform_width_px} waveform_len={waveform_len} max_s={max_s} len_waveform_chunk_as_samples={len_waveform_chunk_as_samples}"
                    );
                    result
                }
                WaveformMode::Zoom => {
                    // The first position in waveform from which we want to draw
                    let waveform_start_pos: i64 = waveform_playhead_pos
                        - ((len_waveform_chunk_as_samples * count_bars as i64) / 2);
                    trace!("waveform_start_pos={waveform_start_pos} waveform_playhead_pos={waveform_playhead_pos}");

                    vec![0.0; waveform_width_px as usize]
                        .iter()
                        .enumerate()
                        .map(|(i, _)| {
                            let [mut summed_lo, mut summed_mi, mut summed_hi] = [0.0; 3];

                            let waveform_sum_pos: i64 = waveform_start_pos + len_waveform_chunk_as_samples * i as i64;

                            if i == 0 {
                                trace!("waveform_sum_pos={waveform_sum_pos} waveform_widht={waveform_width_px} sum_len={len_waveform_chunk_as_samples}");
                            }

                            if waveform_sum_pos >= 0
                                && waveform_sum_pos < waveform_len as i64 - len_waveform_chunk_as_samples
                            {
                                let waveform_sum_pos = waveform_sum_pos as usize;
                                for c in 0..len_waveform_chunk_as_samples {
                                    let chunk_bands = waveform.bands[waveform_sum_pos + c as usize];
                                    for (i, sum) in [&mut summed_lo, &mut summed_mi, &mut summed_hi]
                                        .iter_mut()
                                        .enumerate()
                                    {
                                        **sum += chunk_bands[i];
                                    }
                                }

                                for sum in [&mut summed_lo, &mut summed_mi, &mut summed_hi] {
                                    *sum /= len_waveform_chunk_as_samples as f32;
                                }
                            } else {
                                for sum in [&mut summed_lo, &mut summed_mi, &mut summed_hi] {
                                    *sum = -70.0;
                                }
                            }

                            [summed_lo, summed_mi, summed_hi]
                        })
                        .collect()
                }
            };

            let mut shapes: Vec<Shape> = vec![];

            for (x, bands) in waveform_sums.iter().enumerate() {
                for y in 0..3 {
                    let summed = bands[2 - y];
                    let b = WAVEFORM_TRESHOLDS
                        .iter()
                        .position(|t| summed >= *t)
                        .unwrap_or(WAVEFORM_TRESHOLDS.len() - 1);

                    let color = {
                        let brightness = 128 + b as u8;

                        if b == 127 {
                            None
                        } else if y == 0 {
                            Some(Color32::from_rgb(0, 0, brightness))
                        } else if y == 1 {
                            Some(Color32::from_rgb(0, brightness, 0))
                        } else {
                            Some(Color32::from_rgb(brightness, 0, 0))
                        }
                    };

                    let bar_height = ui.available_height() / 3.0;

                    if let Some(color) = color {
                        let min = Pos2::new(
                            ui.cursor().min.x + (x as f32 * bar_width_px),
                            ui.cursor().min.y + (y as f32 * bar_height),
                        );
                        let max = Pos2::new(
                            ui.cursor().min.x + ((x + 1) as f32 * bar_width_px),
                            ui.cursor().min.y + ((y + 1) as f32 * bar_height),
                        );
                        shapes.push(Shape::rect_filled(
                            Rect::from_min_max(min, max),
                            Rounding::default(),
                            color,
                        ))
                    }
                }
            }
            ui.painter().add(shapes);
        }

        // X Position of the playhead on the screen
        let playhead_x: Option<f32> = if deck.waveform.is_some() && *mode == WaveformMode::Overview
        {
            Some(if let Some(file_info) = deck.player_state.file_info {
                if playhead_samples >= file_info.count_samples as i64 {
                    waveform_width_px
                } else if playhead_samples == 0 {
                    -1.0
                } else if playhead_samples < 0 {
                    -2.0
                } else {
                    (playhead_samples as f32 / file_info.count_samples as f32) * waveform_width_px
                }
            } else {
                -1.0
            })
        } else if *mode == WaveformMode::Overview {
            None
        } else {
            Some(waveform_width_px / 2.0)
        };

        // Draw Playhead
        if let Some(playhead_x) = playhead_x {
            let playheader_x_width = 1.0;
            ui.painter().vline(
                ui.cursor().min.x + playhead_x,
                ui.cursor().min.y + PLAYHEAD_MARGIN_TOP_BOTTOM
                    ..=ui.cursor().min.y + ui.available_height() - PLAYHEAD_MARGIN_TOP_BOTTOM,
                Stroke::new(playheader_x_width, Color32::BLACK),
            );
        }
    }
}

impl egui::Widget for DeckWidget<'_> {
    fn ui(mut self, ui: &mut egui::Ui) -> egui::Response {
        let single_deck_height = DECK_HEIGHT;
        let single_deck_size = Vec2::new(ui.available_width(), single_deck_height);
        // println!("decksize = {size}");
        ui.vertical(|ui| {
            for deck_index in 0..4 {
                ui.add_full_space(single_deck_size, |ui| {
                    self.add_single_deck(ui, deck_index);
                });
            }
        })
        .response
    }
}

pub fn draw_time_and_loop_size(ui: &mut egui::Ui, deck: &Deck) {
    let (elapsed_secs, duration_secs) = if let Some(file_info) = deck.player_state.file_info {
        let position = deck.player_state.playhead_samples as f32 / file_info.sample_rate as f32;
        let duration = file_info.count_samples as f32 / file_info.sample_rate as f32;

        (Some(position), Some(duration))
    } else {
        (None, None)
    };

    let elapsed_duration = format!(
        "  {} / {}",
        if let Some(elapsed_secs) = elapsed_secs {
            format_time_elapsed(elapsed_secs)
        } else {
            " --:--:--".to_string()
        },
        if let Some(duration_secs) = duration_secs {
            format_time_length(duration_secs)
        } else {
            "--:--".to_string()
        },
    );

    let loop_size = format!(
        "       {}",
        match deck.player_state.loop_length_beat() {
            None => "Loop: 00 OFF".to_string(),
            Some(loop_size_beat) => {
                let enabled = deck.player_state.is_playhead_inside_enabled_loop();
                format!(
                    "Loop: {:02} {}",
                    loop_size_beat,
                    if enabled { "ON" } else { "OFF" }
                )
            }
        }
    );

    let style = ui.style();
    let mut layout_job = LayoutJob::default();
    RichText::new(elapsed_duration)
        .color(style.visuals.text_color())
        .append_to(
            &mut layout_job,
            style,
            FontSelection::Default,
            Align::Center,
        );
    RichText::new(loop_size)
        .color(if deck.player_state.is_playhead_inside_enabled_loop() {
            Color32::RED
        } else {
            style.visuals.text_color()
        })
        .append_to(
            &mut layout_job,
            style,
            FontSelection::Default,
            Align::Center,
        );
    let galley = ui.painter().layout_job(layout_job);
    let (galley_pos, _response) = ui.allocate_exact_size(galley.size(), Sense::empty());
    ui.painter().add(epaint::TextShape::new(
        galley_pos.left_top(),
        galley,
        Color32::RED,
    ));
}

pub fn format_time_elapsed(time_sec: f32) -> String {
    let sign = if time_sec >= 0.0 { "+" } else { "-" };
    let time_sec = time_sec.abs();
    let minutes = (time_sec / 60.0) as i32;
    let seconds = (time_sec).rem_euclid(60.0) as i32;
    let miliseconds = (time_sec * 100.0).rem_euclid(100.0) as i32;
    format!("{}{:02}:{:02}.{:02}", sign, minutes, seconds, miliseconds)
}

pub fn format_time_length(time_sec: f32) -> String {
    let minutes = (time_sec / 60.0) as i32;
    let seconds = (time_sec).rem_euclid(60.0) as i32;
    format!("{:02}:{:02}", minutes, seconds)
}

pub fn format_beat_position_and_beat_size(beat_position: BeatPosition, beat_size: f32) -> String {
    let mut beat = format!("{:.01}", beat_position.beat());
    if beat == "5.0" {
        beat = "1.0".to_string();
    }
    format!(
        "   {}      / {:.00}           Size: {:.00}",
        beat,
        beat_position.bar(4.0),
        beat_size
    )
}
