use crate::{
    colors::COLOR_TEXT,
    egui_extension::LibreCDJUI,
    key_handling::on_raw_input_hook,
    widgets::{
        decks::DeckWidget,
        library::{LibraryWidget, SIGNAL_LIBRARY_CURRENT_AMOUNT_ITEMS},
    },
};

use eframe::egui::{self, Color32, Margin, Rounding, Spacing, Style, Vec2, Visuals};
use libbackend::{
    audio_engine::{AudioEngine, AudioEngineCommand, AudioEngineToGUIMessage},
    backend::Backend,
    jack::JackBackend,
};

pub struct App {
    pub backend: Backend,
    pub jack_backend: JackBackend,
}

fn setup_fonts(ctx: &egui::Context) {
    // Start with the default fonts (we will be adding to them rather than replacing them).
    let mut fonts = egui::FontDefinitions::default();

    let hack_regular_name = "Hack-Regular";
    // Install my own font (maybe supporting non-latin characters).
    // .ttf and .otf files supported.
    fonts.font_data.insert(
        hack_regular_name.to_owned(),
        egui::FontData::from_static(include_bytes!("../fonts/Hack-Regular.ttf")).into(),
    );

    // Put my font first (highest priority) for proportional text:
    fonts
        .families
        .entry(egui::FontFamily::Proportional)
        .or_default()
        .insert(0, hack_regular_name.to_owned());

    // Put my font as last fallback for monospace:
    fonts
        .families
        .entry(egui::FontFamily::Monospace)
        .or_default()
        .push(hack_regular_name.to_owned());

    // Tell egui to use these fonts:
    ctx.set_fonts(fonts);
}

impl App {
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // Customize egui here with cc.egui_ctx.set_fonts and cc.egui_ctx.set_visuals.
        // Restore app state using cc.storage (requires the "persistence" feature).
        // Use the cc.gl (a glow::Context) to create graphics shaders and buffers that you can use
        // for e.g. egui::PaintCallback.
        let (to_gui_tx, from_audio_rx) = rtrb::RingBuffer::<AudioEngineToGUIMessage>::new(2048);
        let (to_audio_tx, from_gui_rx) = rtrb::RingBuffer::<AudioEngineCommand>::new(2048);
        let audio_engine = AudioEngine::new(to_gui_tx, from_gui_rx);
        let jack_backend = JackBackend::new(audio_engine);
        let mut backend = Backend::new(from_audio_rx, to_audio_tx);
        backend
            .library
            .set_ui_height_arc(Some(SIGNAL_LIBRARY_CURRENT_AMOUNT_ITEMS.clone()));

        setup_fonts(&cc.egui_ctx);

        cc.egui_ctx.set_style(Style {
            spacing: Spacing {
                item_spacing: Vec2::new(0.0, 0.0),
                window_margin: Margin::same(0),
                ..Default::default()
            },
            ..Default::default()
        });

        cc.egui_ctx.set_visuals(Visuals {
            override_text_color: Some(COLOR_TEXT),
            ..Default::default()
        });

        Self {
            backend,
            jack_backend,
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        self.backend.calculate_state();
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.painter()
                .rect_filled(ui.clip_rect(), Rounding::default(), Color32::WHITE);

            let deck_height = 400.0;
            let deck_size = Vec2::new(ui.available_width(), deck_height);
            ui.add_full_space(deck_size, |ui| {
                ui.add(DeckWidget::new(
                    &self.backend.state.decks,
                    &self.backend.state.positions,
                    &self.backend.state.waveform_mode,
                ));
            });
            ui.add_full_space(ui.available_size(), |ui| {
                ui.add(LibraryWidget::new(&self.backend));
            });

            ctx.request_repaint();
        });
    }
    fn raw_input_hook(&mut self, ctx: &egui::Context, raw_input: &mut egui::RawInput) {
        on_raw_input_hook(self, ctx, raw_input)
    }
}
