mod app;
mod colors;
mod egui_extension;
mod key_handling;
mod widgets;

use crate::app::App;

fn main() {
    tracing_subscriber::fmt::init();

    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "librecdj",
        native_options,
        Box::new(|cc| Ok(Box::new(App::new(cc)))),
    )
    .expect("Failed starting gui");
}
