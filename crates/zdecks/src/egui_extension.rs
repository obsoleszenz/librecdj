use eframe::egui::{InnerResponse, Rect, TextWrapMode, Ui, UiBuilder, Vec2};

pub trait LibreCDJUI {
    fn add_full_space<R>(
        &mut self,
        size: Vec2,
        add_contents: impl FnOnce(&mut Ui) -> R,
    ) -> InnerResponse<R>;

    fn add_horizontal_full_space<R>(
        &mut self,
        size: Vec2,
        add_contents: impl FnOnce(&mut Ui) -> R,
    ) -> InnerResponse<()>;

    fn available_rect(&self) -> Rect;

    fn style_no_spacing(&mut self);
}

impl LibreCDJUI for Ui {
    fn add_full_space<R>(
        &mut self,
        size: Vec2,
        add_contents: impl FnOnce(&mut Ui) -> R,
    ) -> InnerResponse<R> {
        let (_id, rect) = self.allocate_space(size);
        self.allocate_new_ui(UiBuilder::new().max_rect(rect), |ui| {
            ui.set_min_width(size.x);
            ui.set_min_height(size.y);
            ui.style_mut().wrap_mode = Some(TextWrapMode::Truncate);
            add_contents(ui)
        })
    }

    fn add_horizontal_full_space<R>(
        &mut self,
        size: Vec2,
        add_contents: impl FnOnce(&mut Ui) -> R,
    ) -> InnerResponse<()> {
        self.horizontal(|ui| {
            ui.add_full_space(size, add_contents);
        })
    }

    /// Just a shorter alias for [`Ui::available_rect_before_wrap`]
    fn available_rect(&self) -> Rect {
        self.available_rect_before_wrap()
    }

    /// Set horizontal and vertical item spacing to 0.0
    fn style_no_spacing(&mut self) {
        self.style_mut().spacing.item_spacing = Vec2::new(0.0, 0.0);
    }
}
