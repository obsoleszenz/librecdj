use eframe::egui;
use libbackend::keyboardbindings::{
    key_event::LCDJKeyEvent, LCDJKeyCode, LCDJKeyCombination, LCDJKeyEventKind,
};
use tracing::debug;

use crate::app::App;

pub fn on_raw_input_hook(app: &mut App, _ctx: &egui::Context, raw_input: &mut egui::RawInput) {
    for event in raw_input.events.iter() {
        debug!("event = {event:?}");
        if let egui::Event::Key {
            key,
            physical_key: _,
            pressed,
            repeat: _,
            modifiers,
        } = event
        {
            let lcdj_key_code = match convert_egui_key_to_lcdj_key_code(key) {
                Some(key_code) => key_code,
                None => continue,
            };
            let lcdj_key_combination = convert_egui_modifiers_to_lcdj_key(modifiers, lcdj_key_code);
            let lcdj_key_event = LCDJKeyEvent::new(
                lcdj_key_combination,
                if *pressed {
                    LCDJKeyEventKind::Press
                } else {
                    LCDJKeyEventKind::Release
                },
            );

            println!("{lcdj_key_event:?}");
            app.backend.on_key(lcdj_key_event);
        }
    }
}
pub fn convert_egui_key_to_lcdj_key_code(key: &egui::Key) -> Option<LCDJKeyCode> {
    println!("xxx {key:?}");
    Some(match key {
        egui::Key::ArrowDown => LCDJKeyCode::Down,
        egui::Key::ArrowLeft => LCDJKeyCode::Left,
        egui::Key::ArrowRight => LCDJKeyCode::Right,
        egui::Key::ArrowUp => LCDJKeyCode::Up,
        egui::Key::Escape => LCDJKeyCode::Esc,
        egui::Key::Enter => LCDJKeyCode::Enter,
        egui::Key::PageUp => LCDJKeyCode::PageUp,
        egui::Key::PageDown => LCDJKeyCode::PageDown,
        egui::Key::F1 => LCDJKeyCode::F(1),
        egui::Key::F2 => LCDJKeyCode::F(2),
        egui::Key::F3 => LCDJKeyCode::F(3),
        egui::Key::F4 => LCDJKeyCode::F(4),
        egui::Key::F5 => LCDJKeyCode::F(5),
        egui::Key::F6 => LCDJKeyCode::F(6),
        egui::Key::F7 => LCDJKeyCode::F(7),
        egui::Key::F8 => LCDJKeyCode::F(8),
        egui::Key::F9 => LCDJKeyCode::F(9),
        egui::Key::F10 => LCDJKeyCode::F(10),
        egui::Key::F11 => LCDJKeyCode::F(11),
        egui::Key::F12 => LCDJKeyCode::F(12),
        egui::Key::F13 => LCDJKeyCode::F(13),
        egui::Key::F14 => LCDJKeyCode::F(14),
        egui::Key::F15 => LCDJKeyCode::F(15),
        egui::Key::F16 => LCDJKeyCode::F(16),
        egui::Key::F17 => LCDJKeyCode::F(17),
        egui::Key::F18 => LCDJKeyCode::F(18),
        egui::Key::F19 => LCDJKeyCode::F(19),
        egui::Key::F20 => LCDJKeyCode::F(20),
        egui::Key::F21 => LCDJKeyCode::F(21),
        egui::Key::F22 => LCDJKeyCode::F(22),
        egui::Key::F23 => LCDJKeyCode::F(23),
        egui::Key::F24 => LCDJKeyCode::F(24),
        egui::Key::F25 => LCDJKeyCode::F(25),
        egui::Key::F26 => LCDJKeyCode::F(26),
        egui::Key::F27 => LCDJKeyCode::F(27),
        egui::Key::F28 => LCDJKeyCode::F(28),
        egui::Key::F29 => LCDJKeyCode::F(29),
        egui::Key::F30 => LCDJKeyCode::F(30),
        egui::Key::F31 => LCDJKeyCode::F(31),
        egui::Key::F32 => LCDJKeyCode::F(32),
        egui::Key::F33 => LCDJKeyCode::F(33),
        egui::Key::F34 => LCDJKeyCode::F(34),
        egui::Key::F35 => LCDJKeyCode::F(35),
        egui::Key::A => LCDJKeyCode::Char('a'),
        egui::Key::B => LCDJKeyCode::Char('b'),
        egui::Key::C => LCDJKeyCode::Char('c'),
        egui::Key::D => LCDJKeyCode::Char('d'),
        egui::Key::E => LCDJKeyCode::Char('e'),
        egui::Key::F => LCDJKeyCode::Char('f'),
        egui::Key::G => LCDJKeyCode::Char('g'),
        egui::Key::H => LCDJKeyCode::Char('h'),
        egui::Key::I => LCDJKeyCode::Char('i'),
        egui::Key::J => LCDJKeyCode::Char('j'),
        egui::Key::K => LCDJKeyCode::Char('k'),
        egui::Key::L => LCDJKeyCode::Char('l'),
        egui::Key::M => LCDJKeyCode::Char('m'),
        egui::Key::N => LCDJKeyCode::Char('n'),
        egui::Key::O => LCDJKeyCode::Char('o'),
        egui::Key::P => LCDJKeyCode::Char('p'),
        egui::Key::Q => LCDJKeyCode::Char('q'),
        egui::Key::R => LCDJKeyCode::Char('r'),
        egui::Key::S => LCDJKeyCode::Char('s'),
        egui::Key::T => LCDJKeyCode::Char('t'),
        egui::Key::U => LCDJKeyCode::Char('u'),
        egui::Key::V => LCDJKeyCode::Char('v'),
        egui::Key::W => LCDJKeyCode::Char('w'),
        egui::Key::X => LCDJKeyCode::Char('x'),
        egui::Key::Y => LCDJKeyCode::Char('y'),
        egui::Key::Z => LCDJKeyCode::Char('z'),
        egui::Key::Tab => LCDJKeyCode::Tab,
        egui::Key::Backspace => LCDJKeyCode::Backspace,
        egui::Key::Space => LCDJKeyCode::Char(' '),
        egui::Key::Insert => LCDJKeyCode::Insert,
        egui::Key::Delete => LCDJKeyCode::Delete,
        egui::Key::Home => LCDJKeyCode::Home,
        egui::Key::End => LCDJKeyCode::End,
        egui::Key::Copy => LCDJKeyCode::Copy,
        egui::Key::Cut => LCDJKeyCode::Cut,
        egui::Key::Paste => LCDJKeyCode::Paste,
        egui::Key::Colon => LCDJKeyCode::Char(':'),
        egui::Key::Comma => LCDJKeyCode::Char(','),
        egui::Key::Backslash => LCDJKeyCode::Char('\\'),
        egui::Key::Slash => LCDJKeyCode::Char('/'),
        egui::Key::Pipe => LCDJKeyCode::Char('|'),
        egui::Key::Questionmark => LCDJKeyCode::Char('?'),
        egui::Key::OpenBracket => LCDJKeyCode::Char('('),
        egui::Key::CloseBracket => LCDJKeyCode::Char(')'),
        egui::Key::Backtick => LCDJKeyCode::Char('`'),
        egui::Key::Minus => LCDJKeyCode::Char('-'),
        egui::Key::Period => LCDJKeyCode::Char('.'),
        egui::Key::Plus => LCDJKeyCode::Char('+'),
        egui::Key::Equals => LCDJKeyCode::Char('='),
        egui::Key::Semicolon => LCDJKeyCode::Char(';'),
        egui::Key::Quote => LCDJKeyCode::Char('"'),
        egui::Key::Num0 => LCDJKeyCode::NumPad(0),
        egui::Key::Num1 => LCDJKeyCode::NumPad(1),
        egui::Key::Num2 => LCDJKeyCode::NumPad(2),
        egui::Key::Num3 => LCDJKeyCode::NumPad(3),
        egui::Key::Num4 => LCDJKeyCode::NumPad(4),
        egui::Key::Num5 => LCDJKeyCode::NumPad(5),
        egui::Key::Num6 => LCDJKeyCode::NumPad(6),
        egui::Key::Num7 => LCDJKeyCode::NumPad(7),
        egui::Key::Num8 => LCDJKeyCode::NumPad(8),
        egui::Key::Num9 => LCDJKeyCode::NumPad(9),
        egui::Key::Exclamationmark => LCDJKeyCode::Char('!'),
        egui::Key::OpenCurlyBracket => LCDJKeyCode::Char('{'),
        egui::Key::CloseCurlyBracket => LCDJKeyCode::Char('}'),
    })
}

pub fn convert_egui_modifiers_to_lcdj_key(
    modifiers: &egui::Modifiers,
    key_code: LCDJKeyCode,
) -> LCDJKeyCombination {
    if modifiers.alt && modifiers.ctrl {
        LCDJKeyCombination::CtrlAlt(key_code)
    } else if modifiers.alt {
        LCDJKeyCombination::Alt(key_code)
    } else if modifiers.ctrl {
        LCDJKeyCombination::Ctrl(key_code)
    } else if modifiers.shift_only() {
        LCDJKeyCombination::Shift(key_code)
    } else {
        LCDJKeyCombination::Single(key_code)
    }
}
