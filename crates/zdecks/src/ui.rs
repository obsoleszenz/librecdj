struct App {
    pub backend: Backend,
    pub jack_backend: JackBackend,
}

impl App {
    fn new(cc: &eframe::CreationContext<'_>) -> Self {
        // Customize egui here with cc.egui_ctx.set_fonts and cc.egui_ctx.set_visuals.
        // Restore app state using cc.storage (requires the "persistence" feature).
        // Use the cc.gl (a glow::Context) to create graphics shaders and buffers that you can use
        // for e.g. egui::PaintCallback.
        let (to_gui_tx, from_audio_rx) = rtrb::RingBuffer::<AudioEngineToGUIMessage>::new(2048);
        let (to_audio_tx, from_gui_rx) = rtrb::RingBuffer::<AudioEngineCommand>::new(2048);
        let audio_engine = AudioEngine::new(to_gui_tx, from_gui_rx);
        let jack_backend = JackBackend::new(audio_engine);
        let mut backend = Backend::new(from_audio_rx, to_audio_tx);
        backend
            .library
            .set_ui_height_arc(Some(SIGNAL_LIBRARY_CURRENT_AMOUNT_ITEMS.clone()));
        Self {
            backend,
            jack_backend,
        }
    }
}

impl eframe::App for App {
    fn update(&mut self, ctx: &egui::Context, frame: &mut eframe::Frame) {
        self.backend.calculate_state();
        egui::CentralPanel::default().show(ctx, |ui| {
            ui.painter()
                .rect_filled(ui.clip_rect(), Rounding::default(), Color32::WHITE);
            ui.style_mut().spacing.item_spacing = Vec2::new(0.0, 0.0);

            ui.add(DeckWidget::new(&self.backend.state.decks));
            ui.add(LibraryWidget::new(&self.backend));

            ctx.request_repaint();
        });
    }
    fn raw_input_hook(&mut self, _ctx: &egui::Context, raw_input: &mut egui::RawInput) {
        on_raw_input_hook(self, _ctx, raw_input)
    }
}
