# CHANGELOG

## Unreleased
- Some refactorings on libbackend::library (see #49)
- Fix keybindings missing Alt-{Up|Down} for moving playlist items
- Fix playlists not being sorted by name
- Add support for .ogg files (@sonnenteich)
- Update Dummy mapping to be in sync with the MidiMapping trait again (@sonnenteich)

## v0.3.0 -- 17.10.2024

### libbackend
- Add support for custom keybindings. They are loaded from `~/.config/librecdj/keyboard.ron`.

## v0.2.0 -- 27.09.2024

### General
- Update nix files to support package building
- a lot of bug fixes left and right

### libmetadata
- Upgrade lofty

### libbackend
- Create libbackend crate which holds the backend
- implement support for midi controllers
  - add support for Denon SC2000 midi controller

### tui
- Restructure tui widgets
- Move out backend code


## v0.1.0 -- 15-02-2024

### librecdj
- Add conversion of openkey & traditional key notations to lancelot notation
- Change keybindings for yxcv to change tempo of all decks, and win+x,y,c,v to change tempo for selected deck 
- Add jump & move to section

### libkeynotation
- initial release
- fix panic because of overseen todo!
- implement transposing

### libmetadata
- initial release
- use lofty for tag reading
- add support for flac, wav, aiff...

### libplayer
- Fix bug in resampler where pitch couldnt get changed if `sample_rate_in` == `sample_rate_out`
- Update rubato to 0.14.1
- Switch to git version of symphonia at commit b157bb5
- Add support for flac/wav/aiff

