<div align="right">
    <a href="https://codeberg.org/obsoleszenz/librecdj/releases"><img src="https://codeberg.org/obsoleszenz/librecdj/raw/branch/main/assets/version-badge.svg" alt="version badge" width="120px"/></a>
   <a href="https://www.gnu.org/licenses/gpl-3.0"><img src="https://codeberg.org/obsoleszenz/librecdj/raw/branch/main/assets/license-badge.png" alt="gpl license badge" width="120px"/></a>
</div>
<br>
<br>
<div align="center">
  <h1>LibreCDJ</h1>
  <h4>An experimental playground to develop a 4 Deck DJ Software for Linux.</h4>


<img height="500px" width="auto" src="https://codeberg.org/obsoleszenz/librecdj/raw/branch/main/assets/screenshot-1.png" alt="Screenshot of the gui"/>
  <br>
Current screen design for future gui version:<br>
<img height="500px" width="auto" src="https://codeberg.org/obsoleszenz/librecdj/raw/branch/main/assets/screen-design.svg" alt="Screenshot of the gui"/>
  <br>
</div>


## Goals

- 4 Decks
- Good beat jumping/looping
- Good performance
- Low latency
- As best as possible playback/resampling
- Minimal & clean user interface
- Good library management
- Completely usable with Keyboard


## Non-Goals

- No mixing/eq/effects. Use an external mixer or something like [equis](https://codeberg.org/obsoleszenz/equis)
- Session management. Use qpwgraph for this
- Radio Broadcasting
- Stem seperation (I'm up to support stem files though)
- Rekordbox/Serato export/import
    - If there's a good crate, maybe though

## Supported Hardware
- Denon SC2000

## How to run
Make sure you have pipewire/jack installed and a rust compiler. Also i recommend using kitty as a terminal (or any terminal supporting key release events).

```
- git clone the repo
- PIPEWIRE_QUANTUM=256 PLAYLISTS=/path/to/playlists LIBRARY=/path/to/your/music cargo run --release
```
## Keybindings

- `Esc` => Open Menu
- `Arrow Keys` => navigate library/file browser/popup
- `Enter` => Load track to current Deck
- `F1-F4` => Switch between Deck 1-4
- `q` => Cue
- `w` => Play
- `a s d f` => Nudging
- `z x c v` => Adjusting Pitch
- `i o p [ ]` => Set loop starting at current playhead
- `I O P { }` => Set loop endingt at current playhead
- `n m , . /`- => Jump x beats forward
- `N M < > ?` => Jump x beats backwards
- `Space` => Switch waveform zoom
- `F10` => Debug logs

To quit the program first press `Esc`, then `q`.

### Customize Keybindings

The Keybindings are almost completely customizable. 
To do so, copy `resources/keybindings/en_US.ron` to `~/.config/librecdj/keyboard.ron`.
You can extend/alter the bindings map. Please have a look at the file and if you have 
questions feel free to ask. 

## Architecture
![](https://codeberg.org/obsoleszenz/librecdj/raw/branch/main/assets/architecture.svg)
