{ pkgs ? import <nixpkgs> { } }:
let manifest = (pkgs.lib.importTOML ./crates/librecdj/Cargo.toml).package;
in pkgs.rustPlatform.buildRustPackage rec {
  pname = manifest.name;
  version = manifest.version;
  cargoLock = {
    lockFile = ./Cargo.lock; 
    allowBuiltinFetchGit = true;
  };
  src = pkgs.lib.cleanSource ./.;

  nativeBuildInputs = with pkgs; [
           pkg-config
  ];
  buildInputs = with pkgs; [
           jack2 
  ];

}
